# README #


## Info ##

* This is the ION Racing software suite, here you can find all the projects we are currently working on.


## Setup and installation ##

### Windows ###
* Install and start Visual Studio 2015 community edition
* Open the Team Viewer Connections
    * View -> Team explorer -> Connections(Green power connector in top)
* Under Local Git Repositories click Clone
* Enter https://bitbucket.org/ionracing/Ion.Master and click Clone
* Choose which project you are going to work on under
    * Team Explorer -> Home (House in top) -> Solutions

### GNU/Linux ###
* Install Mono And MonoDevelop
* Clone repository with Git and select project

### Mac ###
* Install Xmarine Studio
* Clone repository with Git and select project

### Ion.Graphics.IonEngine ###
To be able to work on this project you have to follow this steps

#### Windows ####
* Extract the files in the zip folder named Ion.Graphics.IonEngine.Missing Files.zip under Ion.Graphics.IonEngine

#### GNU/Linux and Mac ####
* Download and install SDL2 (Simple Direct Layer 2)

## Contact information ##
* Repository admin: Nicro950

### Markdown ###
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)