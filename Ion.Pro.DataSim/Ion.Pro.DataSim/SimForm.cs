﻿using Ion.Data.Networking;
using Ion.Data.Networking.Manager;
using Ion.Data.Sensor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ion.Pro.DataSim
{
    public partial class SimForm : Form
    {
        LocalNetworkClient simClient;
        Timer updateTimer;
        Timer simTimer;
        public SimForm()
        {
            InitializeComponent();
            CanError.Initialize();
            simClient = new LocalNetworkClient(NetPorts.DataSend);
            //simClient.Connect(IPAddress.Parse("10.0.0.3"), NetPorts.DataRecive);
            //simClient.Connect(IPAddress.Loopback, NetPorts.DataRecive);
            simClient.Connect(IPAddress.Loopback, NetPorts.ScreenSend);

            foreach (CanError error in CanError.AllErrors())
            {
                listBox1.Items.Add(new ObjContainer<CanError>(error, (s) => $"{s.SensorID}.{s.ValueID}: {s.Msg}"));
            }
            simTimer = new Timer();
            simTimer.Interval = 10;
            simTimer.Tick += SimTimer_Tick;
            simTimer.Start();
            updateTimer = new Timer();
            updateTimer.Interval = 10;
            updateTimer.Tick += UpdateTimer_Tick;
            updateTimer.Start();
            
        }

        double value = 0;
        private void SimTimer_Tick(object sender, EventArgs e)
        {
            vScrollBar1.Value = (int)((Math.Sin(value) + 1) / 2 * (vScrollBar1.Maximum - vScrollBar1.Minimum) + vScrollBar1.Minimum);
            
            vScrollBar2.Value = (int)((Math.Sin(value + Math.PI / 2) + 1) / 2 * (vScrollBar2.Maximum - vScrollBar2.Minimum) + vScrollBar2.Minimum);
            vScrollBar3.Value = (int)((Math.Sin(value + Math.PI) + 1) / 2 * (vScrollBar3.Maximum - vScrollBar3.Minimum) + vScrollBar3.Minimum);
            vScrollBar4.Value = (int)((Math.Sin(value + Math.PI / 2 * 3) + 1) / 2 * (vScrollBar4.Maximum - vScrollBar4.Minimum) + vScrollBar4.Minimum);
            vScrollBar5.Value = (int)((Math.Sin(value + Math.PI / 2 * 4) + 1) / 2 * (vScrollBar5.Maximum - vScrollBar5.Minimum) + vScrollBar5.Minimum);
            lastSpeed = vScrollBar1.Value;
            lastVoltage12 = vScrollBar2.Value;
            lastSoc = vScrollBar3.Value;
            lastTempBat = vScrollBar4.Value;
            lastTempCool = vScrollBar5.Value;

            value %= (Math.PI * 2);
            value += 0.01;
        }

        int lastSpeed = 0;
        int lastVoltage12 = 0;
        int lastSoc = 0;
        int lastTempBat = 0;
        int lastTempCool = 0;

        private void UpdateTimer_Tick(object sender, EventArgs e)
        {
            simClient.SendDataWrappers(
            new DataWrapper[]
            {
                new DataWrapper() { SensorID = SensorLookup.GetByName(SensorLookup.SPEED).ID, Value = lastSpeed },
                new DataWrapper() { SensorID = SensorLookup.GetByName(SensorLookup.VOLTAGE12).ID, Value = lastVoltage12 },
                new DataWrapper() { SensorID = SensorLookup.GetByName(SensorLookup.SOC).ID, Value = lastSoc},
                new DataWrapper() { SensorID = SensorLookup.GetByName(SensorLookup.TEMPBAT).ID, Value = lastTempBat },
                new DataWrapper() { SensorID = SensorLookup.GetByName(SensorLookup.TEMPCOOL).ID, Value = lastTempCool },
            }, false);
        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            lastSpeed = e.NewValue;
        }

        private void vScrollBar2_Scroll(object sender, ScrollEventArgs e)
        {
            lastVoltage12 = e.NewValue;
        }

        private void vScrollBar3_Scroll(object sender, ScrollEventArgs e)
        {
            lastSoc = e.NewValue;
        }

        private void vScrollBar4_Scroll(object sender, ScrollEventArgs e)
        {
            lastTempBat = e.NewValue;
        }

        private void vScrollBar5_Scroll(object sender, ScrollEventArgs e)
        {
            lastTempCool = e.NewValue;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CanError error = (listBox1.SelectedItem as ObjContainer<CanError>)?.Value;
            if (error != null)
            {
                simClient.SendDataWrappers(
                new DataWrapper[]
                {
                    new DataWrapper() { SensorID = (ushort)error.SensorID, Value = error.ValueID }
                }, false);
            }
        }
    }

    public class ObjContainer<T>
    {
        public T Value { get; set; }
        public Func<T, string> ToStringOverride { get; set; }

        public ObjContainer(T value, Func<T, string> toStringOverride)
        {
            Value = value;
            ToStringOverride = toStringOverride;
        }


        public override string ToString()
        {
            return ToStringOverride(Value);
        }

    }
}
