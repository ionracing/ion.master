﻿using Ion.Data.Networking.Logging;
using Ion.Data.Networking.Manager;
using Ion.Data.Sensor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.CarGui
{
    public class CanEntry : LogEntry
    {
        public DataWrapper CanMessage { get; set; }

        public override string Value
        {
            get
            {
                return this.ToString();
            }
            set
            {

            }
        }

        public CanError GetError()
        {
            return CanError.GetError(CanMessage.SensorID, CanMessage.Value);
        }

        public override string ToString()
        {
            return GetError().ToString();
        }
    }


}
