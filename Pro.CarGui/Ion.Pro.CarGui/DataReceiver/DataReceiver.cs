﻿using Ion.Data.Networking;
using Ion.Data.Networking.Logging;
using Ion.Data.Networking.Manager;
using Ion.Data.Sensor;
using Ion.Graphics.IonEngine;
using Ion.Graphics.IonEngine.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.CarGui
{

    public class DataReciver
    {
        public DataReciver()
        {

        }
        //static LogManager manager;
        static ILogger receiveLogger;

        public static bool DriveToSide = false;
        public static DateTime MessageReceived = DateTime.Now;


        public static DataClient carClient;
        public static DataClient telemetryClient;

        public static bool running = false;

        static Dictionary<SensorLookup, DataWrapper> lastesTable = new Dictionary<SensorLookup, DataWrapper>();

        public static DataWrapper? GetData(ushort sensorID)
        {
            return GetData(SensorLookup.GetById(sensorID));
        }

        public static DataWrapper? GetData(string name)
        {
            return GetData(SensorLookup.GetByName(name));
        }

        public static DataWrapper? GetData(SensorLookup lookup)
        {
            if (lookup == null)
                return null;
            if (lastesTable.ContainsKey(lookup))
                return lastesTable[lookup];
            return null;
        }

        public static void Initialize()
        {
            LogManager manager = new LogManager();
            manager.OnVerbose += (object sender, LogWriteEventArgs e) => Console.WriteLine("(" + e.Entry.Time.ToString("HH:mm:ss") + ")[" + e.Entry.Sender + "]\t" + e.Entry.Value);
            Initialize(manager);
        }

        public static void Initialize(ILogManager manager)
        {
            receiveLogger = manager.CreateLogger("RECEIVER");

            try
            {
                receiveLogger.WriteLine("Started to listen to local interface", "INFO", 2);
                carClient = new LocalNetworkClient(NetPorts.ScreenSend) { PackageMode= WrapperMode.CompressedMode };

                Task.Run(new Action(ReceiveLoop));
            }
            catch (Exception e)
            {
                receiveLogger.WriteException("Error opening connection", e, 4);
            }
        }

        public static bool Contains(string[] devices, string dev)
        {
            foreach (string s in devices)
            {
                if (s == dev)
                    return true;
            }
            return false;
        }

        public static void ReceiveLoop()
        {
            receiveLogger.WriteLine("Started car receive loop", "INFO", 1);
            int packagesReceived = 0;
            try
            {
                while (true)
                {

                    DataWrapper[] data = carClient.ReadDataWrappers(false);
                    packagesReceived++;
                    AddValue(data);

                    //receiveLogger.WriteLine("Received message, bytes left to read: " + piPort.BytesToRead + " package count: " + packagesReceived, "INFO", 1);
                }
            }
            catch (Exception e)
            {
                receiveLogger.WriteException("There has been an error in the recive loop", e, 4);
            }
        }

        private static void AddValue(DataWrapper[] wrapper)
        {
            foreach (DataWrapper dw in wrapper)
            {
                AddValue(dw);
            }
        }

        private static void AddValue(DataWrapper wrapper)
        {
            wrapper.TimeStamp = (int)(GameTime.GlobalTime?.TotalElapsed.TotalMilliseconds ?? 0);
            //usartLogWriter.Write(wrapper.GetBytesWithTime());
            SensorLookup lookup = SensorLookup.GetById(wrapper.SensorID);
            if (lastesTable == null)
                lastesTable = new Dictionary<SensorLookup, DataWrapper>();
            if (lookup != null)
            {
                if (lookup.ID == 0x85)
                {
                    receiveLogger?.WriteVerbose("Recived button push!");
                    if (!lastesTable.ContainsKey(lookup) || (lastesTable.ContainsKey(lookup) && lastesTable[lookup].TimeStamp + 30 < wrapper.TimeStamp))
                    {
                        Keyboard.AddKey((Keys)(wrapper.Value + 200));
                        receiveLogger?.WriteVerbose("Adding push event: " + (wrapper.Value + 200).ToString() + " -> " + ((Keys)(wrapper.Value + 200)).ToString());
                    }
                }
                if (lastesTable.ContainsKey(lookup))
                    lastesTable[lookup] = wrapper;
                else
                    lastesTable.Add(lookup, wrapper);
                

                /*if (wrapper.SensorID > 99 && wrapper.SensorID < 200)
                {
                    exceptionWriter.Write(wrapper.GetBytes(WrapperMode.ShortMode));
                    canLogger.WriteCustom(new CanEntry() { CanMessage = wrapper, Category = "ERROR", Level = 4, Value = "CAN error message" });
                }*/
            }
            //receiveLogger.WriteLine(string.Format("Message ID: {0} \tValue: {1}", wrapper.SensorID, wrapper.Value), "INFO", 1);


        }
    }



    public class OBJWrap<T>
    {
        public T obj { get; set; }

        public OBJWrap(T t)
        {
            this.obj = t;
        }
    }
}
