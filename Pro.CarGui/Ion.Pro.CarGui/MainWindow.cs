﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ion.Graphics.IonEngine;
using Ion.Graphics.IonEngine.Events;
using Ion.Graphics.IonEngine.Drawing;
using Ion.Graphics.IonEngine.Font;
using Ion.Graphics.IonEngine.Gui;
using System.IO;
using Ion.Data.Networking.Manager;
using Ion.Data.Sensor;
using Ion.Data.Networking.Logging;
using Ion.Data.Networking.CoreCom;
using System.Diagnostics;
using NicroWare.Lib.IonEngine.PNGParser;
using NicroWare.Lib.IonEngine.PIInput;
using System.Net.Sockets;
using System.Net;
using Ion.Data.Networking;

namespace Ion.Pro.CarGui
{
    class MainWindow : GuiWindow
    {
        public const bool DebugMode = true;
        SDLRenderer renderer;
        public SDLFont digiFont;
        public SDLFont numberFont;
        public SDLFont newFont;
        public SDLFont sansSerifFont;
        KeyboardState curKey;
        KeyboardState prevKey;
        MouseState prevMouse;
        MouseState curMouse;
        int maxSpeed;// = 200;

        ILogManager mainLogger;
        ILogger guiLogger;
        public const string Version = "0.1.0 alpha";
        public const string Name = "CARGUI";

        public static bool CoreOnline { get; private set; } = false;

        SDLSurface testSurface;
        SDLTexture testTexture;

        //Disable warning about field is never assigned and is never used because thay are assigned via code
        //and many are not used because of GUI Elements
        #pragma warning disable 649, 169
        #region Page0
        ListView listView_event;
        Button button_clear;
        Button button_clearLight;
        #endregion

        #region Page1
        //InfoMeter infometer_bat;
        //InfoMeter infometer_volt;
        //InfoMeter infometer_tempBat;
        //InfoMeter infometer_tempCool;
        //InfoMeter[] infometers;
        NewInfoMeter newinfometer_bat;
        NewInfoMeter newinfometer_volt;
        NewInfoMeter newinfometer_tempBat;
        NewInfoMeter newinfometer_tempCool;
        NewInfoMeter[] newinfometers;
        NewSpeedometer newSpeedometer;
        Label label_Speed;
        Label label_Speed2;
        Label label_voltageMax;
        Label label_voltageMin;
        Label label_batMax;
        Label label_batMin;
        Label label_tempCoolMax;
        Label label_tempCoolMin;
        Label label_tempBatMax;
        Label label_tempBatMin;
        TextBlock textBox_infoBlock;
        Image image_bat;
        Image image_bat12;
        Image image_coolTemp;
        Image image_batTemp;
        Image image_warningBox;
        Image image_startupWarning;
        Image image_brakeWarning;
        Image image_temperature;
        Image image_pedalWarning;
        Image image_engineWarning;
        Image[] warningLights;
        #endregion

        #region Page2
        Button button_accPedalMin;
        Button button_accPedalMax;
        BarMeter barMeter_accPedal;

        Button button_brakePedalMin;
        Button button_brakePedalMax;
        BarMeter barMeter_brakePedal;


        #endregion

        #region Page3
        ListView listView_Data;
        #endregion

#pragma warning restore 649, 169

        public MainWindow()
        {
            maxSpeed = (int)SensorLookup.GetByName(SensorLookup.SPEED).MaxValue;
            Event.Updater += EventUpdater;
            renderer = SDLRenderer.Create(this);
            Process p = Process.GetCurrentProcess();
            try
            {
                CommandSender sender = new CommandSender();
                string value = sender.SendCommand("Register", Name, p.Id.ToString());
                sender.Close();
                CoreOnline = true;
            }
            catch
            { }

            if (CoreOnline)
            {
                mainLogger = new LogManagerClient();
            }
            else // Creates a logger in case the IonCore is not responding or not available.
            {
                mainLogger = new LogManager();
                //((LogManager)mainLogger).OnVerbose += MainWindow_OnVerbose;
            }
            mainLogger.AddEventListner(MainWindow_OnVerbose, LogLevel.Error);
            //mainLogger = new LogManager();
            guiLogger = mainLogger.CreateLogger("GUI");





#pragma warning disable
            //TODO: Implement event handling for mainlogger
            /*if (DebugMode)
                mainLogger.OnVerbose += MainLogger_OnWarning;
            else
                mainLogger.OnWarning += MainLogger_OnWarning;*/
#pragma warning restore

            FileInfo fontFile = new FileInfo("Content/SFDigital.ttf");
            FileInfo font2File = new FileInfo("Content/MonospaceTypewriter.ttf");
            FileInfo font3File = new FileInfo("Content/Catamaran-Light.ttf");
            FileInfo font4File = new FileInfo("Content/SourceSansPro-Light.ttf");
            digiFont = SDLFont.LoadFont(fontFile.FullName, 150);
            numberFont = SDLFont.LoadFont(font3File.FullName, 150);
            sansSerifFont = SDLFont.LoadFont(font4File.FullName, 150);
            Global.Font = SDLFont.LoadFont(font2File.FullName, 30);
            Global.SideScroll = true;


            InitializeComponents(renderer);
            DataReciver.Initialize(mainLogger);
            CanError.Initialize();

            ShowCursor = false;
            scroller.CurrentPage = 1;
            //FrameLock = true;
            FrameRate = 60;
            guiLogger.WriteInfo("Image: " + image_bat.ImageTexture.Width.ToString() + ", " + image_bat.ImageTexture.Height.ToString());
            

            newinfometer_bat.MaxValue = 100;
            newinfometer_tempBat.MaxValue = 100;
            newinfometer_tempCool.MaxValue = 100;
            newinfometer_volt.MaxValue = 100;

            newinfometers = new NewInfoMeter[4] { newinfometer_bat, newinfometer_tempBat, newinfometer_tempCool, newinfometer_volt };

            warningLights = new Image[6] { image_warningBox
                , image_startupWarning
                , image_temperature
                , image_brakeWarning
                , image_pedalWarning
                , image_engineWarning };

            //PngFile file = PngFile.ReadFile(@"C:\Users\Nicolas\Pictures\ProposalForDataStructure.PNG");

            //SDLSurface surface = SDLSurface.FromPNGFile(file);

            //testTexture = SDLTexture.CreateFrom(renderer, surface);
        }

        private void MainWindow_OnVerbose(object sender, LogWriteEventArgs e)
        {
            listView_event.Items.Add($"[{e.Entry.Time}] ({e.Entry.Level}) {e.Entry.Sender} {e.Entry.Value}");
        }

        private void MainLogger_OnWarning(object sender, LogWriteEventArgs e)
        {
            if (e.Entry is CanEntry && e.Entry.Sender == "CANMSG")
            {
                CanEntry error = (e.Entry as CanEntry);
                image_warningBox.ImageColor = SDLColor.Red;
                switch (error.CanMessage.SensorID)
                {
                    case 100:
                        image_pedalWarning.ImageColor = SDLColor.Red;
                        break;
                    case 101:
                        image_startupWarning.ImageColor = SDLColor.Red;
                        break;
                    case 102:
                        image_engineWarning.ImageColor = SDLColor.Red;
                        break;
                    case 103:
                        image_engineWarning.ImageColor = SDLColor.Red;
                        break;
                }
                listView_event.Items.Add($"{e.Entry.Time}({e.Entry.Sender})[{e.Entry.Level}]: {error.GetError().Msg}");

            }
            else
            {
                listView_event.Items.Add($"{e.Entry.Time}({e.Entry.Sender})[{e.Entry.Level}]: {e.Entry.Value}");
            }
        }

        public void button_clear_Click(object sender, EventArgs e)
        {
            listView_event.Items.Clear();
        }

        public void button_clearLight_Click(object sender, EventArgs e)
        {
            foreach (Image warningLight in warningLights)
                warningLight.ImageColor = new SDLColor(60, 60, 60);
        }

        public void button_accPedalMin_Click(object sender, EventArgs e)
        {
            button_accPedalMin.FrameColor = SDLColor.Green;
        }

        public void EventUpdater()
        {
            InputEvent? evn;
            MouseState current = Mouse.GetState();
            while ((evn = Input.GetEvent()) != null)
            {
                InputEvent ev = evn.Value;
                if (ev.type == 3)
                {
                    Global.SideScroll = true;
                    if (ev.code == 0)
                    {
                        Mouse.SetMousePos((int)(ev.value / 4096.0 * Size.X), current.Location.Y);
                        Mouse.AddKey(MouseButtons.Left);
                        current = Mouse.GetState();
                    }
                    else if (ev.code == 1)
                    {
                        Mouse.SetMousePos(current.Location.X, (int)(ev.value / 4096.0 * Size.Y));
                        Mouse.AddKey(MouseButtons.Left);
                        current = Mouse.GetState();
                    }
                }
                if (ev.type == 1)
                {
                    if (ev.code == 272 && ev.value == 0)
                    {
                        Mouse.RemoveKey(MouseButtons.Left);
                    }
                }
            }
        }

        public void DoEvents()
        {
            prevKey = curKey;
            prevMouse = curMouse;

            curKey = Keyboard.GetState();
            curMouse = Mouse.GetState();

            //Unused
            MouseData.LastPos = new SDLPoint(-1, -1);

            //Test code to simulate speed, page switch, lights and infometers
            double value = 0.1;
            if (curKey.IsKeyDown(Keys.UP))
            {
                newSpeedometer.Value += 0.2;
                foreach (NewInfoMeter newinfoMeter in newinfometers)
                {
                    newinfoMeter.Value += value;
                    value += 0.05;
                }
                    
            }
            else if (curKey.IsKeyDown(Keys.DOWN))
            {
                newSpeedometer.Value -= 0.2;
                foreach (NewInfoMeter newinfoMeter in newinfometers)
                {
                    newinfoMeter.Value -= 0.2;
                }
                    
            }
            if (curKey.IsKeyDown(Keys.LEFT) && !prevKey.IsKeyDown(Keys.LEFT))
                scroller.CurrentPage--;
            else if (curKey.IsKeyDown(Keys.RIGHT) && !prevKey.IsKeyDown(Keys.RIGHT))
                scroller.CurrentPage++;

            if (curKey.IsKeyDown(Keys.L))
            {
                foreach (Image warningLight in warningLights)
                    warningLight.ImageColor = SDLColor.Red;
            }
            else
            {
                foreach (Image warningLight in warningLights)
                    warningLight.ImageColor = new SDLColor(60, 60, 60);
            }

            //if (prevMouse.IsKeyUp(MouseButtons.Left) && curMouse.IsKeyDown(MouseButtons.Left))
            //{
            //    Console.WriteLine(curMouse.Location.X + ", " + curMouse.Location.Y);
            //}
            if (prevKey.IsKeyUp(Keys.F5) && curKey.IsKeyDown(Keys.F5))
            {
                //InitializeComponents(renderer);
            }
        }

        public void UpdateValues()
        {
            ushort[] sensors = SensorLookup.SensorIDs;
            DataWrapper? wrapper = null;

            //Advanced switch body, updates sensor values as it should
            SwitchBody<SensorLookup> switcher = SwitchBody<SensorLookup>.CreateSwitch()
                .Case(SensorLookup.GetByName(SensorLookup.SPEED), () => newSpeedometer.Value = wrapper.Value.CalculateValue())//.Value - SensorLookup.GetByName(SensorLookup.SPEED).MinValue)
                .Case(SensorLookup.GetByName(SensorLookup.SOC), () => newinfometer_bat.Value = wrapper.Value.CalculateValue())//.Value - SensorLookup.GetByName(SensorLookup.SOC).MinValue)
                .Case(SensorLookup.GetByName(SensorLookup.VOLTAGE12), () => newinfometer_volt.Value = wrapper.Value.CalculateValue())//.Value - SensorLookup.GetByName(SensorLookup.VOLTAGE12).MinValue)
                .Case(SensorLookup.GetByName(SensorLookup.TEMPBAT), () => newinfometer_tempBat.Value = wrapper.Value.CalculateValue())//.Value - SensorLookup.GetByName(SensorLookup.TEMPBAT).MinValue)
                .Case(SensorLookup.GetByName(SensorLookup.TEMPCOOL), () => newinfometer_tempCool.Value = wrapper.Value.CalculateValue())//.Value - SensorLookup.GetByName(SensorLookup.TEMPCOOL).MinValue)
                //.Case(SensorLookup.GetByName(SensorLookup.SPEEDPEDDAL), () => speedPeddalMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.SPEEDPEDDAL).MinValue)
                //.Case(SensorLookup.GetByName(SensorLookup.BREAKPEDDAL), () => breakPeddalMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.BREAKPEDDAL).MinValue)
                //.Case(SensorLookup.GetByName(SensorLookup.DRVWHEEL), () => wheelTurnMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.DRVWHEEL).MinValue)
                //.Case(SensorLookup.GetByName(SensorLookup.SUSPENSION), () => suspensionMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.SUSPENSION).MinValue)
                //.Case(SensorLookup.GetByName(SensorLookup.CURRENT), () => currentBarMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.CURRENT).MinValue)
                ;

            for (ushort i = 0; i < sensors.Length; i++)
            {
                //Get lastes wrapper
                wrapper = DataReciver.GetData(sensors[i]);

                if (wrapper != null)
                {
                    switcher.Switch(SensorLookup.GetById(sensors[i])).Run();
                }
            }
        }

        public override void Update(GameTime gameTime)
        {
            DoEvents();
            UpdateValues();
            label_Speed.TextColor = new SDLColor(200, 200, 200);
            KeyboardState state = Keyboard.GetState();
            if (state.IsKeyDown(Keys.SPECIAL1))
            {
                Keyboard.RemoveKey(Keys.SPECIAL1);
                scroller.CurrentPage--;
                guiLogger.WriteVerbose("Special key 1, page--");
            }
            if (state.IsKeyDown(Keys.SPECIAL2))
            {
                Keyboard.RemoveKey(Keys.SPECIAL2);
                scroller.CurrentPage++;
                guiLogger.WriteVerbose("Special key 2, page++");
            }

            if (newSpeedometer.Persent * maxSpeed < 9.5)
                label_Speed.Text = (newSpeedometer.Persent * maxSpeed).ToString("  0");

            else if (newSpeedometer.Persent * maxSpeed < 99.5)
                label_Speed.Text = (newSpeedometer.Persent * maxSpeed).ToString(" 00");

            else
                label_Speed.Text = (newSpeedometer.Persent * maxSpeed).ToString("000");
            if (scroller.CurrentPage == 3) //HACK: Should be able to name page and add update events to pages
            {
                UpdateListView();
            }

            scroller.Update(gameTime);


        }
        bool offline = false;
        private void UpdateListView()
        {
            if (!offline)
            {
                TcpClient client = new TcpClient();
                try
                {
                    client.Connect(new IPEndPoint(IPAddress.Loopback, NetPorts.SensorDataList));
                    BinaryReader reader = new BinaryReader(client.GetStream());

                }
                catch
                {
                    offline = true;
                }
            }
        }

        public override void Draw(GameTime gameTime)
        {
            renderer.Clear();
            scroller.Draw(renderer, gameTime);
            //renderer.DrawTexture(testTexture, new SDLRectangle(0, 0, testTexture.Width, testTexture.Height));
            renderer.Present();
        }
    }
}

