﻿using Ion.Data.Networking.CoreCom;
using Ion.Data.Networking.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.IonCore
{
    class Program
    {
        public const string Version = "0.2.0 Beta";
        public static bool running = true;
        static void Main(string[] args)
        {
            Console.Title = "Ion Core";
            LogManagerServer logServer = new LogManagerServer();
            logServer.OnVerbose += LogServer_OnVerbose;
            CommandListner listener = new CommandListner(new Commands(logServer.CreateLogger("CORECOM"), new SystemCore(logServer)));
            listener.Start();

            string line = "";
            try
            {
                while ((line = Console.ReadLine()) != "exit")
                {
                }
                running = false;
            }
            catch (Exception e)
            {
                Console.WriteLine("Console not available");
            }
            while (running)
            {
                System.Threading.Thread.Sleep(500);
            }
        }

        private static void LogServer_OnVerbose(object sender, LogWriteEventArgs e)
        {
            Console.WriteLine($"{e.Entry.Time} {e.Entry.Sender} {e.Entry.Level} {e.Entry.Value}");
        }
    }

    public class Commands
    {
        SystemCore core;
        ILogger logger;
        public Commands(ILogger logger, SystemCore core)
        {
            this.logger = logger;
            this.core = core;
        }
        public string Version()
        {
            return Program.Version;
        }

        public string Update(string updateFile)
        {
            core.UpdateManager.EnqueUpdate(updateFile);
            return "OK";
        }

        public string Register(string name, string id)
        {
            int idVal;
            if (int.TryParse(id, out idVal))
            {
                core.ProcessManager.Register(name, idVal);
                return "OK";
            }
            else
            {
                logger.WriteException("Process: " + name + " dose not contain an value", new Exception("Id not an number"), 3);
                return "NO ID";
            }

        }
    }

    public class SystemCore
    {
        public UpdateManager UpdateManager { get; private set; }// = new UpdateManager();
        public ProcessManager ProcessManager { get; private set; }// = new ProcessManager();

        public SystemCore(ILogManager manager)
        {
            UpdateManager = new UpdateManager(manager.CreateLogger("UPMAN"));
            ProcessManager = new ProcessManager(manager.CreateLogger("PROMAN"));
        }
    }
}
