﻿using Ion.Data.Networking.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.IonCore
{
    public class UpdateManager
    {
        string currentPath = null;
        private ILogger logger;

        public UpdateManager(ILogger logInterface)
        {
            this.logger = logInterface;
        }

        public void EnqueUpdate(string updateFile)
        {
            logger.WriteInfo("Enqueing update");
            currentPath = updateFile;
            Task.Run(new Action(Update));
        }

        public void Update()
        {
            System.Threading.Thread.Sleep(500);

            if (currentPath != null)
            {
                logger.WriteInfo("Starting update");
                FileInfo fi = new FileInfo(currentPath);
                Stream s = fi.OpenRead();
                ZipArchive archive = new ZipArchive(s);
                ZipArchiveEntry entry = archive.GetEntry("update.conf");
                TextReader tr = new StreamReader(entry.Open());
                logger.WriteInfo("Getting update info");
                string path = tr.ReadLine();
                string startProcess = tr.ReadLine();
                tr.Close();
                logger.WriteInfo("Extracting files");
                /*if (Directory.Exists(path))
                    Directory.Delete(path, true);
                Directory.CreateDirectory(path);*/
                archive.ExtractToDirectory(path);
                s.Close();
                logger.WriteInfo("Starting process");
                ProcessStartInfo info = new ProcessStartInfo(Path.Combine(path, startProcess));
                info.WorkingDirectory = path;
                Process.Start(info);
                logger.WriteInfo("Update complete");
            }
        }
    }
}
