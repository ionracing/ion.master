﻿using Ion.Data.Networking.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.IonCore
{
    public class ProcessManager
    {
        ILogger logger;
        public List<ProcessInformation> Processes { get; } = new List<ProcessInformation>();

        public ProcessManager(ILogger logger)
        {
            this.logger = logger;
        }

        public void Register(string name, int id)
        {
            Processes.Add(new ProcessInformation(id, name));
            logger.WriteInfo("Registered process: " + name + " with id: " + id.ToString());
            Process p = Process.GetProcessById(id);
            p.EnableRaisingEvents = true;
            p.Exited += ProcessManager_Exited;
            //Process.GetProcessById(id).Exited += ProcessManager_Exited;

        }

        private void ProcessManager_Exited(object sender, EventArgs e)
        {
            int processID = 0;
            for (int i = 0; i < Processes.Count; i++)
            {
                if (Processes[i].RunningId == (sender as Process).Id)
                {
                    processID = Processes[i].RunningId;
                    Processes.RemoveAt(i);
                    logger.WriteLine("Process with id: " + processID + " exited", "INFO", 2);
                    break;
                }
            }


        }
    }

    public class ProcessInformation
    {
        public int RunningId { get; set; }
        public string Name { get; set; }
        public Process ProcessObj => Process.GetProcessById(RunningId);

        public ProcessInformation(int id, string Name)
        {
            this.RunningId = id;
        }

    }

}
