﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ion.Data.Networking.NewStack;
using Ion.Data.Networking.Logging;
using System.Collections.Generic;

namespace Ion.Testing.NetworkTesting
{
    [TestClass]
    public class IDP_Tester
    {
        
        [ClassInitialize]
        public static void IDP_Setup(TestContext context)
        {
            NetworkManager.ThrowExceptions = true;
            LoopbackDataLink loopback = new LoopbackDataLink();
            NetworkManager.RegisterInterface(1, loopback);
        }

        [TestMethod]
        public void IDP_DataGram()
        {
            //Add defailt interface to IdpStack or NetworkManager as well?
            IdpClient client = new IdpClient(50) { DefaultInterface = 1 };
            IdpClient receiver = new IdpClient(51) {  DefaultInterface = 1 };
            
            client.Send(51, new byte[] { 0x01, 0x02, 0x03 });

            byte[] bytes = receiver.Receive();
            Assert.AreEqual(bytes.Length, 3, "Not received all data");
            Assert.AreEqual(bytes[0], 0x01, "Slot one data is inncorrect");
            Assert.AreEqual(bytes[1], 0x02, "Slot two data is inncorrect");
            Assert.AreEqual(bytes[2], 0x03, "Slot three data is inncorrect");
        }

        [TestMethod]
        public void IDP_DataGram_WrongInterface()
        {
            //Add defailt interface to IdpStack or NetworkManager as well?
            IdpClient client = new IdpClient(50) { DefaultInterface = 2 };
            IdpClient receiver = new IdpClient(51) { DefaultInterface = 2 };
            try
            {
                client.Send(51, new byte[] { 0x01, 0x02, 0x03 });
                byte[] bytes = receiver.Receive();
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(KeyNotFoundException), "Wrong error received");
            }
        }
    }
}
