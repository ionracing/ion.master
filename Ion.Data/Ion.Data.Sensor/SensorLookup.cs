﻿using Ion.Data.Networking.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Sensor
{
    public class SensorLookup
    {
        static List<SensorLookup> allLoockUps = new List<SensorLookup>();
        static List<ushort> sensorIDs = new List<ushort>();
        public static ushort[] SensorIDs
        {
            get
            {
                return sensorIDs.ToArray();
            }
        }

        public const string SPEED = "SPEED";

        public const string SOC = "SOC";
        public const string CURRENT = "CURRENT";
        public const string VOLTAGE = "VOLTAGE";
        public const string VOLTAGE12 = "VOLTAGE12";

        public const string TEMPBAT = "TEMPBAT";
        public const string TEMPCOOL = "TEMPCOOL";

        public const string DRVWHEEL = "DRVWHEEL";
        public const string SPEEDPEDDAL = "SPEEDPEDDAL";
        public const string BREAKPEDDAL = "BREAKPEDDAL";
        public const string SUSPENSION = "SUSPENSION";

        public const string GYROX = "GYROX";
        public const string GYROY = "GYROY";
        public const string GYROZ = "GYROZ";

        public const string ACCELX = "ACCELX";
        public const string ACCELY = "ACCELY";
        public const string ACCELZ = "ACCELZ";

        public const string TEMPMONO = "TEMPMONO";

        public const string MAGX = "MAGX";
        public const string MAGY = "MAGY";
        public const string MAGZ = "MAGZ";

        public const string BAR = "BAR";

        public const string GPSDEG = "GPSDEG";
        public const string GPSMIN = "GPSMIN";
        public const string LAT = "LAT";
        public const string LONG = "LONG";
        public const string HIGHRESGPS = "HIGHRESGPS";


        public const string PDLERR = "PDLERR";
        public const string STUPERR = "STUPERR";
        public const string MRC3ERR = "MRC3ERR";
        public const string MRC4ERR = "MRC4ERR";

        public const string BUTTONS = "BUTTONS";

        static SensorLookup()
        {
            //TODO: Delete get by name, and change names to int, so it is the id instead, maybe it is nessesary if changeing to Sensormap because that should be the link
            //TODO: Create Sensormap, so all this values are in JSON or config file, so it can be added or removed values, as well as backed up for some config files
            allLoockUps.Add(new SensorLookup(0, SPEED, "Speed", 12, 1, 0, 200));
            //allLoockUps.Add(new SensorLookup(1, SOC, "State Of Charge", 12, 0.2, 0, 100));
            //allLoockUps.Add(new SensorLookup(2, VOLTAGE, "Voltage", 12, 0.2, 0, 100));
            allLoockUps.Add(new SensorLookup(0x349, CURRENT, "Current", 12, 0.2, 0, 250));
            allLoockUps.Add(new SensorLookup(4, VOLTAGE12, "Voltage 12V battery", 12, 0.2, 0, 100));

            //allLoockUps.Add(new SensorLookup(5, TEMPBAT, "Battery Temperature",  12, 0.2, 0, 100));
            allLoockUps.Add(new SensorLookup(6, TEMPCOOL, "Coolant Temperature", 12, 0.2, 0, 100));
            allLoockUps.Add(new SensorLookup(0xD1, TEMPMONO, "Mono Temperature", 12, 0.2, true, 204.8));

            allLoockUps.Add(new SensorLookup(7, DRVWHEEL, "Drive Wheel", 12, 0));
            allLoockUps.Add(new SensorLookup(8, SPEEDPEDDAL, "Speed Peddal", 12, 0));
            allLoockUps.Add(new SensorLookup(9, BREAKPEDDAL, "Break Peddal", 12, 0));
            allLoockUps.Add(new SensorLookup(10, SUSPENSION, "Suspension", 12, 0));

            allLoockUps.Add(new SensorLookup(0x85, BUTTONS, "Buttons", 12, 1));

            allLoockUps.Add(new SensorLookup(0xC8, GYROX, "Gyroscoop X", 16, 0, true, 100));
            allLoockUps.Add(new SensorLookup(0xC9, GYROY, "Gyroscoop Y", 16, 0, true, 100));
            allLoockUps.Add(new SensorLookup(0xCA, GYROZ, "Gyroscoop Z", 16, 0, true, 100));

            allLoockUps.Add(new SensorLookup(0xCB, ACCELX, "Accelerometer X", 16, 0.2, true, 9.80665 * 4)); //9,80665 
            allLoockUps.Add(new SensorLookup(0xCC, ACCELY, "Accelerometer Y", 16, 0.2, true, 9.80665 * 4));
            allLoockUps.Add(new SensorLookup(0xCD, ACCELZ, "Accelerometer Z", 16, 0.2, true, 9.80665 * 4));

            allLoockUps.Add(new SensorLookup(0xCE, MAGX, "Magnetometer X", 16, 0, true, 100));
            allLoockUps.Add(new SensorLookup(0xCF, MAGY, "Magnetometer Y", 16, 0, true, 100));
            allLoockUps.Add(new SensorLookup(0xD0, MAGZ, "Magnetometer Z", 16, 0, true, 100));

            allLoockUps.Add(new SensorLookup(0xD2, BAR, "Barometer", 0xFFF));

            allLoockUps.Add(new SensorLookup(250, LAT, "Latitude", 24));
            allLoockUps.Add(new SensorLookup(251, LONG, "Longitude", 24));
            allLoockUps.Add(new SensorLookup(252, HIGHRESGPS, "High Res GPS", 24));


            allLoockUps.Add(new SensorLookup(100, PDLERR, "Peddal Error"));
            allLoockUps.Add(new SensorLookup(101, STUPERR, "Startup Error"));
            allLoockUps.Add(new SensorLookup(102, MRC3ERR, "Micro 3 Error"));
            allLoockUps.Add(new SensorLookup(103, MRC4ERR, "Micro 4 Error"));

            allLoockUps.Add(new SensorLookup(0x620, "RAW BMS 1", "RAW BMS 1"));
            allLoockUps.Add(new SensorLookup(0x621, "RAW BMS 2", "RAW BMS 2"));
            allLoockUps.Add(new SensorLookup(0x622, "RAW BMS 3", "RAW BMS 3"));
            allLoockUps.Add(new SensorLookup(0x623, "RAW BMS 4", "RAW BMS 4"));
            allLoockUps.Add(new SensorLookup(0x624, "RAW BMS 5", "RAW BMS 5"));
            allLoockUps.Add(new SensorLookup(0x625, "RAW BMS 6", "RAW BMS 6"));
            allLoockUps.Add(new SensorLookup(0x626, "RAW BMS 7", "RAW BMS 7"));
            allLoockUps.Add(new SensorLookup(0x627, "RAW BMS 8", "RAW BMS 8"));
            allLoockUps.Add(new SensorLookup(0x628, "RAW BMS 9", "RAW BMS 9"));

            allLoockUps.Add(new SensorLookup(0xF001, VOLTAGE, "Pack Voltage [V]", 16, 0.2, 0, ushort.MaxValue));
            allLoockUps.Add(new SensorLookup(0xF002, "Min Vtg", "Min Vtg [V]", 8, 0.2, 0, 25.5));
            allLoockUps.Add(new SensorLookup(0xF003, "Min Vtg ID", "Min Vtg ID", 8));
            allLoockUps.Add(new SensorLookup(0xF004, "Max vtg", "Max vtg [V]", 8, 0.2, 0, 25.5));
            allLoockUps.Add(new SensorLookup(0xF005, "Max Vtg ID", "Max Vtg ID", 8));

            allLoockUps.Add(new SensorLookup(0xF006, "Current BMS", "Current BMS[A]", 16, 0.2, true, short.MaxValue));
            allLoockUps.Add(new SensorLookup(0xF007, "Charge limit", "Charge limit", 16, 0, false, ushort.MaxValue));
            allLoockUps.Add(new SensorLookup(0xF008, "Discharge limit", "Discharge limit", 16, 0, false, ushort.MaxValue));

            allLoockUps.Add(new SensorLookup(0xF009, "Batt. energy in", "Batt. energy in", 32, 0, false, uint.MaxValue));
            allLoockUps.Add(new SensorLookup(0xF00A, "Batt. energy out", "Batt. energy out", 32, 0, false, uint.MaxValue));

            allLoockUps.Add(new SensorLookup(0xF00B, SOC, "SOC", 8, 0.2, 0, 100));
            allLoockUps.Add(new SensorLookup(0xF00C, "DOD", "DOD", 16, 0, false, ushort.MaxValue));
            allLoockUps.Add(new SensorLookup(0xF00D, "Capacity", "Capacity", 16, 0, false, ushort.MaxValue));
            allLoockUps.Add(new SensorLookup(0xF00E, "00h", "00h"));
            allLoockUps.Add(new SensorLookup(0xF00F, "SOH", "SOH", 8, 0, 0, 100));

            allLoockUps.Add(new SensorLookup(0xF010, TEMPBAT, "Battery Temperature", 8, 0.2, true, sbyte.MaxValue));
            allLoockUps.Add(new SensorLookup(0xF011, "Min tmp", "Min tmp", 8, 0.2, true, sbyte.MaxValue));
            allLoockUps.Add(new SensorLookup(0xF012, "Min tmp ID", "Min tmp ID"));
            allLoockUps.Add(new SensorLookup(0xF013, "Max tmp", "Max tmp", 8, 0.2, true, sbyte.MaxValue));
            allLoockUps.Add(new SensorLookup(0xF014, "Max tmp ID", "Max tmp ID"));

            allLoockUps.Add(new SensorLookup(0xF015, "Pack resistance", "Pack resistance", 16, 0, false, 6553.5));
            allLoockUps.Add(new SensorLookup(0xF016, "Min res", "Min res", 8, 0, 0, 25.5));
            allLoockUps.Add(new SensorLookup(0xF017, "Min resID", "Min resID", 8));
            allLoockUps.Add(new SensorLookup(0xF018, "Max res", "Max res", 8, 0, 0, 25.5));
            allLoockUps.Add(new SensorLookup(0xF019, "Max resID", "Max resID", 8));

            allLoockUps.Add(new SensorLookup(0x4B2, "Break Light", "Break Light", 8));

            allLoockUps.Add(new SensorLookup(0x328, "Break Force Front", "Break Force Front", 12));
            allLoockUps.Add(new SensorLookup(0x329, "Break Force Back", "Break Force Back", 12));

            allLoockUps.Add(new SensorLookup(0x37B, "Mono EngineRoom", "Mono EngineRoom", 12));
            allLoockUps.Add(new SensorLookup(0x37C, "Mono MainArea", "Mono MainArea", 12));

            allLoockUps.ForEach(x => sensorIDs.Add(x.ID));
        }

        public static SensorLookup GetById(ushort id)
        {
            return allLoockUps.Find(x => x.ID == id);
        }

        public static SensorLookup GetByName(string name)
        {
            return allLoockUps.Find(x => x.Name.ToUpper() == name.ToUpper());
        }

        public SensorLookup(ushort id, string name)
            : this(id, name, null)
        {
        }

        public SensorLookup(ushort id, string name, string displayName)
            : this(id, name, displayName, -1, -1)
        {

        }

        public SensorLookup(ushort id, string name, string displayName, int resoluton)
            : this(id, name, displayName, resoluton, 1)
        {

        }

        public SensorLookup(ushort id, string name, string displayName, int resolution, double importance)
        {
            this.ID = id;
            this.Name = name;
            this.displayName = displayName;
            this.Resolution = resolution;
            this.Importance = importance;
        }

        public SensorLookup(ushort id, string name, string displayName, int resolution, double importance, double minValue, double maxValue)
            : this(id, name, displayName, resolution, importance)
        {
            this.MinValue = minValue;
            this.MaxValue = maxValue;
        }

        public SensorLookup(ushort id, string name, string displayName, int resolution, double importance, bool signed, double maxValue)
            : this(id, name, displayName, signed ? resolution - 1 : resolution, importance)
        {
            this.MinValue = -maxValue;
            this.MaxValue = maxValue;
            this.Signed = signed;
        }


        public double Importance { get; private set; }
        public int Resolution { get; private set; }
        public double MaxValue { get; private set; }
        public double MinValue { get; private set; }

        public bool Signed { get; private set; }
        public ushort ID { get; private set; }
        public string Name { get; private set; }
        string displayName;
        public string DisplayName
        {
            get
            {
                return displayName ?? Name;
            }
            set
            {
                displayName = value;
            }
        }

        public double ConvertValue(int rawValue)
        {
            if (MaxValue == MinValue)
                return rawValue;
            else if (Signed)
                return ConvertToPercent(rawValue) * (MaxValue);
            else
                return ConvertToPercent(rawValue) * (MaxValue - MinValue) + MinValue;
        }

        public double ConvertToPercent(int rawValue)
        {
            if (Signed)
            {
                int shift = 0;
                if (rawValue >> Resolution > 0)
                    shift = (-1 << (Resolution));
                return ((double)((shift | rawValue)) / (double)((1 << (Resolution)) - 1));

            }
            else
                return ((double)(rawValue) / (double)((1 << (Resolution)) - 1));
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(int))
            {
                return (int)obj == ID;
            }
            else if (obj.GetType() == typeof(SensorLookup))
            {
                return ((SensorLookup)obj).ID == ID;
            }
            else
                return obj.Equals(this);
        }

        public override int GetHashCode()
        {
            return ID;
        }
    }

    public class NewSensorLookup
    {
        public ushort ID { get; private set; }
        public string Name { get; private set; }
        string displayName;
        public string DisplayName
        {
            get
            {
                return displayName ?? Name;
            }
            set
            {
                displayName = value;
            }
        }

        public byte Resolution { get; set; }
        public double Importance { get; private set; }
        public double MaxValue { get; private set; }
        public double MinValue { get; private set; }
        public double ValDif { get { return MaxValue - MinValue; } }
        public bool Signed { get; private set; }

        public NewSensorLookup(ushort id, string name, string displayName, bool signed, byte resolution, double importance, double minValue, double maxValue)
        {
            this.ID = id;
            this.Name = name;
            this.DisplayName = displayName;
            this.Signed = signed;
            this.Resolution = resolution;
            this.Importance = importance;
            this.MinValue = minValue;
            this.MaxValue = maxValue;
        }
    }

    public static class DataWrapperExtension
    {
        public static double CalculateValue(this DataWrapper wrapper)
        {
            SensorLookup lookup = SensorLookup.GetById(wrapper.SensorID);
            if (lookup != null)
                return lookup.ConvertValue(wrapper.Value);
            else
                return wrapper.Value;
        }

        public static double CalculatePercent(this DataWrapper wrapper)
        {
            SensorLookup lookup = SensorLookup.GetById(wrapper.SensorID);
            if (lookup != null)
                return lookup.ConvertToPercent(wrapper.Value);
            else
                return (double)wrapper.Value / int.MaxValue;
        }
    }
}
