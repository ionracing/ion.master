﻿using System;
using System.IO;

public class PackageInfo
{
    public int ID { get; private set; }
    public long LastValue { get; private set; }
    public DateTime LastRegister { get; private set; }
    public TimeSpan LastDiff { get; private set; }
    public TimeSpan AvgDiff { get; private set; }
    public int Count { get; private set; }

    public PackageInfo(int sensorID)
    {
        this.ID = sensorID;
    }

    public void RegisterValue(long value)
    {
        this.LastValue = value;
        Increment();
    }

    private void Increment()
    {
        DateTime last = LastRegister;
        LastRegister = DateTime.Now;
        LastDiff = LastRegister - last;
        double dCount = Count;
        if (Count > 1)
            AvgDiff = TimeSpan.FromMilliseconds(AvgDiff.TotalMilliseconds * (dCount / (dCount + 1)) + LastDiff.TotalMilliseconds / (dCount + 1));
        else if (Count == 1)
            AvgDiff = LastDiff;

        Count++;
    }

    public void ToBBinaryWriter(BinaryWriter writer)
    {
        writer.Write(ID);
        writer.Write(LastValue);
        writer.Write(LastRegister.ToBinary());
        writer.Write(LastDiff.TotalMilliseconds);
        writer.Write(AvgDiff.TotalMilliseconds);
        writer.Write(Count);
    }

    public static PackageInfo FromBinaryReader(BinaryReader reader)
    {
        PackageInfo info = new PackageInfo(reader.ReadInt32());
        info.LastValue = reader.ReadInt64();
        info.LastRegister = DateTime.FromBinary(reader.ReadInt64());
        info.LastDiff = TimeSpan.FromMilliseconds(reader.ReadDouble());
        info.AvgDiff = TimeSpan.FromMilliseconds(reader.ReadDouble());
        info.Count = reader.ReadInt32();
        return info;
    }
}