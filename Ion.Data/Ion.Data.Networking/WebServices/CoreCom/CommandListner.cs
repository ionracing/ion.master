﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.CoreCom
{
    public class CommandListner
    {
        TcpListener listener = new TcpListener(IPAddress.Any, NetPorts.CoreComServer);
        Dictionary<string, MethodInfo> methodInfo = new Dictionary<string, MethodInfo>();
        object comObject;
        public CommandListner(object comObject)
        {
            this.comObject = comObject;
            foreach (MethodInfo mi in comObject.GetType().GetMethods())
            {
                methodInfo.Add(mi.Name, mi);
            }
            
        }

        public void Start()
        {
            listener.Start();
            AcceptClients();
        }

        private async void AcceptClients()
        {
            TcpClient client = await listener.AcceptTcpClientAsync();
            AcceptClients();
            try
            {
                BinaryReader reader = new BinaryReader(client.GetStream());
                BinaryWriter writer = new BinaryWriter(client.GetStream());
                string metName = "";
                while ((metName = reader.ReadString()) != "exit")
                {
                    int parNum = reader.ReadInt32();
                    List<string> paramanters = new List<string>();
                    for (int i = 0; i < parNum; i++)
                    {
                        paramanters.Add(reader.ReadString());
                    }
                    if (methodInfo.ContainsKey(metName))
                    {
                        MethodInfo info = methodInfo[metName];
                        object o = info.Invoke(comObject, paramanters.ToArray());
                        string s = o as string ?? "";
                        
                        writer.Write(s);
                        writer.Flush();
                    }
                }
                reader.Close();
                client.Close();
            }
            catch
            {

            }
        }
    }
}
