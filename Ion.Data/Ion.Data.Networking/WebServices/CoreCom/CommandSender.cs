﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.CoreCom
{
    public class CommandSender
    {
        TcpClient client = new TcpClient();
        BinaryReader reader;// = new BinaryReader(client.GetStream());
        BinaryWriter writer;// = new BinaryWriter(client.GetStream());

        public CommandSender()
        {
            client.Connect(IPAddress.Loopback, NetPorts.CoreComServer);
            reader = new BinaryReader(client.GetStream());
            writer = new BinaryWriter(client.GetStream());
        }

        public string SendCommand(string name, params string[] parameters)
        {
            writer.Write(name);
            writer.Write(parameters.Length);
            foreach (string s in parameters)
            {
                writer.Write(s);
            }
            return reader.ReadString();
        }

        public void Close()
        {
            writer.Write("exit");
            writer.Close();
            client.Close();
        }
    }
}
