﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.CoreCom
{
    public class CoreInterface
    {
        CommandSender sender;

        public CoreInterface(CommandSender sender)
        {
            this.sender = sender;
        }

        public string Register(string name, string processId)
        {
            return sender.SendCommand(nameof(Register), name, processId);
        }

        public string Update(string updateFile)
        {
            return sender.SendCommand(nameof(Update), updateFile);
        }

        public string Version()
        {
            return sender.SendCommand(nameof(Version), new string[0]);
        }

        public void Close()
        {
            sender.Close();
        }
    }
}
