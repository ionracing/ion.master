﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

/*
This file is taken from NicroWare NetUtility
© Nicolas Fløysvik

*/

namespace NicroWare.Lib.NetUtility.Dhcp
{
    //https://www.ietf.org/rfc/rfc2131.txt DHCP

    public class DHCPServer
    {
        public List<IPAddress> RouterIP { get; set; }
        public IPAddress SubnetMask { get; set; } = new IPAddress(new byte[] { 255, 255, 255, 0 });
        public AdvIPAddress DHCPServerIP { get; set; } = new AdvIPAddress(new byte[] { 10, 0, 0, 1 }, 24);
        public List<IPAddress> DNSServers { get; private set; } = new List<IPAddress>(new IPAddress[] { new IPAddress(new byte[] { 10, 0, 2, 1 }) });
        //TODO: Add some random fuss around T1 and T2 timing (renewtime and rebind time)
        public uint LeaseTime { get; set; } = 86400; //86400 Secounds = 1 day
        private UdpClient dhcpClient;
        public static bool Running { get; private set; } = false;
        Dictionary<uint, IPAddress> sessions = new Dictionary<uint, IPAddress>();

        int nextIP = 3;
        public bool SingleAddressMode { get; set; } = false;

        //TODO: Have an array of aviable ip addresses

        public DHCPServer()
        {
            if (Running)
                throw new Exception("An DHCP server is already running on this host");
        }

        public DHCPServer(AdvIPAddress routerIP, List<IPAddress> DNSServers, int startIP)
        {
            this.DHCPServerIP = routerIP;
            this.SubnetMask = routerIP.FullSubnetMaks;
            this.DNSServers = DNSServers;
            this.nextIP = startIP;
        }

        public void Start()
        {
            dhcpClient = new UdpClient(new IPEndPoint(IPAddress.Any, 67));
            dhcpClient.EnableBroadcast = true;
            StartListening();
        }

        private async void StartListening()
        {
            UdpReceiveResult result = await dhcpClient.ReceiveAsync();
            StartListening();

            DHCPMessage message = DHCPMessage.FromByteArray(result.Buffer);

            DHCPMessage respons = null;
            switch (message.MessageType)
            {
                case DHCPMessageType.DHCPDISCOVER:
                    respons = CreateDHCPOffer(message);
                    break;
                case DHCPMessageType.DHCPREQUEST:
                    respons = CreateDHCPAcknowledg(message);
                    break;
                default:
                    Console.WriteLine("Got message of type: " + message.MessageType + " from: " + message.ClientHardwareAddressName);
                    break;
            }
            if (respons == null)
            {
                Console.WriteLine("Respons was not generated");
                return;
            }
            byte[] responsBytes = respons.ToByteArray();
            dhcpClient.Send(responsBytes, responsBytes.Length, new IPEndPoint(IPAddress.Broadcast, 68));
        }

        private DHCPMessage CreateDHCPOffer(DHCPMessage message)
        {
            IPAddress sessionIP;
            if (sessions.ContainsKey(message.TransactionID))
                sessionIP = sessions[message.TransactionID];
            else
            {
                //sessionIP = new IPAddress(new byte[] { 10, 0, 2, (byte)nextIP });
                sessionIP = DHCPServerIP.NetworkAddress + (SingleAddressMode ? nextIP : nextIP++);
                sessions.Add(message.TransactionID, sessionIP);
            }
            Console.WriteLine("Got Discover from: " + message.ClientHardwareAddressName + " assigning address: " + sessionIP.ToString());

            DHCPMessage respons = DHCPMessage.CreateOffer(message, sessionIP, DHCPServerIP, new DHCPOption[]
                {
                            new DHCPOption(DHCPOptionTags.DHCPMessageType, new byte[] { (byte)DHCPMessageType.DHCPOFFER }),
                            new DHCPOption(DHCPOptionTags.SubnetMask, SubnetMask.GetAddressBytes()),

                            //Times
                            new DHCPOption(DHCPOptionTags.RenewalTime, BitUtils.GetBytes((int)(LeaseTime * 0.5))),
                            new DHCPOption(DHCPOptionTags.RebindingTime, BitUtils.GetBytes((int)(LeaseTime * 0.875))),
                            new DHCPOption(DHCPOptionTags.AddressTime, BitUtils.GetBytes(LeaseTime)),

                            new DHCPOption(DHCPOptionTags.DHCPServerId, DHCPServerIP.GetAddressBytes()),
                            new DHCPOption(DHCPOptionTags.Router, DHCPServerIP.GetAddressBytes()),
                            new DHCPOption(DHCPOptionTags.DomainServer, DNSServers[0].GetAddressBytes()),
                });
            
            Console.WriteLine("Sending offer");
            return respons;
        }

        private DHCPMessage CreateDHCPAcknowledg(DHCPMessage message)
        {
            IPAddress sessionIP;
            if (sessions.ContainsKey(message.TransactionID))
                sessionIP = sessions[message.TransactionID];
            else
            {
                sessionIP = IPAddress.Any;
                foreach (DHCPOption option in message.Options)
                {
                    if (option.Code == DHCPOptionTags.AddressRequest)
                    {
                        sessionIP = new IPAddress((byte[])option.Data);
                        sessions.Add(message.TransactionID, sessionIP);
                        break;
                    }
                }
                if (sessionIP == IPAddress.Any)
                    throw new Exception("Some random exception");
            }
            //    throw new NotImplementedException("Renew is currently not implemented");
            Console.WriteLine("Got Request from: " + message.ClientHardwareAddressName + " assigned address: " + sessionIP.ToString());
            DHCPMessage respons = DHCPMessage.CreateAcknowledg(message, sessionIP, DHCPServerIP, new DHCPOption[]
                {
                            new DHCPOption(DHCPOptionTags.DHCPMessageType, new byte[] { (byte)DHCPMessageType.DHCPACK }),
                            new DHCPOption(DHCPOptionTags.SubnetMask, SubnetMask.GetAddressBytes()),

                            //Times
                            new DHCPOption(DHCPOptionTags.RenewalTime, BitUtils.GetBytes((int)(LeaseTime * 0.5))),
                            new DHCPOption(DHCPOptionTags.RebindingTime, BitUtils.GetBytes((int)(LeaseTime * 0.875))),
                            new DHCPOption(DHCPOptionTags.AddressTime, BitUtils.GetBytes(LeaseTime)),

                            new DHCPOption(DHCPOptionTags.DHCPServerId, DHCPServerIP.GetAddressBytes()),
                            new DHCPOption(DHCPOptionTags.Router, DHCPServerIP.GetAddressBytes()),
                            new DHCPOption(DHCPOptionTags.DomainServer, DNSServers[0].GetAddressBytes()),
                });
            sessions.Remove(message.TransactionID);
            Console.WriteLine("Sending ack");
            return respons;
        }
    }

    public class DHCPSession
    {

    }

    public class DHCPMessage
    {
        byte op;        //Op code (1 = Bootrequest, 2 = Bootreplay)
        byte htype;     //Hardware address type (e.g. '1' = 10mb ethernet);
        byte hlen;      //Hardware address length e.g. '6' for 10mb ethernet;
        byte hops;      //Optionaly set by relay client
        uint xid;       //Transaction ID (randome number choosen by client)
        short secs;     //Filled by client, secounds elapsed since client began address acqusition
        short flags;    //Flags B0000000000000000 -> B broadcast flag, other to zero for future use
        uint ciaddr;    //Client IP address, only filled in if client is in BOUND, RENEW or REBINDING
        uint yiaddr;    //"Your" (client) IP address
        uint siaddr;    //IP address of next server to use in bootstrap;
        uint giaddr;    //Relay agent IP address, used in booting via relay agent.
        byte[] chaddr;  //Client hardware address
        string sname;   //Optinal server host name, null terminated string.

        public DHCPMessageType MessageType
        {
            get
            {
                foreach (DHCPOption option in options)
                {
                    if (option.Code == DHCPOptionTags.DHCPMessageType)
                    {
                        return (DHCPMessageType)option.Data;
                    }
                }
                return DHCPMessageType.DHCPDISCOVER;
            }
        }

        string file;    //Boot file name, null terminated string; "generic" name or null in DHCPDISCOVER, fully qualified directory-path name in DHCPOFFER.

        uint magiccoockie;   //A magic coockie
        DHCPOption[] options;

        public HardwareType HardwareType => (HardwareType)htype;
        public int HardwareAddressLength => hlen;
        public uint TransactionID => xid;
        public int ElapsedSecounds => secs;
        public IPAddress ClientIPAddress => new IPAddress(ciaddr);
        public IPAddress YourIPAddress => new IPAddress(yiaddr);
        public IPAddress ServerIPAddress => new IPAddress(siaddr);
        public IPAddress GatewayIPAddress => new IPAddress(giaddr);

        public string ClientHardwareAddressName => BitConverter.ToString(chaddr, 0, hlen);

        public uint MagicCookie => magiccoockie;

        public DHCPOption[] Options => options;



        public const uint MagicCookieConst = 1666417251; //0x63825363 // 1669485411?

        public DHCPMessage()
        {
            chaddr = new byte[16];
            options = new DHCPOption[0];
        }

        public static DHCPMessage CreateTestDiscover()
        {
            DHCPMessage message = new DHCPMessage();
            message.op = 1;
            message.htype = (byte)HardwareType.Ethernet10Mb;
            message.hlen = 6;
            message.xid = 432252532;
            message.secs = 0;
            message.flags = 0;
            message.magiccoockie = MagicCookieConst;
            return message;
        }

        public static DHCPMessage FromByteArray(byte[] input)
        {
            DHCPMessage message = new DHCPMessage();
            message.op = input[0];
            message.htype = input[1];
            message.hlen = input[2];
            message.hops = input[3];
            message.xid = BitConverter.ToUInt32(input, 4);
            message.secs = BitConverter.ToInt16(input, 8);
            message.flags = BitConverter.ToInt16(input, 10);



            message.ciaddr = BitConverter.ToUInt32(input, 12);
            message.yiaddr = BitConverter.ToUInt32(input, 16);
            message.siaddr = BitConverter.ToUInt32(input, 20);
            message.giaddr = BitConverter.ToUInt32(input, 24);

            message.chaddr = new byte[16];
            Array.Copy(input, 28, message.chaddr, 0, 16);
            message.sname = BitConverter.ToString(input, 44, 64);
            message.file = BitConverter.ToString(input, 108, 128);
            message.magiccoockie = BitConverter.ToUInt32(input, 236);
            int counter = 240;
            List<DHCPOption> allOptions = new List<DHCPOption>();
            while (input[counter] != 255)
            {
                DHCPOption option;
                allOptions.Add(option = DHCPOption.ReadFromArray(input, counter));
                counter += 2 + option.Length;
            }

            message.options = allOptions.ToArray();
            return message;

        }

        public static DHCPMessage CreateOffer(DHCPMessage dicover, IPAddress yourAddress, IPAddress serverAddress, DHCPOption[] options)
        {
            DHCPMessage message = new DHCPMessage();

            message.magiccoockie = MagicCookieConst;
            message.xid = dicover.xid;
            message.secs = dicover.secs;
            message.flags = dicover.flags;
            message.chaddr = dicover.chaddr;
            message.hlen = dicover.hlen;
            message.op = 2;
            message.htype = dicover.htype;
            message.siaddr = BitConverter.ToUInt32(serverAddress.GetAddressBytes(), 0);
            message.yiaddr = BitConverter.ToUInt32(yourAddress.GetAddressBytes(), 0);
            message.options = options;

            return message;

        }

        public static DHCPMessage CreateAcknowledg(DHCPMessage request, IPAddress yourAddress, IPAddress serverAddress, DHCPOption[] options)
        {
            DHCPMessage message = new DHCPMessage();

            message.magiccoockie = MagicCookieConst;
            message.xid = request.xid;
            message.secs = request.secs;
            message.flags = request.flags;
            message.chaddr = request.chaddr;
            message.hlen = request.hlen;
            message.op = 2;
            message.htype = request.htype;
            message.siaddr = BitConverter.ToUInt32(serverAddress.GetAddressBytes(), 0);
            message.yiaddr = BitConverter.ToUInt32(yourAddress.GetAddressBytes(), 0);
            message.options = options;

            return message;
        }

        public byte[] ToByteArray()
        {
            List<byte> allBytes = new List<byte>();
            allBytes.Add(op);
            allBytes.Add(htype);
            allBytes.Add(hlen);
            allBytes.Add(hops);
            allBytes.AddRange(BitConverter.GetBytes(xid));
            allBytes.AddRange(BitConverter.GetBytes(secs));
            allBytes.AddRange(BitConverter.GetBytes(flags));
            allBytes.AddRange(BitConverter.GetBytes(ciaddr));
            allBytes.AddRange(BitConverter.GetBytes(yiaddr));
            allBytes.AddRange(BitConverter.GetBytes(siaddr));
            allBytes.AddRange(BitConverter.GetBytes(giaddr));
            allBytes.AddRange(chaddr);
            allBytes.AddRange(new byte[64]);
            allBytes.AddRange(new byte[128]);
            allBytes.AddRange(BitConverter.GetBytes(magiccoockie));

            foreach (DHCPOption option in options)
            {
                allBytes.AddRange(option.ToByteArray());
            }

            allBytes.Add(255);
            return allBytes.ToArray();
        }
    }

    public enum DHCPOptionTags : int
    {
        Unknown = -1,
        Pad = 0,
        SubnetMask = 1,
        Router = 3,
        TimeServer = 4,
        NameServer = 5,
        DomainServer = 6,
        LogServer = 7,
        QuotesServer = 8,
        LRPServer = 9,
        ImpressServer = 10,
        RLPServer = 11,
        Hostname = 12,
        DomainName = 15,

        RouterDiscovery = 31,
        RouterRequest = 32,
        StaticRoute = 33,

        VendorSpecific = 43,
        NETBIOSNameServer = 44,
        NETBIOSDistServer = 45,
        NETBIOSNodeType = 46,
        NETBIOSScope = 47,
        XWindowFont = 48,
        XWindowManager = 49,
        AddressRequest = 50,
        AddressTime = 51,
        Overload = 52,
        DHCPMessageType = 53,
        DHCPServerId = 54,
        ParameterList = 55,
        DHCPMessage = 56,
        DHCPMaxMessageSize = 57,
        RenewalTime = 58,
        RebindingTime = 59,
        ClassId = 60,
        ClientId = 61,

        ClasslessStaticRouteOption = 121,
    }

    public class DHCPOption
    {
        protected byte code;
        public DHCPOptionTags Code => (DHCPOptionTags)code;
        protected byte length;
        public byte Length { get { return length; } }
        protected byte[] fields;
        public object Data
        {
            get
            {
                switch (Code)
                {
                    case DHCPOptionTags.Hostname: //12
                        return Encoding.Default.GetString(fields);
                    case DHCPOptionTags.DHCPMessageType: //53
                        return (DHCPMessageType)fields[0];
                    case DHCPOptionTags.ParameterList: // 55
                        List<DHCPOptionTags> parametersListTags = new List<DHCPOptionTags>();
                        foreach (byte b in fields)
                        {
                            parametersListTags.Add((DHCPOptionTags)b);
                        }
                        return parametersListTags.ToArray();
                    case DHCPOptionTags.ClientId: //61
                        return BitConverter.ToString(fields);
                    default:
                        return fields;
                }
            }
        }

        public DHCPOption()
        {
        }

        public DHCPOption(DHCPOptionTags tag, byte[] data)
        {
            this.code = (byte)tag;
            this.fields = data;
            length = (byte)data.Length;
        }

        public static DHCPOption ReadFromArray(byte[] byteArray, int startIndex)
        {
            DHCPOption option = new DHCPOption();
            option.code = byteArray[startIndex];
            option.length = byteArray[startIndex + 1];
            option.fields = new byte[option.length];
            Array.Copy(byteArray, startIndex + 2, option.fields, 0, option.length);
            return option;
        }

        public byte[] ToByteArray()
        {
            List<byte> bytes = new List<byte>();
            bytes.Add(code);
            bytes.Add(length);
            bytes.AddRange(fields);
            return bytes.ToArray();
        }
    }

    // TODO: Write DHCP implementation
    /*public class DHCPDiscover
    {
        byte op;
        byte htype;
        byte hlen;
        byte hops;
        uint xid;
        ushort secs;
        ushort flags;
        int ciaddr; // Client IP Address
        int yiaddr; // Your IP Address
        int siaddr; // Server IP Address
        int giaddr; // Gateway ip address
        int[] chaddr; //Client hardware address
        int MagicCookie;
    }*/

    //http://www.iana.org/assignments/arp-parameters/arp-parameters.xhtml#arp-parameters-2
    public enum HardwareType : byte
    {
        Reserved = 0,
        Ethernet10Mb = 1,
        ExpEthernet3Mb = 2,
        AmatureRadioAX25 = 3,
        ProteonProNETTokenRing = 4,
        Chaos = 5,
        IEEE802Networks = 6,
        ARCNET = 7,
        Hyperchannel = 8,
        Lanstar = 9,
        AutonetShortAddress = 10,
        LocalTalk = 11,
        LocalNet_IBM = 12,
        UltraLink = 13,
        SMDS = 14,
        FrameRelay = 15,
        AsyncTransmissionMode = 16,
        HDLC = 17,
        FiberChannel = 18,
        AsyncTransmissionMode2 = 19,
        SerialLine = 20,
        AsyncTransmissionMode3 = 21,
        MIL_STD_188_220 = 22,
        Metricom = 23,
        IEEE1394_1995 = 25,
        MAPOS = 25,
        Twinaxial = 26,
        EUI_64 = 27,
        HIPARP = 28,
        IP_ARP_ISO_7816_3 = 29,
        ARPSec = 30,
        IPSec = 31,
        InfiniBand = 32,
        TIA_102_Project_25_CommonAirInterface = 33,
        WiegandInterface = 34,
        PureIP = 35,
        HW_EXP1 = 36,
        HFI = 37,
        Unknown
    }


    public enum DHCPMessageType : byte
    {
        DHCPDISCOVER = 1,
        DHCPOFFER = 2,
        DHCPREQUEST = 3,
        DHCPDECLINE = 4,
        DHCPACK = 5, //Accept
        DHCPNAK = 6, //NotAccept
        DHCPRELEASE = 7,
        DHCPINFORM = 8,
        DHCPFORCERENEW = 9,
        DHCPLEASEQUERY = 10,
        DHCPLEASEUNASSIGNED = 11,
        DHCPLEASEUNKNOWN = 12,
        DHCPLEASEACTIVE = 13,
        DHCPBULKLEASEQUERY = 14,
        DHCPLEASEQUERYDONE = 15,
    }

    public static class BitUtils
    {
        public static bool UseBigEndian { get; set; } = false;

        public static byte[] GetBytes(short value)
        {
            if (UseBigEndian)
                return new byte[] { (byte)(value >> 8), (byte)(value) };
            else
                return new byte[] { (byte)(value), (byte)(value >> 8) };
        }

        public static byte[] GetBytes(ushort value)
        {
            if (UseBigEndian)
                return new byte[] { (byte)(value >> 8), (byte)(value) };
            else
                return new byte[] { (byte)(value), (byte)(value >> 8) };
        }

        public static byte[] GetBytes(int value)
        {
            if (UseBigEndian)
                return new byte[] { (byte)(value >> 24), (byte)(value >> 16), (byte)(value >> 8), (byte)(value) };
            else
                return new byte[] { (byte)(value), (byte)(value >> 8), (byte)(value >> 16), (byte)(value >> 24) };
        }

        public static byte[] GetBytes(uint value)
        {
            if (UseBigEndian)
                return new byte[] { (byte)(value >> 24), (byte)(value >> 16), (byte)(value >> 8), (byte)(value) };
            else
                return new byte[] { (byte)(value), (byte)(value >> 8), (byte)(value >> 16), (byte)(value >> 24) };
        }
    }

    public class AdvIPAddress : IPAddress
    {
        public int SubnetMaskLength { get; set; }

        public AdvIPAddress FullSubnetMaks
        {
            get
            {
                byte[] bytes = new byte[this.GetAddressBytes().Length];
                for (int i = 0; i < SubnetMaskLength; i++)
                {
                    bytes[i / 8] |= (byte)(0x1 << (7 - (i % 8)));
                }
                return new AdvIPAddress(bytes);
            }
        }

        public AdvIPAddress NetworkAddress
        {
            get
            {
                byte[] baseBytes = this.GetAddressBytes();
                byte[] bytes = new byte[baseBytes.Length];
                
                for (int i = 0; i < SubnetMaskLength; i++)
                {
                    bytes[i / 8] |= (byte)(((baseBytes[i / 8] >> (7 - (i % 8))) & 0x1) << (7 - (i % 8)));
                }
                return new AdvIPAddress(bytes);
            }
        }

        public AdvIPAddress InterfaceAddress
        {
            get
            {
                byte[] baseBytes = this.GetAddressBytes();
                byte[] bytes = new byte[baseBytes.Length];

                for (int i = SubnetMaskLength; i < baseBytes.Length * 8; i++)
                {
                    bytes[i / 8] |= (byte)(((baseBytes[i / 8] >> (7 - (i % 8))) & 0x1) << (7 - (i % 8)));
                }
                return new AdvIPAddress(bytes);
            }
        }

        public int TotalAddresses
        {
            get
            {
                return 0x1 << (base.GetAddressBytes().Length * 8 - SubnetMaskLength);
            }
        }

        public int AvailableAddresses
        {
            get
            {
                return TotalAddresses - 2;
            }
        }

        public AdvIPAddress(byte[] address)
            : base(address)
        {
        }

        public AdvIPAddress(byte[] address, int netMask)
            : this(address)
        {
            this.SubnetMaskLength = netMask;
        }

        public AdvIPAddress(long newAddress)
            : base(newAddress)
        {
        }

        public AdvIPAddress(byte[] address, long scopeid)
            : base(address, scopeid)
        {
        }

        public static AdvIPAddress operator +(AdvIPAddress address, int incremet)
        {
            byte[] bytes = address.GetAddressBytes();
            if (bytes.Length > 0)
            {
                int curByte = incremet;
                int counter = bytes.Length - 1;
                int invers = 0;
                while (curByte != 0 && counter >= 0)
                {
                    curByte += bytes[counter];
                    bytes[counter] = (byte)curByte;
                    invers++;
                    curByte = (curByte >> invers * 8);
                    counter--;

                }
            }
            return new AdvIPAddress(bytes, address.SubnetMaskLength);
        }

        public static AdvIPAddress operator -(AdvIPAddress address, int decrement)
        {
            byte[] bytes = address.GetAddressBytes();
            if (bytes.Length > 0)
            {
                int curByte = -decrement;
                int counter = bytes.Length - 1;
                int invers = 0;
                while (curByte != 0 && counter >= 0)
                {
                    curByte += bytes[counter];
                    bytes[counter] = (byte)curByte;
                    invers++;
                    curByte = (curByte >> invers * 8);
                    counter--;

                }
            }
            return new AdvIPAddress(bytes, address.SubnetMaskLength);
        }
    }
}
