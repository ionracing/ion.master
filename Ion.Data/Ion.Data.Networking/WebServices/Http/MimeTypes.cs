﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.Http
{
    public class MimeTypes
    {
        public static List<MimeType> AllMimeTypes { get; private set; } = new List<MimeType>(
            new MimeType[] 
            {
                new MimeType(".png", "image/png")
                , new MimeType(".jpg", "image/jpeg")
                , new MimeType(".jpeg", "image/jpeg")
                , new MimeType(".gif", "image/gif")

                , new MimeType(".pdf", "application/pdf")
                , new MimeType(".log", "application/log")
                , new MimeType(".js", "application/javascript")
                , new MimeType(".json", "application/json")
                , new MimeType(".xml", "application/xml")
                
                , new MimeType(".zip", "application/zip")

                , new MimeType(".css", "text/css")
                , new MimeType(".htm", "text/html")
                , new MimeType(".html", "text/html")
                , new MimeType(".csv", "text/csv")

                , new MimeType(".doc", "application/msword")
                , new MimeType(".ppt", "application/vnd.ms-powerpoint")
                , new MimeType(".xls", "application/vnd.ms-excel")

                , new MimeType(".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                , new MimeType(".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation")
                , new MimeType(".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                
            }
            );

        public static Dictionary<string, string> FastMimeType { get; private set; }

        static MimeTypes()
        {
            FastMimeType = new Dictionary<string, string>();
            foreach (MimeType mt in AllMimeTypes)
            {
                FastMimeType.Add(mt.Extension, mt.Type);
            }
        }

        public static string GetMimeTypeFor(string extension)
        {
            /*foreach (MimeType mt in AllMimeTypes)
            {
                if (mt.Extension == extension)
                    return mt.Type;
            }*/
            if (FastMimeType.ContainsKey(extension))
                return FastMimeType[extension];
            return "application/octet-stream";
        }

    }

    public class MimeType
    {
        public MimeType( string extension, string type)
        {
            Extension = extension;
            Type = type;
        }

        public string Extension { get; private set; }
        public string Type { get; private set; }
        


    }
}
