﻿using Ion.Data.Networking.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
/*
This file is taken from NicroWare NetUtility

© Nicolas Fløysvik
*/


namespace NicroWare.Lib.NetUtility.Http
{
    public enum HttpRequestType
    {
        UNKNOWN,
        GET,
        HEAD,
        POST,
        PUT,
        DELETE,
        TRACE,
        OPTIONS,
        CONNECT,
        PATCH
    }

    public class HttpCookie
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public override string ToString()
        {
            return Value;
        }
    }

    public class HttpHeader
    {
        public string Version { get; set; }
        public Dictionary<string, string> HttpHeaderFields = new Dictionary<string, string>();
        //TODO: Propper naming, and property accessing
        public Dictionary<string, string> GetFields = new Dictionary<string, string>();
        public Dictionary<string, string> PostFields = new Dictionary<string, string>();
        public List<HttpFileInfo> AllFiles = new List<HttpFileInfo>();
        public Dictionary<string, byte[]> FileFields = new Dictionary<string, byte[]>();
        public Dictionary<string, string> CurrentCookies = new Dictionary<string, string>();
        public Dictionary<string, HttpCookie> NewCookies = new Dictionary<string, HttpCookie>();

        public string ContentType
        {
            get
            {
                return HttpHeaderFields.ContainsKey("CONTENT-TYPE") ? HttpHeaderFields["CONTENT-TYPE"] : null;
            }
            set
            {
                AddOrSet("CONTENT-TYPE", value.ToString());
            }
        }

        public int ContentLength
        {
            get
            {
                int temp;
                if (HttpHeaderFields.ContainsKey("CONTENT-LENGTH") && int.TryParse(HttpHeaderFields["CONTENT-LENGTH"], out temp))
                {
                    return temp;
                }
                return 0;
            }
            set
            {
                AddOrSet("CONTENT-LENGTH", value.ToString());
            }
        }

        public string Data => Encoding.Default.GetString(BinaryData);
        public byte[] BinaryData { get; set; }

        public void AddOrSet(string key, string value)
        {
            if (key == "COOKIE")
                SetCookieFields(value);
            if (HttpHeaderFields.ContainsKey(key))
                HttpHeaderFields[key] = value;
            else
                HttpHeaderFields.Add(key, value);
        }

        public override string ToString()
        {
            return ToString(new StringBuilder());
        }

        public string ToString(StringBuilder builder)
        {
            foreach (KeyValuePair<string, string> pair in HttpHeaderFields)
            {
                builder.AppendFormat("{0}: {1}\n", pair.Key, pair.Value);
            }
            foreach (KeyValuePair<string, HttpCookie> cookie in NewCookies)
            {
                builder.AppendFormat("{0}: {1}={2}\n","Set-Cookie", cookie.Key, cookie.Value);
            }
            builder.Append('\n');
            return builder.ToString();
        }

        public void SetGetFields(string data)
        {
            GetFields.Clear();
            AddFieldsTo(data, GetFields);
        }

        public void SetPostFields(string data)
        {
            PostFields.Clear();
            AddFieldsTo(data, PostFields);
        }

        public void SetCookieFields(string data)
        {
            CurrentCookies.Clear();
            string[] parts = data.Split(',');
            foreach (string s in parts)
            {
                string[] cookie = s.Split('=');
                CurrentCookies.Add(cookie[0], cookie[1]);
            }
        }

        //TODO: Cleanup multipart code
        public void SetMultipartPostFields(byte[] data, string contentType)
        {
            string[] parts = contentType.Split(';');
            string boundary = "";
            foreach (string s in parts)
            {
                string[] temp;
                if ((temp = s.Split('='))[0].Trim() == "boundary")
                {
                    boundary = temp[1];
                }
            }
            if (String.IsNullOrWhiteSpace(boundary))
                return;
            BufferReader reader = new BufferReader(data);
            while (true)
            {
                if (reader.ReadLine().Contains(boundary))
                {
                    string line = "";
                    Dictionary<string, string> info = new Dictionary<string, string>();
                    while ((line = reader.ReadLine()) != "")
                    {
                        string[] infoParts = line.Split(':');
                        info.Add(infoParts[0].ToUpper(), infoParts[1]);
                    }
                    if (info.Count == 0)
                        break;

                    if (info.ContainsKey("CONTENT-TYPE"))
                    {
                        List<byte> allBytes = new List<byte>();
                        byte[] curLine = reader.ReadLineBinary(true);
                        while (curLine.Length > 60 || !Encoding.Default.GetString(curLine).Contains(boundary))
                        {
                            allBytes.AddRange(curLine);
                            curLine = reader.ReadLineBinary(true);
                        }
                        reader.Seek(-curLine.Length);
                        string name = "";
                        string fileName = "";
                        info["CONTENT-DISPOSITION"].Split(';').ToList().ForEach(x =>
                        {
                            string[] nameParts;
                            if ((nameParts = x.Split('='))[0].Trim() == "name")
                                name = nameParts[1].Replace("\"", "");
                            if (nameParts[0].Trim() == "filename")
                                fileName = nameParts[1].Replace("\"", "");
                        });

                        //HACK: Possible issue if last is /n and secound last is /r and /r is part of the file
                        byte last = allBytes[allBytes.Count - 1];
                        allBytes.RemoveAt(allBytes.Count - 1);
                        if (last == '\n' && allBytes[allBytes.Count - 1] == '\r')
                            allBytes.RemoveAt(allBytes.Count - 1);
                        
                        PostFields.Add(name, fileName);
                        HttpFileInfo fInfo = new HttpFileInfo() { Name = fileName, MimeType = info["CONTENT-TYPE"], RawData = allBytes.ToArray() };
                        AllFiles.Add(fInfo);
                        //FileFields.Add(fileName, allBytes.ToArray());
                    }
                    else
                    {
                        string name = "";
                        info["CONTENT-DISPOSITION"].Split(';').ToList().ForEach(x =>
                        {
                            string[] nameParts;
                            if ((nameParts = x.Split('='))[0].Trim() == "name")
                                name = nameParts[1];
                        });
                        PostFields.Add(name.Replace("\"", ""), reader.ReadLine());
                    }
                }
            }
            //TODO: Remove commented code if nessesary
            /*foreach (string s in dataParts)
            {
                StringReader reader = new StringReader(s);
                reader.ReadLine();
                string curLine = "";
                Dictionary<string, string> info = new Dictionary<string, string>();
                while ((curLine = reader.ReadLine()) != null && curLine.Length > 4)
                {
                    string[] infoParts = curLine.Split(':');
                    info.Add(infoParts[0].ToUpper(), infoParts[1]);
                }
                if (curLine == null)
                    continue;
                string name = "";
                info["CONTENT-DISPOSITION"].Split(';').ToList().ForEach(x => 
                {
                    string[] nameParts;
                    if ((nameParts = x.Split('='))[0].Trim() == "name")
                        name = nameParts[1];
                });
                string body = reader.ReadToEnd();
                PostFields.Add(name.Replace("\"", ""), body.Remove(body.Length - 4));
            }*/
        }

        private void AddFieldsTo(string data, Dictionary<string, string> list)
        {
            string[] fields = data.Split('&');
            foreach (string s in fields)
            {
                string[] keyValue = s.Split('=');
                list.Add(keyValue[0], keyValue[1]);
            }
        }
    }

    public class HttpFileInfo
    {
        public byte[] RawData { get; set; }
        public string Name { get; set; }
        public string MimeType { get; set; }
    }

    public class HttpHeaderRequest : HttpHeader
    {
        public HttpRequestType RequestType { get; set; }
        public string BasePath { get; set; }
        public string FullPath
        {
            get
            {
                string returnString = $"{BasePath}";
                if (GetFields.Count > 0)
                {
                    returnString += "?";
                    bool first = true;
                    foreach (KeyValuePair<string, string> pair in base.GetFields)
                    {
                        if (first)
                            first = false;
                        else
                            returnString += "&";
                        returnString += $"{pair.Key}={pair.Value}";
                    }
                }
                return returnString;
            }
            set
            {
                string[] parts = value.Split(new char[] { '?' }, 2);
                BasePath = parts[0];
                if (parts.Length > 1)
                    SetGetFields(parts[1]);
            }
        }

        public string Host
        {
            get
            {
                if (HttpHeaderFields.ContainsKey("HOST"))
                    return base.HttpHeaderFields["HOST"];
                else
                    return null;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0} {1} {2}\n", RequestType.ToString(), FullPath, Version.ToString());
            return base.ToString(sb);
        }
    }

    public class HttpHeaderResponse : HttpHeader
    {
        public int ResponsCode { get; set; }
        string statusCode = "OK";

        /*public static HttpHeaderResponse Parse(string code)
        {
            string[] parts = code.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);
            HttpHeaderResponse respons = new HttpHeaderResponse();
            if (parts.Length > 2)
            {
                string[] responsHeader = parts[0].Split();
                respons.Version = parts[0];
            }
            return respons;
        }*/

        public override string ToString()
        {

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0} {1} {2}\n", base.Version, ResponsCode, statusCode);
            return base.ToString(sb);
        }
    }

    public class HttpReader
    {
        ProtocolReader pr;
        public HttpReader(Stream stream)
        {
            pr = new ProtocolReader(stream);
        }

        public HttpHeaderRequest ReadMessage()
        {
            string[] info = pr.ReadLine().Split(' ');
            if (info.Length < 3)
            {
                return null;
            }
            HttpHeaderRequest request = new HttpHeaderRequest();
            HttpRequestType type;
            if (!Enum.TryParse<HttpRequestType>(info[0].ToUpper(), out type))
            {
                return null;
            }
            request.RequestType = type;
            request.FullPath = info[1].Replace("%20", " ");
            request.Version = info[2];

            string reader = "";
            while ((reader = pr.ReadLine()) != null && reader.Length != 0)
            {
                string[] parts = reader.Split(':');
                if (parts.Length < 2)
                    continue;
                request.AddOrSet(parts[0].ToUpper(), parts[1].Trim());
                //request.HttpHeaderFields.Add(parts[0].ToUpper(), parts[1].Trim());
            }
            if (request.ContentLength > 0)
                ReadData(request);
            if (request.RequestType == HttpRequestType.POST)
            {
                if (request.HttpHeaderFields.ContainsKey("CONTENT-TYPE"))
                {
                    string[] parts = request.HttpHeaderFields["CONTENT-TYPE"].Split(';');
                    for (int i = 0; i < parts.Length; i++)
                        parts[i] = parts[i].Trim();
                    if (parts.Contains("application/x-www-form-urlencoded"))
                    {
                        request.SetPostFields(request.Data);
                    }
                    else if (parts.Contains("multipart/form-data"))
                    {
                        request.SetMultipartPostFields(request.BinaryData, request.HttpHeaderFields["CONTENT-TYPE"]);
                    }
                }
                
            }
            return request;
        }

        private void ReadData(HttpHeader header)
        {
            int length = header.ContentLength;
            byte[] buffer = new byte[length];
            for (int i = 0; i < buffer.Length;)
            {
                i += pr.Read(buffer, i, length - i);
            }
            header.BinaryData = buffer;
            //header.Data = new string(buffer);
            //TODO: Use correct Encoding
            //header.Data = Encoding.Default.GetString(buffer);
        }

        public void Close()
        {
            pr.Close();
        }
    }

    public class HttpWriter
    {
        ProtocolWriter pw;

        public HttpWriter(Stream stream)
        {
            this.pw = new ProtocolWriter(stream);
        }

        public void WriteMessage(string message)
        {
            WriteMessage(message, "text/plain");
        }

        public void WriteMessage(string message, string contentType)
        {
            WriteMessage(GenerateDefault(200), message, contentType);
        }

        public void WriteMessage(HttpHeaderResponse response, string message, string contentType)
        {
            response.ContentType = contentType;
            response.ContentLength = message.Length;
            pw.Write(response.ToString());
            pw.WriteLine(message);
        }


        public void WriteMessage(byte[] message, string contentType)
        {
            WriteMessage(GenerateDefault(200), message, contentType);
        }

        public void WriteMessage(HttpHeaderResponse response, byte[] message, string contentType)
        {
            response.ContentType = contentType;
            response.ContentLength = message.Length;
            pw.Write(response.ToString());
            pw.WriteBytes(message);
        }


        public void WriteFile(string fileName)
        {
            WriteFile(fileName, MimeTypes.GetMimeTypeFor(new FileInfo(fileName).Extension));
        }

        public void WriteFile(HttpHeaderResponse response, string fileName)
        {
            WriteFile(response, fileName, MimeTypes.GetMimeTypeFor(new FileInfo(fileName).Extension));
        }

        public void WriteFile(string fileName, string contentType)
        {
            FileInfo file = new FileInfo(fileName);
            byte[] bytes;
            if (file.Exists)
            {
                bytes = FileManager.ReadAllBytes(file.FullName);
            }
            else
            {
                throw new FileNotFoundException($"File: {fileName} dose not exsist");
            }

            HttpHeaderResponse responsHeader = GenerateDefault(200);
            
            responsHeader.ContentType = contentType;
            responsHeader.ContentLength = bytes.Length;
            responsHeader.HttpHeaderFields.Add("Content-Disposition", "attachment; filename=\"" + file.Name + "\"");

            pw.Write(responsHeader.ToString());
            pw.WriteBytes(bytes);
        }

        public void WriteFile(HttpHeaderResponse response, string fileName, string contentType)
        {
            FileInfo file = new FileInfo(fileName);
            byte[] bytes;
            if (file.Exists)
            {
                bytes = FileManager.ReadAllBytes(file.FullName);
            }
            else
            {
                throw new FileNotFoundException($"File: {fileName} dose not exsist");
            }

            response.ContentType = contentType;
            response.ContentLength = bytes.Length;
            response.HttpHeaderFields.Add("Content-Disposition", "attachment; filename=\"" + file.Name + "\"");

            pw.Write(response.ToString());
            pw.WriteBytes(bytes);
        }


        public static HttpHeaderResponse GenerateDefault(int responseCode)
        {
            HttpHeaderResponse response = new HttpHeaderResponse();
            response.Version = "HTTP/1.1";
            response.ResponsCode = responseCode;
            response.HttpHeaderFields.Add("Server", "NicroWare Http Server (Mixed)");
            response.HttpHeaderFields.Add("Cache-Control", "no-store");
            return response;
        }


        public void Close()
        {
            pw.Close();
        }
    }

    public class ProtocolReader
    {
        public Stream BaseStream { get; private set; }
        const int bufferSize = 4096;
        private byte[] buffer;
        private int available;
        private int position;
        private int left => available - position;
        private bool locked = false;
        public bool BlockWhenEmpty { get; set; } = false;

        public ProtocolReader(Stream baseStream)
        {
            BaseStream = baseStream;
            buffer = new byte[bufferSize];
        }

        private void ReadBuffer()
        {
            //BaseStream.ReadTimeout = 50;
            available = BaseStream.Read(buffer, 0, bufferSize);
            if (available == 0)
            {
                if (BlockWhenEmpty)
                {
                    while ((available = BaseStream.Read(buffer, 0, bufferSize)) == 0)
                        System.Threading.Thread.Sleep(1);
                }
                else
                {
                    throw new OperationCanceledException("No data was available for reading");
                }
            }
            position = 0;
            locked = available < bufferSize;
                
        }

        private int FillBuffer(byte[] buffer, int offset, int length)
        {
            locked = false;
            int curIndex = 0;
            while (curIndex < length)
            {
                if (left == 0)
                    ReadBuffer();
                if (left > length - curIndex)
                {
                    Buffer.BlockCopy(this.buffer, position, buffer, offset + curIndex, length - curIndex);
                    curIndex += length;
                    position += length;
                }
                else
                {
                    Buffer.BlockCopy(this.buffer, position, buffer, offset + curIndex, left);
                    curIndex += left;
                    position += left;

                }
                if (locked)
                    break;
            }
            return curIndex;
        }

        public byte[] ReadBytes(int count)
        {
            byte[] tempBuffer = new byte[count];
            FillBuffer(tempBuffer, 0, count);
            return tempBuffer;
        }

        public string ReadLine()
        {
            StringBuilder builder = new StringBuilder();
            char current = ' ';
            while (true)
            {
                current = Read();
                if (current == '\r' || current == '\n')
                {
                    if (current == '\r' && Peek() == '\n')
                        Read();
                    break;
                }
                else
                    builder.Append(current);
            }
            return builder.ToString();
        }

        public char Read()
        {
            if (left == 0)
                ReadBuffer();
            return (char)buffer[position++];
        }

        public char Peek()
        {
            if (left == 0)
                ReadBuffer();
            return (char)buffer[position];
        }

        public int Read(byte[] buffer, int offset, int length)
        {
            return FillBuffer(buffer, offset, length);
        }

        public void Close()
        {
            BaseStream.Close();
        }
    }

    /// <summary>
    /// TODO: Complete Protocol Writer
    /// </summary>
    public class ProtocolWriter
    {
        public Stream BaseStream { get; private set; }

        public ProtocolWriter(Stream baseStream)
        {
            this.BaseStream = baseStream;
        }

        public void WriteLine(string line)
        {
            Write(line + "\r\n");
        }

        public void Write(string text)
        {
            WriteBytes(Encoding.Default.GetBytes(text));
        }

        public void WriteBytes(byte[] bytes)
        {
            BaseStream.Write(bytes, 0, bytes.Length);
        }

        internal void Close()
        {
            BaseStream.Close();
        }
    }

    public class BufferReader
    {
        byte[] buffer;
        int position;
        int left => buffer.Length - position;
        
        public BufferReader(byte[] buffer)
        {
            this.buffer = buffer;
        }

        public int Read(byte[] buffer, int offset, int length)
        {
            int canRead = length > left ? left : length;
            Buffer.BlockCopy(this.buffer, position, buffer, offset, canRead);
            return canRead;
        }

        public string ReadLine()
        {
            StringBuilder builder = new StringBuilder();
            char current = ' ';
            while (left > 0)
            {
                current = Read();
                if (current == '\r' || current == '\n')
                {
                    if (current == '\r' && Peek() == '\n')
                        Read();
                    break;
                }
                else
                    builder.Append(current);
            }
            return builder.ToString();
        }

        public byte[] ReadLineBinary()
        {
            return ReadLineBinary(false);
        }

        public byte[] ReadLineBinary(bool includeNewLineBytes)
        {
            List<byte> allBytes = new List<byte>();
            byte current = 0;
            while (true)
            {
                current = ReadByte();
                if (current == '\r' || current == '\n')
                {
                    if (includeNewLineBytes)
                        allBytes.Add(current);
                    if (current == '\r' && Peek() == '\n')
                    {
                        byte b = ReadByte();
                        if (includeNewLineBytes)
                            allBytes.Add(b);
                    }
                    break;
                }
                else
                    allBytes.Add(current);
            }
            return allBytes.ToArray();
        }

        public byte ReadByte()
        {
            return buffer[position++];
        }

        public byte PeekByte()
        {
            return buffer[position++];
        }

        public char Read()
        {
            return (char)buffer[position++];
        }

        public char Peek()
        {
            return (char)buffer[position];
        }

        public void Seek(int num)
        {
            position += num;
        }
    }

    public class FileManager //TODO: Make FileManager the primary sender of files in HTTP so cache control and stuff like that works
    {
        public static Dictionary<string, CacheEntry<string>> Cache { get; set; } //= new Dictionary<string, CacheEntry<string>>();
        public static Dictionary<string, CacheEntry<byte[]>> ByteCache { get; set; } //= new Dictionary<string, CacheEntry<byte[]>>();

        static FileManager()
        {
            Cache = new Dictionary<string, CacheEntry<string>>();
            ByteCache = new Dictionary<string, CacheEntry<byte[]>>();
            System.Diagnostics.Debug.WriteLine("StaticInit");
        }

        public static string ReadAllText(string path)
        {
            if (Cache.ContainsKey(path))
            {
                CacheEntry<string> entry = Cache[path];
                DateTime lastWrite = File.GetLastWriteTime(path);
                if (lastWrite < entry.TimeStamp)
                    return entry.Content;
                else
                {
                    entry.Content = File.ReadAllText(path);
                    entry.TimeStamp = DateTime.Now;
                    return entry.Content;
                }
            }
            else
            {
                CacheEntry<string> entry = new CacheEntry<string>();
                entry.Path = path;
                entry.Content = File.ReadAllText(path);
                entry.TimeStamp = DateTime.Now;
                lock (Cache)
                {
                    Cache.Add(path, entry);
                }
                return entry.Content;
            }
        }

        public static byte[] ReadAllBytes(string path)
        {
            if (ByteCache.ContainsKey(path))
            {
                DateTime lastWrite = File.GetLastWriteTime(path);
                CacheEntry<byte[]> entry = ByteCache[path];
                if (lastWrite < entry.TimeStamp)
                    return entry.Content;
                else
                {
                    entry.Content = File.ReadAllBytes(path);
                    entry.TimeStamp = DateTime.Now;
                    return entry.Content;
                }
            }
            else
            {
                CacheEntry<byte[]> entry = new CacheEntry<byte[]>();
                entry.Path = path;
                entry.Content = File.ReadAllBytes(path);
                entry.TimeStamp = DateTime.Now;
                lock (ByteCache)
                {
                    ByteCache.Add(path, entry);
                }
                return entry.Content;
            }
        }
    }

    public class CacheEntry<T>
    {
        public string Path { get; set; }
        public T Content { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
