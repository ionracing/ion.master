﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.Net
{
    public class InterfaceReader
    {
        Stream baseStream;
        byte[] buffer;
        byte[] tempBuffer;
        public int TotalRead { get; private set; }
        public int CurrentRead { get; private set; }
        public int Available { get { return TotalRead - CurrentRead; } }

        public int TotalIndex { get { return TotalRead % tempBuffer.Length; } }
        public int CurrentIndex { get { return CurrentRead % tempBuffer.Length; } }

        public int LeftInBuffer { get { return buffer.Length - TotalIndex; } }
        public int DataLeftInBuffer { get { return buffer.Length - CurrentIndex; } }

        public InterfaceReader(Stream s)
        {
            baseStream = s;
            buffer = new byte[2048];
            tempBuffer = new byte[1024];
        }

        private void FillBuffer()
        {
            //Console.WriteLine("Fill buffer!");
            int read = 0;
            try
            {
                read = baseStream.Read(tempBuffer, 0, tempBuffer.Length);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.ToString());
                return;
            }
            //Console.WriteLine("Read: " + read);
            if (read > LeftInBuffer)
            {
                
                int copy = LeftInBuffer;
                Array.Copy(tempBuffer, 0, buffer, TotalIndex, copy);
                Array.Copy(tempBuffer, copy, buffer, 0, read - copy);
            }
            else
            {
                //Console.WriteLine("Filling buffer");
                Array.Copy(tempBuffer, 0, buffer, TotalIndex, read);
            }
            TotalRead += read;
            //Console.WriteLine("Total Read: " + TotalRead);
        }

        private void FillBuffer(int size)
        {
            while (Available < size)
                FillBuffer();
        }

        private byte ByteAt(int index)
        {
            index = index % buffer.Length;
            return buffer[index];
        }

        public void Read(byte[] buffer, int length, byte[] startsWith)
        {
            while (true)
            {
                FillBuffer(startsWith.Length);
                bool allMatch = true;
                for (int i = 0; i < startsWith.Length; i++)
                {
                    if (startsWith[i] != ByteAt(CurrentIndex + i))
                    {
                        allMatch = false;
                        break;
                    }
                }
                if (allMatch)
                    break;
                else
                    CurrentRead++;
            }
            Read(buffer, length);
        }

        public void Read(byte[] buffer)
        {
            ReadFromBuffer(buffer, 0, buffer.Length);
        }

        public void Read(byte[] buffer, int length)
        {
            ReadFromBuffer(buffer, 0, length);
        }

        public void Read(byte[] buffer, int offset, int length)
        {
            ReadFromBuffer(buffer, offset, length);
        }

        public byte ReadByte()
        {
            FillBuffer(1);
            byte b = this.buffer[CurrentIndex];
            CurrentRead++;
            return b;
        }

        private void ReadFromBuffer(byte[] buffer, int offset, int length)
        {
            if (length > 2048)
                throw new Exception("Support for reading byte arrays bigger then 2048 is no yet supported");
            FillBuffer(length);
            int currentIndex = 0;
            int canRead = Math.Min(DataLeftInBuffer, length);
            Array.Copy(this.buffer, CurrentIndex, buffer, offset + currentIndex, canRead);
            currentIndex += canRead;
            CurrentRead += canRead;
            if (canRead < length)
                Array.Copy(this.buffer, 0, buffer, offset + currentIndex, length - canRead);
        }
    }
}
