﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
This file is taken from NicroWare NetUtility

© Nicolas Fløysvik
*/
namespace NicroWare.Lib.NetUtility.Html.Parser
{
    public class HtmlParser
    {
        List<CodeMark> allMarks = new List<CodeMark>();
        MarkTracker tracker;
        CodeMark lastMark;
        bool InTag;
        string code;

        public HtmlParser()
        {
            lastMark = new CodeMark() { markType = MarkType.unknown, position = -1 };
        }

        public class MarkTracker
        {
            public int curPositon;
            public List<CodeMark> allCodeMarks;
            public bool End { get { return allCodeMarks.Count == curPositon; } }
            public MarkTracker(List<CodeMark> allCodeMarkers)
            {
                curPositon = 0;
                this.allCodeMarks = allCodeMarkers;
            }

            public MarkType PeekCurrentType()
            {
                return allCodeMarks[curPositon].markType;
            }

            public MarkType PeekNextType()
            {
                return allCodeMarks[curPositon + 1].markType;
            }

            public CodeMark PeekCurrent()
            {
                return allCodeMarks[curPositon];
            }

            public CodeMark PeekNext()
            {
                return allCodeMarks[curPositon + 1];
            }

            public CodeMark GetCurrent()
            {
                return allCodeMarks[curPositon++];
            }

            public CodeMark PeekForward(int i)
            {
                return allCodeMarks[curPositon + i];
            }

            public void SkipCodeMark(MarkType mark)
            {
                while (PeekCurrentType() == mark)
                    GetCurrent();
            }

            public void SkipUntilMark(MarkType mark)
            {
                while (PeekCurrentType() != mark)
                    GetCurrent();
            }

            public MarkType PeekTypeForward(int i)
            {
                return PeekForward(i).markType;
            }
        }

        private string PeekText(MarkTracker mt)
        {
            return code.Substring(mt.PeekCurrent().position, mt.PeekNext().position - 1, true).Trim();
        }

        private string PeekHtmlText(MarkTracker mt)
        {
            return code.Substring(mt.PeekCurrent().position + 1, mt.PeekNext().position - 1, true).Trim();
        }

        private string PeekText(CodeMark start, CodeMark end)
        {
            return code.Substring(start.position, end.position - 1, true).Trim();
        }

        private string GetText(MarkTracker mt)
        {
            return code.Substring(mt.GetCurrent().position, mt.PeekCurrent().position - 1, true).Trim();
        }

        private string GetString(MarkTracker mt)
        {
            if (mt.PeekCurrentType() == MarkType.stringLiteralStart && mt.PeekNextType() == MarkType.stringLiteralEnd)
                return code.Substring(mt.GetCurrent().position + 1, mt.GetCurrent().position - 1, true);
            else
            {
                int pos = mt.PeekCurrent().position;
                throw new ArgumentException(string.Format("Cant find end to string at pos:{0} it the string: \"{1}\"", pos, code.Substring(pos - 100, pos + 100, true)));
            }
        }

        public void AddMarker(CodeMark cm)
        {
            allMarks.Add(cm);
            lastMark = cm;
        }

        public void CreateMarkLits(string s)
        {
            char stringStart = ' ';
            for (int i = 0; i < s.Length; i++)
            {
                if (lastMark.markType == MarkType.commentStart)
                {
                    if (s[i] == '-' && s[i + 1] == '-' && s[i + 2] == '>')
                    {
                        AddMarker(i.CreateMark(MarkType.commentEnd));
                        i += 2;
                    }
                }
                else if (lastMark.markType != MarkType.stringLiteralStart)
                {
                    switch (s[i])
                    {
                        case '<':
                            if (s[i + 1] == '!' && s[i + 2] == '-' && s[i + 3] == '-')
                            {
                                AddMarker(i.CreateMark(MarkType.commentStart));
                                i += 3;
                            }
                            else
                                AddMarker(i.CreateMark(MarkType.openTag));
                            InTag = true;
                            break;
                        case '>':
                            AddMarker(i.CreateMark(MarkType.closeTag));
                            InTag = false;
                            break;
                        default:
                            if (InTag)
                            {
                                switch (s[i])
                                {
                                    case '/':
                                        AddMarker(i.CreateMark(MarkType.slash));
                                        break;
                                    case '\t':
                                    case ' ':
                                        if (lastMark.markType != MarkType.space)
                                            AddMarker(i.CreateMark(MarkType.space));
                                        break;
                                    case '=':
                                        AddMarker(i.CreateMark(MarkType.equal));
                                        break;
                                    case '\'':
                                    case '"':
                                        stringStart = s[i];
                                        AddMarker(i.CreateMark(MarkType.stringLiteralStart));
                                        break;
                                    case '!':
                                    case '?':
                                        AddMarker(i.CreateMark(MarkType.spesialMark));
                                        break;
                                    default:
                                        if (char.IsLetter(s[i]) && lastMark.markType != MarkType.name)
                                            AddMarker(i.CreateMark(MarkType.name));
                                        break;
                                }
                            }
                            break;
                    }
                }
                else
                {
                    switch (s[i])
                    {
                        case '\'':
                        case '"':
                            if (s[i] == stringStart)
                                AddMarker(i.CreateMark(MarkType.stringLiteralEnd));
                            break;
                        case '\\':
                            i++;
                            break;
                    }
                }
            }
        }

        public HtmlObject CreateDocument()
        {
            HtmlObject baseObj = new HtmlObject("document");
            Stack<HtmlObject> Document = new Stack<HtmlObject>();
            tracker.SkipUntilMark(MarkType.openTag);
            tracker.GetCurrent();
            Document.Push(baseObj);
            string docType = "html";
            if (tracker.PeekCurrentType() == MarkType.spesialMark && tracker.PeekNextType() == MarkType.name)
            {
                tracker.GetCurrent();
                HtmlObject temp = new HtmlObject(GetText(tracker));
                if (temp.tagName.ToLower() == "xml")
                    docType = "xml";
                /*if (temp.tagName.ToLower() == "html")
                {*/
                    tracker.SkipUntilMark(MarkType.openTag);
                /*}
                else
                {
                    AddParameters(temp, tracker);
                    baseObj.children.Add(temp);
                }*/
            }
            /*else if (tracker.PeekCurrentType() == MarkType.name && PeekText(tracker) == "html")
            {
                baseObj = new HtmlObject(GetText(tracker));
                AddParameters(baseObj, tracker);
                Document.Push(baseObj);
            }*/

            while (!tracker.End)
            {
                if (tracker.GetCurrent().markType == MarkType.openTag)
                {
                    if (tracker.PeekCurrentType() == MarkType.slash)
                    {
                        tracker.GetCurrent();
                        string text = GetText(tracker);
                        if (text == Document.Peek().tagName)
                            Document.Pop();
                        else if (text == Document.ToArray()[1].tagName)
                        {
                            Document.Pop();
                            Document.Pop();
                        }
                        else
                            throw new FormatException("The given closeing tag dose not match");
                    }
                    else
                    {

                        HtmlObject temp = new HtmlObject(GetText(tracker));

                        AddParameters(temp, tracker);

                        Document.Peek().children.Add(temp);
                        tracker.SkipCodeMark(MarkType.space);
                        if (docType != "html" || (temp.tagName != "link" && temp.tagName != "meta" && temp.tagName != "br" && temp.tagName != "hr"))
                            if (tracker.PeekCurrent().markType == MarkType.closeTag)
                                Document.Push(temp);
                            else
                                tracker.GetCurrent();
                        //string text = PeekHtmlText(tracker);
                        if (Document.Peek().tagName == "script")
                        {
                            int posStart = tracker.PeekCurrent().position;
                            while (true)
                            {
                                MarkType type = tracker.PeekCurrentType();
                                if (type == MarkType.openTag && tracker.PeekNextType() == MarkType.slash)
                                {

                                    string s = PeekText( tracker.PeekForward(2), tracker.PeekForward(3)).ToLower();
                                    if (s == "script")
                                        break;
                                }
                                tracker.GetCurrent();
                            }
                            /*while (tracker.PeekCurrent().markType != MarkType.openTag || tracker.PeekNext().markType != MarkType.slash)
                                tracker.GetCurrent();*/
                            int endPosition = tracker.PeekCurrent().position;
                            Document.Peek().children.Add(new HtmlObject("") { innerHTML = code.Substring(posStart + 1, endPosition - 1, true) });
                        }
                        else
                        {
                            string text = PeekHtmlText(tracker);
                            if (!string.IsNullOrWhiteSpace(text))
                                Document.Peek().children.Add(new HtmlObject("") { innerHTML = text });
                            tracker.GetCurrent();
                        }


                    }
                }
            }
            return baseObj;
        }

        public void AddParameters(HtmlObject obj, MarkTracker mt)
        {
            mt.SkipCodeMark(MarkType.space);
            while (mt.PeekCurrentType() != MarkType.slash && mt.PeekCurrentType() != MarkType.closeTag)
            {
                string name = GetText(mt);
                mt.SkipCodeMark(MarkType.space);
                if (mt.PeekCurrentType() != MarkType.equal)
                {
                    obj.parameters.Add(name, "");
                    continue;
                }
                mt.GetCurrent();
                mt.SkipCodeMark(MarkType.space);
                if (mt.PeekCurrentType() != MarkType.stringLiteralStart)
                {
                    mt.SkipUntilMark(MarkType.space);
                }
                else
                {
                    string param = GetString(mt);
                    obj.parameters.Add(name, param);
                    mt.SkipCodeMark(MarkType.space);
                }
            }
        }

        public HtmlObject Parse(string s)
        {
            this.code = s;
            CreateMarkLits(s);
            tracker = new MarkTracker(allMarks);
            return CreateDocument();

        }
    }

    public class HtmlObject
    {
        public string tagName;
        public string innerHTML;
        public List<HtmlObject> children;
        public Dictionary<string, string> parameters;
        public string InnerText
        {
            get
            {
                string returnString = "";
                if (innerHTML != null)
                    return innerHTML;
                foreach (HtmlObject ho in children)
                {
                    returnString += ho.InnerText;
                }
                return returnString;
            }
        }

        public HtmlObject(string name)
        {
            this.tagName = name;
            children = new List<HtmlObject>();
            parameters = new Dictionary<string, string>();
        }

        public void ToHtmlString(StringBuilder builder)
        {
            ToHtmlString(builder, false, 0);
        }

        public void ToHtmlString(StringBuilder builder, bool ignoreThis)
        {
            ToHtmlString(builder, ignoreThis, 0);
        }

        private void ToHtmlString(StringBuilder builder, bool ignoreThis, int tabs)
        {
            if (!ignoreThis)
            {
                builder.Append('\t', tabs);
                if (tagName == "")
                {
                    builder.Append(innerHTML);
                }
                else if (children.Count == 0)
                {
                    builder.AppendFormat("<{0}{1} />", tagName, parameters.ToHtmlParameters());
                }
                else
                {
                    builder.AppendFormat("<{0}{1}>\r\n", tagName, parameters.ToHtmlParameters());
                    foreach (HtmlObject child in children)
                    {
                        child.ToHtmlString(builder, false, tabs + 1);
                    }
                    builder.Append('\t', tabs);
                    builder.AppendFormat("</{0}>", tagName);
                }
                builder.Append("\r\n");
            }
            else
            {
                foreach (HtmlObject child in children)
                {
                    child.ToHtmlString(builder, false, tabs);
                }
            }
        }

        public HtmlObject[] GetChildsByTag(string tag)
        {
            List<HtmlObject> obj = new List<HtmlObject>();
            GetChildsByTag(tag, obj);
            return obj.ToArray();
        }

        private void GetChildsByTag(string tag, List<HtmlObject> obj)
        {
            foreach (HtmlObject child in children)
            {
                if (child.tagName == tag)
                {
                    obj.Add(child);
                }
                child.GetChildsByTag(tag, obj);
            }
        }

        public HtmlObject[] GetChildsByClass(string className)
        {
            List<HtmlObject> obj = new List<HtmlObject>();
            GetChildsByClass(className, obj);
            return obj.ToArray();
        }

        private void GetChildsByClass(string className, List<HtmlObject> obj)
        {
            foreach (HtmlObject child in children)
            {
                if (child.parameters.ContainsKey("class") && child.parameters["class"].Split(' ').Contains(className))
                {
                    obj.Add(child);
                }
                child.GetChildsByClass(className, obj);
            }
        }

        public HtmlObject[] GetChildsByParameter(string par, string value)
        {
            List<HtmlObject> obj = new List<HtmlObject>();
            GetChildsByParameter(par, value, obj);
            return obj.ToArray();
        }

        private void GetChildsByParameter(string par, string value, List<HtmlObject> obj)
        {
            foreach (HtmlObject child in children)
            {
                if (child.parameters.ContainsKey(par) && child.parameters[par] == value)
                {
                    obj.Add(child);
                }
                child.GetChildsByParameter(par, value, obj);
            }
        }

        public HtmlObject GetChildsByID(string id)
        {
            List<HtmlObject> obj = new List<HtmlObject>();
            GetChildsByTag(id, obj);
            return obj.First();
        }

        private void GetChildsByID(string id, List<HtmlObject> obj)
        {
            foreach (HtmlObject child in children)
            {
                if (child.parameters.ContainsKey("id") && child.parameters["id"] == id)
                {
                    obj.Add(child);
                }
                child.GetChildsByID(id, obj);
            }
        }

        public override string ToString()
        {
            return "<" + tagName + ">";
        }
    }

    public static class ParserExtensions
    {
        public static CodeMark CreateMark(this int i, MarkType type)
        {
            return new CodeMark() { position = i, markType = type };
        }

        public static string Substring(this string s, int start, int end, bool useEnd)
        {
            int length = end - start + 1;
            return s.Substring(start, length);
        }

        public static string ToHtmlParameters(this Dictionary<string, string> dictionary)
        {
            string s = "";
            foreach (KeyValuePair<string, string> pair in dictionary)
            {
                s += string.Format(" {0}=\"{1}\"", pair.Key, pair.Value);
            }
            return s;
        }
    }

    public class CodeMark
    {
        public MarkType markType;
        public int position;

        public override string ToString()
        {
            return string.Format("{0} ({1})", position, markType);
        }
    }

    public enum MarkType
    {
        unknown,
        openTag,
        closeTag,
        slash,
        word,
        stringLiteralStart,
        stringLiteralEnd,
        endLiteralStart,
        space,
        equal,
        name,
        commentStart,
        commentEnd,
        spesialMark,
    }
}
