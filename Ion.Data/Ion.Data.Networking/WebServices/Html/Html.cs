﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NicroWare.Lib.NetUtility.Http;
using System.IO;
/*
This file is taken from NicroWare NetUtility

© Nicolas Fløysvik
*/


namespace NicroWare.Lib.NetUtility.Html
{
    public class HtmlWriter : HttpWriter
    {
        public HtmlWriter(Stream stream)
            : base(stream)
        {

        }

        public void WriteHtml(string html)
        {
            base.WriteMessage(html, "text/html");
        }

        public void WriteHtml(HttpHeaderResponse response, string html)
        {
            base.WriteMessage(response, html, "text/html");
        }

        public void WriteHtml(HtmlBuilder hb)
        {
            base.WriteMessage(hb.ToString(), "text/html");
        }

        public void WriteHtml(HttpHeaderResponse response, HtmlBuilder hb)
        {
            base.WriteMessage(response, hb.ToString(), "text/html");
        }
    }

    public class HtmlBuilder
    {
        public Node BaseNode { get; set; }

        public HtmlBuilder()
        {
            BaseNode = new HtmlNode("html");
        }

        public override string ToString()
        {
            return ToString(false);
        }

        public string ToString(bool withDoctype)
        {
            StringBuilder sb = new StringBuilder();
            if (withDoctype)
                sb.AppendLine("<!DOCTYPE html>");
            BaseNode.GenerateString(sb);
            return sb.ToString();
        }
    }

    public class Node
    {
        public virtual void GenerateString(StringBuilder sb)
        {
        }
    }

    public class TextNode : Node
    {
        public string Content { get; set; }

        public TextNode(string content)
        {
            this.Content = content;
        }

        public override void GenerateString(StringBuilder sb)
        {
            sb.Append(Content);
        }
    }

    public class HtmlNode : Node
    {
        public string Name { get; set; }
        public List<Node> Childern { get; private set; }
        public string Parameters { get; set; }

        public HtmlNode(string name)
        {
            this.Name = name;
            this.Childern = new List<Node>();
        }

        public HtmlNode(string name, string parameters)
            : this(name)
        {
            this.Parameters = parameters;
        }

        public HtmlNode(string name, params Node[] nodes)
            : this(name)
        {
            AddNodes(nodes);
        }

        public HtmlNode(string name, string parameters, params Node[] nodes)
            : this(name, parameters)
        {
            AddNodes(nodes);
        }

        public HtmlNode AddNodes(params Node[] nodes)
        {
            Childern.AddRange(nodes);
            return this;
        }

        public override void GenerateString(StringBuilder sb)
        {
            if (Childern.Count > 0)
            {
                sb.AppendLine($"<{Name} {Parameters}>");
                foreach (Node n in Childern)
                    n.GenerateString(sb);
                sb.AppendLine($"</{Name}>");
            }
            else
                sb.AppendLine($"<{Name} {Parameters}/>");
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}