﻿using NicroWare.Lib.NetUtility.Html;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.Html.Controllers
{
    public class HtmlTable
    {
        string[] headers = null;
        string[] pars = null;
        List<string[]> rows = new List<string[]>();

        public string ID { get; set; } = "";
        public string TableParams { get; set; } = "";

        public HtmlTable()
        {

        }

        public void AddHeaderParams(params string[] pars)
        {
            this.pars = pars;
        }

        public void AddHeader(params string[] cols)
        {
            this.headers = cols;
        }

        public void AddRow(params string[] cols)
        {
            this.rows.Add(cols);
        }

        public HtmlNode ToDom()
        {
            HtmlNode table = new HtmlNode("table", TableParams);
            if (headers != null)
            {
                HtmlNode thead = new HtmlNode("thead");
                HtmlNode trHead = new HtmlNode("tr");
                for (int i = 0; i < headers.Length; i++)
                {
                    trHead.Childern.Add(new HtmlNode("th", pars?.Length > i ? pars[i] : "" ?? "", new TextNode(headers[i])));
                }
                thead.Childern.Add(trHead);
                table.Childern.Add(thead);
            }
            HtmlNode tbody = new HtmlNode("tbody");
            foreach (string[] row in rows)
            {
                HtmlNode trData = new HtmlNode("tr");
                foreach (string s in row)
                {
                    trData.Childern.Add(new HtmlNode("td", new TextNode(s)));
                }
                tbody.Childern.Add(trData);
            }
            table.Childern.Add(tbody);
            return table;
        }
    }
}
