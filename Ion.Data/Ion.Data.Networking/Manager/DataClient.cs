﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.Manager
{
    public struct DataWrapper
    {
        public ushort SensorID { get; set; }
        public int Value { get; set; }
        public int TimeStamp { get; set; }

        public byte[] GetBytes(WrapperMode mode)
        {
            byte[] bytes = new byte[0];
            if (mode == WrapperMode.NormalMode)
            {
                bytes = new byte[6];
                Array.Copy(BitConverter.GetBytes(SensorID), 0, bytes, 0, 2);
                Array.Copy(BitConverter.GetBytes(Value), 0, bytes, 2, 4);
            }
            else if (mode == WrapperMode.ShortMode)
            {
                bytes = new byte[3];
                bytes[0] = (byte)SensorID;
                Array.Copy(BitConverter.GetBytes(Value), 0, bytes, 1, 2);
            }
            else if (mode == WrapperMode.CompressedMode)
            {
                bytes = new byte[4];
                bytes[0] = (byte)SensorID;
                Array.Copy(BitConverter.GetBytes(Value), 0, bytes, 1, 3);
            }
            return bytes;
        }

        public byte[] GetBytesWithTime(WrapperMode mode)
        {
            byte[] bytes = new byte[0];
            if (mode == WrapperMode.NormalMode)
            {
                bytes = new byte[10];
                bytes[0] = (byte)SensorID;
                bytes[1] = (byte)(SensorID >> 8);
                bytes[2] = (byte)Value;
                bytes[3] = (byte)(Value >> 8);
                bytes[4] = (byte)(Value >> 16);
                bytes[5] = (byte)(Value >> 24);
                bytes[6] = (byte)TimeStamp;
                bytes[7] = (byte)(TimeStamp >> 8);
                bytes[8] = (byte)(TimeStamp >> 16);
                bytes[9] = (byte)(TimeStamp >> 24);
            }
            else if (mode == WrapperMode.CompressedMode)
            {
                bytes = new byte[8];
                bytes[0] = (byte)SensorID;
                bytes[1] = (byte)Value;
                bytes[2] = (byte)(Value >> 8);
                bytes[3] = (byte)(Value >> 16);
                bytes[4] = (byte)TimeStamp;
                bytes[5] = (byte)(TimeStamp >> 8);
                bytes[6] = (byte)(TimeStamp >> 16);
                bytes[7] = (byte)(TimeStamp >> 24);
            }
            return bytes;

        }

        public static DataWrapper ReadData(byte[] bytes, int offset, WrapperMode mode)
        {
            DataWrapper wrapper = new DataWrapper();
            if (bytes.Length - offset < ExpectedLength(mode, false))
                return wrapper;
            if (mode == WrapperMode.NormalMode)
            {
                wrapper.SensorID = BitConverter.ToUInt16(bytes, offset);
                wrapper.Value = BitConverter.ToInt32(bytes, offset + 2);
            }
            else if (mode == WrapperMode.CompressedMode)
            {
                wrapper.SensorID = bytes[offset];
                wrapper.Value = bytes[offset + 1] | bytes[offset + 2] << 8 | bytes[offset + 3] << 16;
            }
            else if (mode == WrapperMode.ShortMode)
            {
                wrapper.SensorID = bytes[offset];
                wrapper.Value = BitConverter.ToInt16(bytes, offset + 1);
            }
            return wrapper;
        }

        public static DataWrapper ReadDataWithTime(byte[] bytes, int offset, WrapperMode mode)
        {
            DataWrapper wrapper = new DataWrapper();
            if (bytes.Length - offset < ExpectedLength(mode, true))
                return wrapper;
            int length = 0;
            if (mode == WrapperMode.NormalMode)
            {
                wrapper.SensorID = BitConverter.ToUInt16(bytes, offset);
                wrapper.Value = BitConverter.ToInt32(bytes, offset + 2);
                length = 6;
            }
            else if (mode == WrapperMode.CompressedMode)
            {
                wrapper.SensorID = bytes[offset];
                wrapper.Value = bytes[offset + 1] | bytes[offset + 2] >> 8 | bytes[offset + 3] >> 16;
                length = 4;
            }
            wrapper.TimeStamp = BitConverter.ToInt32(bytes, offset + length);

            return wrapper;
        }

        public static int ExpectedLength(WrapperMode mode, bool withTime)
        {
            switch (mode)
            {
                case WrapperMode.CompressedMode:
                    return 4 + (withTime ? 4 : 0);
                case WrapperMode.NormalMode:
                    return 6 + (withTime ? 4 : 0);
                case WrapperMode.ShortMode:
                    return 3;
                default:
                    throw new ArgumentException("Cant calculate length for current mode");
            }
        }
    }

    

    public abstract class DataClient
    {
        public abstract byte[] Receive();
        public abstract void Send(byte[] data);
        public WrapperMode PackageMode { get; set; } = WrapperMode.NormalMode;
        public byte[] LastRecivedBytes { get; private set; }

        public DataWrapper[] ReadDataWrappers(bool WithTime)
        {
            LastRecivedBytes = Receive();
            List<DataWrapper> AllWrappers = new List<DataWrapper>();
            for (int i = 0; i < LastRecivedBytes.Length;)
            {
                if (WithTime)
                {
                    AllWrappers.Add(DataWrapper.ReadDataWithTime(LastRecivedBytes, i, PackageMode));
                    switch (PackageMode)
                    {
                        case WrapperMode.NormalMode:
                            i += 10;
                            break;
                        case WrapperMode.CompressedMode:
                            i += 8;
                            break;
                    }
                }
                else
                {
                    AllWrappers.Add(DataWrapper.ReadData(LastRecivedBytes, i, PackageMode));
                    switch (PackageMode)
                    {
                        case WrapperMode.ShortMode:
                            i += 3;
                            break;
                        case WrapperMode.NormalMode:
                            i += 6;
                            break;
                        case WrapperMode.CompressedMode:
                            i += 4;
                            break;
                    }
                }

            }
            return AllWrappers.ToArray();
        }

        public void SendDataWrappers(DataWrapper[] wrappers, bool WithTime)
        {
            List<byte> bytes = new List<byte>();
            foreach (DataWrapper wrap in wrappers)
            {
                if (WithTime)
                    bytes.AddRange(wrap.GetBytesWithTime(PackageMode));
                else
                    bytes.AddRange(wrap.GetBytes(PackageMode));
            }
            Send(bytes.ToArray());
        }
    }

    public enum WrapperMode
    {
        ShortMode,
        NormalMode,
        CompressedMode,
    }
}
