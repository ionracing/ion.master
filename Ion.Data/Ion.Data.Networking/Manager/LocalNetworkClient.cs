﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Ion.Data.Networking.Manager
{
    public class LocalNetworkClient : DataClient
    {
        UdpClient client;
        IPEndPoint endPoint;
        public LocalNetworkClient(int udpPort)
        {
            client = new UdpClient(udpPort);
        }

        public void Connect(IPAddress address, int port)
        {
            //client.Connect(address, port);
            endPoint = new IPEndPoint(address, port);
        }

        public override byte[] Receive()
        {
            IPEndPoint source = new IPEndPoint(IPAddress.Any, 0);
            return client.Receive(ref source);
        }

        public override void Send(byte[] data)
        {
            client.Send(data, data.Length, endPoint);
            //client.Send(data, data.Length);
        }
    }
}
