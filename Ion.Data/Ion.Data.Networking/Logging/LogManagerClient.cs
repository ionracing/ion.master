﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.Logging
{
    public class LogManagerClient : ILogManager, IDisposable
    {
        UdpClient udpLogClient = new UdpClient("127.0.0.1", 9965);
        private bool disposed;

        public LogManagerClient()
        {
        }

        public void WriteCustom(LogEntry entry)
        {
            byte[] bytes = RemoteLogEntry.Convert(entry).ToByteArray();
            udpLogClient.Send(bytes, bytes.Length);

            //TcpClient logClient = new TcpClient();
            //logClient.Connect(new IPEndPoint(IPAddress.Loopback, 9965));
            //RemoteLogEntry.Convert(entry).ToStream(logClient.GetStream());
            //logClient.Close();
        }

        public void WriteException(string text, Exception exception, string sender, int level)
        {
            //TODO: Implement method for sending extra data with exception
            WriteCustom(new LogEntry() { Category = "EXCEPTION", Value = text + " " + exception.ToString(), Sender = sender, Level = level });
        }

        public void WriteLine(string text, string category, string sender, int level)
        {
            WriteCustom(new LogEntry() { Category = category, Value = text, Sender = sender, Level = level });
        }

        public LogInterface CreateLogger(string name)
        {
            return new LogInterface(name, this);
        }

        ILogger ILogManager.CreateLogger(string name)
        {
            return CreateLogger(name);
        }

        public LogEntry[] ListEntries()
        {
            TcpClient client = new TcpClient();
            client.Connect("127.0.0.1", 9965); //TODO: A number here that is not configurable at any place
            var networkStream = client.GetStream();
            BinaryWriter writer = new BinaryWriter(networkStream);
            writer.Write("all");
            BinaryReader reader = new BinaryReader(networkStream);
            int count = reader.ReadInt32();
            LogEntry[] entries = new LogEntry[count];
            for (int i = 0; i < count; i++)
            {
                entries[i] = RemoteLogEntry.FromReader(reader);
            }
            reader.Close();
            return entries;
        }

        public void AddEventListner(LogEventHandler eventHandler, LogLevel level)
        {
            TcpClient client = new TcpClient();
            client.Connect("127.0.0.1", 9965);
            var stream = client.GetStream();
            BinaryWriter writer = new BinaryWriter(stream);
            writer.Write("bind");
            Random rnd = new Random();
            UdpClient udpListner = new UdpClient(0);
            writer.Write(((IPEndPoint)udpListner.Client.LocalEndPoint).Port);
            writer.Write((int)level);
            writer.Flush();
            writer.Close();
            WaitForEvent(udpListner, eventHandler);
        }

        public async void WaitForEvent(UdpClient listner, LogEventHandler handler)
        {
            UdpReceiveResult result = await listner.ReceiveAsync();
            RemoteLogEntry entry = RemoteLogEntry.FromByteArray(result.Buffer);
            handler(this, new LogWriteEventArgs() { Entry = entry });
            WaitForEvent(listner, handler);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                udpLogClient.Close();
            }
            disposed = true;
        }
    }
}
