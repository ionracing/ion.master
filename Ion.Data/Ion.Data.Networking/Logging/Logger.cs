﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

//NicroWare.Lib.Networking.Logger, Originates from Nicolas Fløysvik
namespace Ion.Data.Networking.Logging
{
    /*
     * 0 = Verbose Info
     * 1 = Info
     * 2 = Important Info
     * 3 = Warning
     * 4 = Error
     * 5 = Critical
     */

    public interface ILogger
    {
        void WriteLine(string text, string category, int level);

        void WriteLine(string text, string category, string sender, int level);

        void WriteException(string text, Exception exception, int level);

        void WriteException(string text, Exception exception, string sender, int level);

        void WriteCustom(LogEntry entry);
    }

    public interface ILogManager
    {
        void WriteLine(string text, string category, string sender, int level);
        void WriteException(string text, Exception exception, string sender, int level);
        void WriteCustom(LogEntry entry);
        void AddEventListner(LogEventHandler eventHandler, LogLevel level);
        ILogger CreateLogger(string name);
        LogEntry[] ListEntries();
    }

    public delegate void LogEventHandler(object sender, LogWriteEventArgs e);

    public class LogWriteEventArgs : EventArgs
    {
        public LogEntry Entry { get; set; }
    }

    public enum LogType : byte
    {
        Normal = 0,
        Exception = 1
    }

    public enum LogLevel : int
    {
        VerboseInfo = 0,
        Info = 1,
        ImportantInfo = 2,
        Warning = 3,
        Error = 4,
        Critical = 5
    }

    public class LogEntry
    {
        public int ID { get; set; }
        public string Category { get; set; }
        public string Sender { get; set; }
        public int Level { get; set; }
        public DateTime Time { get; set; }
        public virtual string Value { get; set; }
    }

    public class ExceptionEntry : LogEntry
    {
        public Exception Exception { get; set; }
    }

    public class RemoteLogEntry : LogEntry
    {
        public LogType LogType { get; private set; }

        public byte[] ToByteArray()
        {
            MemoryStream memStream = new MemoryStream();
            ToStream(memStream);
            byte[] bytes = memStream.ToArray();
            memStream.Close();
            return bytes;
        }

        public void ToStream(Stream s)
        {
            ToWriter(new BinaryWriter(s));
        }

        public void ToWriter(BinaryWriter writer)
        {
            writer.Write((byte)LogType);
            writer.Write(Time.ToBinary());
            writer.Write(Category);
            writer.Write(Sender);
            writer.Write(Level);
            writer.Write(Value);
        }

        public static RemoteLogEntry FromByteArray(byte[] bytes)
        {
            MemoryStream memStream = new MemoryStream(bytes);
            RemoteLogEntry temp = FromStream(memStream);
            memStream.Close();
            return temp;
        }

        public static RemoteLogEntry FromStream(Stream s)
        {
            return FromReader(new BinaryReader(s));

        }

        public static RemoteLogEntry FromReader(BinaryReader reader)
        {
            RemoteLogEntry temp = new RemoteLogEntry();
            temp.LogType = (LogType)reader.ReadByte();
            temp.Time = DateTime.FromBinary(reader.ReadInt64());
            temp.Category = reader.ReadString();
            temp.Sender = reader.ReadString();
            temp.Level = reader.ReadInt32();
            temp.Value = reader.ReadString();
            return temp;
        }

        public static RemoteLogEntry Convert(LogEntry entry)
        {
            return new RemoteLogEntry() { Category = entry.Category, Level = entry.Level, Sender = entry.Sender, Value = entry.Value, Time = entry.Time };
        }
    }

    public static class LoggingExtension
    {
        public static void WriteInfo(this ILogger logger, string message)
        {
            logger.WriteLine(message, "INFO", 2);
        }

        public static void WriteVerbose(this ILogger logger, string message)
        {
            logger.WriteLine(message, "INFO", 0);
        }
    }
}
