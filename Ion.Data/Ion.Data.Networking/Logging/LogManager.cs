﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.Logging
{
    public class LogManager : ILogManager
    {
        public int nextId = 0;
        public List<LogEntry> AllEnteries { get; private set; } = new List<LogEntry>();
        const string EXCEPTION = "EXCEPTION";

        public event LogEventHandler OnVerbose;
        public event LogEventHandler OnInfo;
        public event LogEventHandler OnWarning;
        public event LogEventHandler OnError;
        public event LogEventHandler OnCritical;

        public void WriteLine(string text, string category, string name, int level)
        {
            Add(new LogEntry() { Value = text, Category = category, Level = level, Sender = name, Time = DateTime.Now });
        }

        public void WriteException(string text, Exception exception, string sender, int level)
        {
            Add(new ExceptionEntry() { Value = text, Exception = exception, Category = EXCEPTION, Level = level, Sender = sender, Time = DateTime.Now });
        }

        public void WriteCustom(LogEntry entry)
        {
            entry.Time = DateTime.Now;
            Add(entry);
        }

        private void Add(LogEntry entry)
        {
            entry.ID = nextId++;
            AllEnteries.Add(entry);
            ActivateEvent(entry);
        }

        protected virtual void ActivateEvent(LogEntry entry)
        {
            LogWriteEventArgs args = new LogWriteEventArgs() { Entry = entry };
            if (entry.Level >= 0 && OnVerbose != null)
                OnVerbose(this, args);
            if (entry.Level >= 2 && OnInfo != null)
                OnInfo(this, args);
            if (entry.Level >= 3 && OnWarning != null)
                OnWarning(this, args);
            if (entry.Level >= 4 && OnError != null)
                OnError(this, args);
            if (entry.Level >= 5 && OnCritical != null)
                OnCritical(this, args);
        }

        public LogEntry[] ListEntries()
        {
            return AllEnteries.ToArray();
        }

        public LogInterface CreateLogger(string name)
        {
            return new LogInterface(name, this);
        }

        ILogger ILogManager.CreateLogger(string name)
        {
            return CreateLogger(name);
        }

        public void AddEventListner(LogEventHandler eventHandler, LogLevel level)
        {
            switch (level)
            {
                case LogLevel.VerboseInfo:
                case LogLevel.Info:
                    OnVerbose += eventHandler;
                    break;
                case LogLevel.ImportantInfo:
                    OnInfo += eventHandler;
                    break;
                case LogLevel.Warning:
                    OnWarning += eventHandler;
                    break;
                case LogLevel.Error:
                    OnError += eventHandler;
                    break;
                case LogLevel.Critical:
                    OnCritical += eventHandler;
                    break;
            }
        }
    }
}
