﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.Logging
{
    public class LogInterface : ILogger
    {
        protected ILogManager Parrent { get; private set; }
        protected string Name { get; private set; }

        public LogInterface(string name, ILogManager parrent)
        {
            this.Name = name;
            this.Parrent = parrent;
        }

        public void WriteLine(string text, string category, int level)
        {
            WriteLine(text, category, Name, level);
        }

        public void WriteLine(string text, string category, string sender, int level)
        {
            Parrent.WriteLine(text, category, sender, level);
        }

        public void WriteException(string text, Exception exception, int level)
        {
            WriteException(text, exception, Name, level);
        }

        public void WriteException(string text, Exception exception, string sender, int level)
        {
            Parrent.WriteException(text, exception, sender, level);
        }

        public void WriteCustom(LogEntry entry)
        {
            if (entry.Sender == null)
                entry.Sender = Name;

            Parrent.WriteCustom(entry);
        }
    }
}
