﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.Logging
{
    public class LogManagerServer : LogManager, IDisposable
    {
        UdpClient logListner = new UdpClient(NetPorts.LogServerUDP);
        TcpListener logClient = new TcpListener(IPAddress.Loopback, NetPorts.LogServerTCP);
        ILogger logServer;
        Dictionary<int, LogEventHandler> allHandlers = new Dictionary<int, LogEventHandler>();
        private bool disposed;

        public LogManagerServer()
        {
            logServer = base.CreateLogger("LOGSERVER");
            logClient.Start();
            Listen();
            UdpListen();
        }

        public async void UdpListen()
        {
            UdpReceiveResult result = await logListner.ReceiveAsync();
            UdpListen();

            RemoteLogEntry entry = RemoteLogEntry.FromByteArray(result.Buffer);
            if (entry != null)
            {
                base.WriteCustom(entry);
            }
            else
            {
                base.WriteLine("Error: recived empty log entry", "ERROR", "LOGSERVER", 3);
                Debugger.Break();
            }
        }

        public async void Listen()
        {
            TcpClient result = await logClient.AcceptTcpClientAsync();
            Listen();
            Stream stream = result.GetStream();
            BinaryReader reader = new BinaryReader(stream);
            string s = reader.ReadString();
            if (s == "all")
            {
                LogEntry[] entries = base.ListEntries();
                BinaryWriter bw = new BinaryWriter(stream);
                bw.Write(entries.Length);
                foreach (LogEntry le in entries)
                {
                    RemoteLogEntry.Convert(le).ToWriter(bw);
                }
                bw.Flush();
                bw.Close();
            }
            else if (s == "bind")
            {
                int port = reader.ReadInt32();
                int level = reader.ReadInt32();
                LogEventHandler handler = new LogEventHandler((object sender, LogWriteEventArgs e) =>
                {
                    UdpClient client = new UdpClient(/*new IPEndPoint(IPAddress.Loopback, 8854)*/);
                    client.Connect(IPAddress.Loopback, port);
                    byte[] bytes = RemoteLogEntry.Convert(e.Entry).ToByteArray();
                    client.Send(bytes, bytes.Length);
                    client.Close();
                });
                base.AddEventListner(handler, (LogLevel)level);
                allHandlers.Add(port, handler);
            }
            else if (s == "unbind")
            {
                int port = reader.ReadInt32();
                if (allHandlers.ContainsKey(port))
                {
                    //TODO: Add posibility to remove eventhandlers
                }

            }
            

            //base.WriteCustom(entry);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                logListner.Close();
                logClient.Stop();
            }
            disposed = true;
        }
    }
}
