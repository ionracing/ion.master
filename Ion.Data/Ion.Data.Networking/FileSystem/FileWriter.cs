﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.FileSystem
{
    public class FileWriter : IDisposable
    {
        byte[] data;
        int currentPlace;
        BinaryWriter bw;// = new TextWriter();
        string path;
        bool opend = false;
        DateTime lastUpdate;
        const int bufferSize = 3000;
        private bool disposed;

        public FileWriter(string filePath)
        {
            data = new byte[bufferSize];
            currentPlace = 0;
            path = filePath;
            lastUpdate = DateTime.Now;
        }

        ~FileWriter()
        {
            Dispose(false);
        }

        public void Write(byte[] buffer)
        {

            Buffer.BlockCopy(buffer, 0, data, currentPlace, buffer.Length);
            currentPlace += buffer.Length;
            if (currentPlace == data.Length || DateTime.Now > lastUpdate + TimeSpan.FromMilliseconds(500))
            {
                if (!opend)
                {
                    if (!File.Exists(path))
                        File.Create(path).Close();
                    bw = new BinaryWriter(new FileStream(path, FileMode.Append));
                    opend = true;
                }
                bw.Write(data, 0, currentPlace);
                bw.Flush();
                data = new byte[bufferSize];
                currentPlace = 0;
                lastUpdate = DateTime.Now;
            }
        }

        #region IDisposable implementation

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                if (opend)
                {
                    bw.Close();
                    opend = false;
                }
            }
            disposed = true;
        }

        #endregion
    }
}
