﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.NewStack
{
    public enum IDPOptions : byte
    {

    }

    public class IDPHeader : INetHeader
    {
        public const ushort IDPHeaderID = 0x9901;
        public virtual ushort ProtocolID { get; } = IDPHeaderID;
        public byte DestPort { get; set; }

        public virtual byte[] GetBytes()
        {
            return new byte[] { DestPort };
        }
    }

    public class IDPExHeader : IDPHeader
    {
        public const ushort IDPExHeaderID = 0x9902;
        public override ushort ProtocolID { get; } = IDPExHeaderID;
        public byte SourcePort { get; set; }
        public IDPOptions options { get; set; }
        public byte Seq { get; set; }
        public byte Ack { get; set; }

        public override byte[] GetBytes()
        {
            byte[] temp = new byte[5];

            temp[0] = base.DestPort;
            temp[1] = SourcePort;
            temp[2] = (byte)options;
            temp[3] = Seq;
            temp[4] = Ack;

            return temp;
        }
    }

    public class IDPPackage : INetPackage
    {
        public IDPHeader Header { get; set; }
        public byte[] Data { get; set; }

        INetHeader INetPackage.Header
        {
            get
            {
                return Header;
            }
        }

        private IDPPackage()
        {
        }

        public IDPPackage(byte[] data, byte destPort)
        {
            Data = data;
            Header = new IDPHeader() { DestPort = destPort };
        }

        public IDPPackage(byte[] data, byte destPort, byte sourcePort)
        {
            Data = data;
            Header = new IDPExHeader() { DestPort = destPort, SourcePort = sourcePort };
        }

        public static IDPPackage Parse(byte[] bytes)
        {
            IDPPackage package = new IDPPackage();
            package.Header = new IDPHeader();
            package.Header.DestPort = bytes[0];
            package.Data = new byte[bytes.Length - 1];
            Array.Copy(bytes, 1, package.Data, 0, package.Data.Length);
            return package;
        }

        public static IDPPackage ParseExtended(byte[] bytes)
        {
            IDPPackage package = new IDPPackage();
            IDPExHeader header = new IDPExHeader();
            header.DestPort = bytes[0];
            header.SourcePort = bytes[1];
            header.options = (IDPOptions)bytes[2];
            header.Seq = bytes[3];
            header.Ack = bytes[4];
            package.Header = header;
            package.Data = new byte[bytes.Length - 5];
            Array.Copy(bytes, 5, package.Data, 0, package.Data.Length);
            return package;
        }

        public byte[] GetBytes()
        {
            byte[] headerBytes = Header.GetBytes();
            byte[] returnData = new byte[headerBytes.Length + Data.Length];
            Array.Copy(headerBytes, 0, returnData, 0, headerBytes.Length);
            Array.Copy(Data, 0, returnData, headerBytes.Length, Data.Length);
            return returnData;
        }
    }
}
