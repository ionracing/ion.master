﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.NewStack
{
    public class LoopbackDataLink : DataLinkInterface
    {
        Queue<byte[]> loopbackQueue = new Queue<byte[]>();

        long totalReceived = 0;
        public override long TotalReceived { get { return totalReceived; } }

        long totalSent = 0;
        public override long TotalSent { get { return totalSent; } }

        protected override void Send(byte[] data)
        {
            totalSent += data.Length;
            loopbackQueue.Enqueue(data);
        }

        protected override byte[] ReceiveBase()
        {
            while (loopbackQueue.Count == 0)
                System.Threading.Thread.Sleep(1);
            byte[] bytes = loopbackQueue.Dequeue();
            totalReceived += bytes.Length;
            return bytes;
        }
    }
}
