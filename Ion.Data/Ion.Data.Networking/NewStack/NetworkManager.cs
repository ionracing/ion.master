﻿using Ion.Data.Networking.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.NewStack
{
    public static class NetworkManager
    {
        static Dictionary<int, NetworkInterface> allInterfaces = new Dictionary<int, NetworkInterface>();
        static Dictionary<int, NetworkStack> registerdStacks = new Dictionary<int, NetworkStack>();
        public static IEnumerable<NetworkInterface> Interfaces
        {
            get
            {
                return allInterfaces.Values;
            }
        }
        public static ILogger DefaultLogger { get; set; }
        public static bool LoggReceived { get; set; }
        public static bool ThrowExceptions { get; set; } = false;

        static NetworkManager()
        {

        }

        public static bool IsInterfaceAvailable(int i)
        {
            return allInterfaces.ContainsKey(i);
        }

        public static void RegisterInterface(int id, NetworkInterface netInterface)
        {
            DefaultLogger?.WriteInfo($"Registering interface: {netInterface.GetType().Name} with interface ID: {id}");
            allInterfaces.Add(id, netInterface);
            StartReceive(netInterface);
        }

        public static void RegisterStack(NetworkStack stack, int protocolID)
        {
            DefaultLogger?.WriteInfo($"Registering stack: {stack.GetType().Name} with stack ID: {protocolID}");
            registerdStacks.Add(protocolID, stack);
        }

        private static async void StartReceive(NetworkInterface netInterface)
        {
            InterfacePackage pack = null;
            try
            {
                pack = await Task.Run<InterfacePackage>(new Func<InterfacePackage>(netInterface.Receive));
            }
            catch (Exception e)
            {
                DefaultLogger.WriteException("Error with receiving package", e, 3);
                if (ThrowExceptions)
                    throw;
            }
            if (pack != null)
            {
                if (LoggReceived)
                    DefaultLogger?.WriteInfo("Received package destind for: " + pack.ProtocolID);
                if (registerdStacks.ContainsKey(pack.ProtocolID))
                {
                    //DefaultLogger?.WriteInfo("Sending to stack");
                    registerdStacks[pack.ProtocolID].ProcessPackage(pack);
                }
                else
                {
                    DefaultLogger?.WriteInfo($"Dropping package destined for {pack.ProtocolID}");
                }
            }
            else
            {
                DefaultLogger.WriteInfo("Received pack was null!");
            }
            StartReceive(netInterface);
        }

        public static void Send(int id, InterfacePackage package)
        {
            try
            {
                if (allInterfaces.ContainsKey(id))
                {
                    allInterfaces[id].Send(package);
                }
                else
                {
                    throw new KeyNotFoundException($"An interface with ID: {id} dose not exist");
                }
            }
            catch (Exception e)
            {
                DefaultLogger?.WriteException("Error in sending datapackage to " + id.ToString(), e, 3);
                if (ThrowExceptions)
                    throw;
            }
        }
    }

    public abstract class NetworkStack
    {
        public abstract void ProcessPackage(InterfacePackage package);

        public virtual void SendPackage(int id, InterfacePackage package)
        {
            NetworkManager.Send(id, package);
        }
    }

    public interface INetHeader
    {
        ushort ProtocolID { get; }
    }

    public interface INetPackage
    {
        INetHeader Header { get; }
        byte[] GetBytes();
    }

    public abstract class NetworkInterface
    {
        public abstract long TotalReceived { get; }
        public abstract long TotalSent { get; }

        public abstract void Send(InterfacePackage package);
        public abstract InterfacePackage Receive();
    }

    public class InterfacePackage
    {
        public int ProtocolID { get; set; }
        public virtual byte[] Data { get; set; }

        public virtual INetPackage Package { get; set; }

        public InterfacePackage()
        {
        }

        public InterfacePackage(byte[] data)
            : this(0, data)
        {
        }

        public InterfacePackage(int protocolID, byte[] data)
        {
            this.Data = data;
            this.ProtocolID = protocolID;
        }

        public InterfacePackage(INetPackage package)
        {
            this.ProtocolID = package.Header.ProtocolID;
            this.Data = package.GetBytes();
            this.Package = package;
        }

        public virtual byte[] ProcessBytes()
        {
            return Data;
        }
    }
}
