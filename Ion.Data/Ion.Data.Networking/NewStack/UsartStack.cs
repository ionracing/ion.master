﻿using Ion.Data.Networking.Net;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.NewStack
{
    public class PiUsartInterface : NetworkInterface
    {
        SerialPort port;// = new SerialPort();
        Random rnd = new Random();
        InterfaceReader reader;

        public override long TotalReceived { get { return reader.TotalRead; } }

        long totalSent = 0;
        public override long TotalSent { get { return totalSent; } }

        public PiUsartInterface()
        {

        }

        public PiUsartInterface(SerialPort port)
        {
            this.port = port;
            if (!port.IsOpen)
                port.Open();
            reader = new InterfaceReader(port.BaseStream);
        }

        public PiUsartInterface(Stream s)
        {
            reader = new InterfaceReader(s);
        }

        public override InterfacePackage Receive()
        {
            byte[] buffer = new byte[8];// { 0xff, 0xff, 0x23, 0x06, 0x01, 0x6c, 0x25, 0x5B };
            reader.Read(buffer, buffer.Length, new byte[] { 0xFF, 0xFF });
            InterfacePackage package;
            ushort id = (ushort)(buffer[2] | (buffer[3] << 8));
            if (id == 211)
            {
                List<byte> allBytes = new List<byte>();
                allBytes.AddRange(buffer);
                allBytes.RemoveRange(0, 3);
                byte b = 0;
                while ((b = reader.ReadByte()) != '\n')
                    allBytes.Add(b);
                package = new InterfacePackage(11, allBytes.ToArray());
            }
            else if (id >= 0x622 || id <= 0x628)
            {
                byte[] tempBuffer = new byte[8];// { 0xff, 0xff, 0x23, 0x06, 0x25, 0x5f, 0, 0 };
                reader.Read(tempBuffer, buffer.Length, new byte[] { 0xFF, 0xFF });
                ushort id2 = (ushort)(tempBuffer[2] | (tempBuffer[3] << 8));
                if (id == id2)
                {
                    
                    buffer = SensorConverter.ConvertBytes(buffer, tempBuffer);
                    package = new InterfacePackage(10, buffer);
                }
                else
                {
                    byte[] tempBuff = { buffer[2], buffer[3], buffer[4], buffer[5], buffer[6], buffer[7], tempBuffer[2], tempBuffer[3], tempBuffer[4], tempBuffer[5], tempBuffer[6], tempBuffer[7] };
                    package = new InterfacePackage(10, tempBuff);
                }
            }
            else
            {
                byte[] tempBuff = { buffer[2], buffer[3], buffer[4], buffer[5], buffer[6], buffer[7] };
                package = new InterfacePackage(10, tempBuff);
            }
            
            return package;
        }

        public override void Send(InterfacePackage package)
        {
            byte[] bytePackage = package.ProcessBytes();
            totalSent += bytePackage.Length;
            port.Write(bytePackage, 0, bytePackage.Length);
        }
    }

    public class UsartInterfaceLink : NetworkStack
    {
        public int ID { get; set; }

        public UsartInterfaceLink(int ID)
        {
            this.ID = ID;
        }

        public override void ProcessPackage(InterfacePackage package)
        {
            UsartStack.ProcessPack(this.ID, package);
        }
    }

    public static class UsartStack
    {
        static UsartStack()
        {
            //bytes.Add(10, new Queue<byte[]>());
            //bytes.Add(11, new Queue<byte[]>());
            NetworkManager.RegisterStack(new UsartInterfaceLink(10), 10);
            NetworkManager.RegisterStack(new UsartInterfaceLink(11), 11);
        }

        //static Dictionary<int, Queue<byte[]>> bytes = new Dictionary<int, Queue<byte[]>>();
        static Queue<byte[]> bytes10 = new Queue<byte[]>();
        static Queue<byte[]> bytes11 = new Queue<byte[]>();

        public static void ProcessPack(int id, InterfacePackage pack)
        {
            if (id == 10)
            {
                lock (bytes10)
                {
                    bytes10.Enqueue(pack.Data);
                }
            }
            else if (id == 11)
            {
                lock (bytes11)
                {
                    bytes11.Enqueue(pack.Data);
                }
            }
            /*
            if (bytes.ContainsKey(id))
            {
                lock (bytes)
                {
                    bytes[id].Enqueue(pack.Data);
                }
            }*/
        }

        public static byte[] ReadNext(int id)
        {
            byte[] temp;

            if (id == 10)
            {
                while (bytes10.Count == 0)
                    System.Threading.Thread.Sleep(1);
                lock (bytes10)
                {
                    temp = bytes10.Dequeue();
                }
                return temp;
            }
            else if(id == 11)
            {
                while (bytes11.Count == 0)
                    System.Threading.Thread.Sleep(1);
                lock (bytes11)
                {
                    temp = bytes11.Dequeue();
                }
                return temp;
            }
            else
                throw new ArgumentException("That id dose not exist");
        }

        public static void Write(int iid, byte[] bytes)
        {
            NetworkManager.Send(iid, new InterfacePackage(10, bytes));
        }
    }

    public class CanLinkClient : Manager.DataClient
    {
        int id;
        public CanLinkClient(int id)
        {
            this.id = id;
        }

        public override byte[] Receive()
        {
            //Console.WriteLine("CanLink Receive");
            return UsartStack.ReadNext(id);
        }

        public override void Send(byte[] data)
        {
            UsartStack.Write(10, data);
        }
    }
}
