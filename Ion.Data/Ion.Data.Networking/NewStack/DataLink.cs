﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.NewStack
{
    public class DataLinkHeader
    {
        public ushort ProtocolID { get; set; }
        public ushort CheckSum { get; set; }
        public byte Length { get; set; }

        public byte[] GetBytes()
        {

            byte[] temp = new byte[5];


            Array.Copy(BitConverter.GetBytes(ProtocolID), 0, temp, 0, 2);
            Array.Copy(BitConverter.GetBytes(CheckSum), 0, temp, 2, 2);
            temp[4] = (byte)Length;

            return temp;
        }
    }

    public abstract class DataLinkInterface : NetworkInterface
    {
        public void Send(INetPackage package)
        {
            DataLinkPackage pack = new DataLinkPackage();
            pack.Package = package;
            Send(pack.ProcessBytes());
        }

        public override void Send(InterfacePackage package)
        {
            DataLinkPackage pack = new DataLinkPackage(package);
            //pack.Data = package.ProcessBytes();
            Send(pack.ProcessBytes());
        }

        protected abstract void Send(byte[] data);

        public override InterfacePackage Receive()
        {
            return DataLinkPackage.Parse(ReceiveBase());
        }

        protected abstract byte[] ReceiveBase();
    }

    public class DataLinkPackage : InterfacePackage
    {
        public DataLinkHeader Header { get; set; }

        public DataLinkPackage()
        {

        }

        public DataLinkPackage(InterfacePackage package)
        {
            this.Data = package.Data;
            this.ProtocolID = package.ProtocolID;
            this.Package = package.Package;
        }

        public override byte[] ProcessBytes()
        {
            Header = new DataLinkHeader();
            Header.ProtocolID = (ushort)this.ProtocolID;

            byte[] packageBytes = this.Data;
            if (packageBytes.Length > 255)
                throw new IndexOutOfRangeException("Given header is to large for the protocol");
            Header.Length = (byte)packageBytes.Length;
            //TODO: Implement DataLink Checksum
            Header.CheckSum = 0;
            Data = Header.GetBytes();

            byte[] totalPackage = new byte[packageBytes.Length + Data.Length];
            Array.Copy(Data, 0, totalPackage, 0, Data.Length);
            Array.Copy(packageBytes, 0, totalPackage, Data.Length, packageBytes.Length);
            return totalPackage;
        }

        public static DataLinkPackage Parse(byte[] bytes)
        {
            DataLinkPackage package = new DataLinkPackage();
            package.Header = new DataLinkHeader();
            package.Header.ProtocolID = BitConverter.ToUInt16(bytes, 0);
            package.ProtocolID = package.Header.ProtocolID;
            package.Header.CheckSum = BitConverter.ToUInt16(bytes, 2);
            package.Header.Length = bytes[4];
            package.Data = new byte[package.Header.Length];
            Array.Copy(bytes, 5, package.Data, 0, package.Header.Length);
            return package;
        }
    }
}
