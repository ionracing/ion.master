﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.NewStack
{
    //TODO: Not implemented
    public class UDPDataLink : DataLinkInterface
    {
        UdpClient sendClient;
        UdpClient receiveClient;
        IPEndPoint ipEndPoint;

        public UDPDataLink(int sourcePort)
        {
            receiveClient = new UdpClient(new IPEndPoint(IPAddress.Any, sourcePort));
            sendClient = new UdpClient();
        }
        long totalReceived;
        public override long TotalReceived
        {
            get
            {
                return totalReceived;
            }
        }

        long totalSent;
        public override long TotalSent
        {
            get
            {
                return totalSent;
            }
        }

        public void Connect(int destination)
        {
            ipEndPoint = new IPEndPoint(IPAddress.Loopback, destination);
        }

        protected override byte[] ReceiveBase()
        {
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, 0);
            byte[] bytes = receiveClient.Receive(ref endPoint);
            totalReceived += bytes.Length;
            return bytes;
        }

        protected override void Send(byte[] data)
        {
            sendClient.Send(data, data.Length, ipEndPoint);
            totalSent += data.Length;
        }
    }
}
