﻿using Ion.Data.Networking.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.NewStack
{
    public static class SensorConverter
    {
        public static byte[] ConvertBytes(byte[] buffer, byte[] tempBuffer, bool copySub = true)
        {
            byte[] buf1 = copySub ? buffer.SubArray(2, 6) : buffer;
            byte[] buf2 = copySub ? tempBuffer.SubArray(2, 6) : tempBuffer;
            List<byte> allBytes = new List<byte>();
            allBytes.AddRange(buf1);
            allBytes.AddRange(buf2);
            int id = buf1[0] | buf1[1] << 8;
            switch (id)
            {
                case 0x623:
                    allBytes.AddRange(GeneratePackage(0xF001, ToBigUInt16(buf1, 2))); //Pack voltage
                    allBytes.AddRange(GeneratePackage(0xF002, buf1[4])); // Min Vtg
                    allBytes.AddRange(GeneratePackage(0xF003, buf1[5])); // min Vtg#
                    allBytes.AddRange(GeneratePackage(0xF004, buf2[2])); // Max vtg
                    allBytes.AddRange(GeneratePackage(0xF005, buf2[3])); // Max vtg #
                    break;
                case 0x624:
                    allBytes.AddRange(GeneratePackage(0xF006, ToBigUInt16(buf1, 2))); // Current
                    allBytes.AddRange(GeneratePackage(0xF007, ToBigUInt16(buf1, 4))); //Charge Limit
                    allBytes.AddRange(GeneratePackage(0xF008, ToBigUInt16(buf2, 2))); //Discharge limit
                    break;
                case 0x625:
                    allBytes.AddRange(GeneratePackage(0xF009, ToBigUInt32(buf1, 2))); // Batt. energy in
                    allBytes.AddRange(GeneratePackage(0xF00A, ToBigUInt32(buf2, 2))); // Batt. energy out
                    break;
                case 0x626:
                    allBytes.AddRange(GeneratePackage(0xF00B, buf1[2])); //SOC
                    allBytes.AddRange(GeneratePackage(0xF00C, ToBigUInt16(buf1, 3))); //DOD
                    allBytes.AddRange(GeneratePackage(0xF00D, buf2[3])); //Capacity
                    allBytes.AddRange(GeneratePackage(0xF00E, buf2[4])); //00h
                    allBytes.AddRange(GeneratePackage(0xF00F, buf2[5])); //SOH
                    break;
                case 0x627:
                    allBytes.AddRange(GeneratePackage(0xF010, buf1[2])); // Temperature
                    allBytes.AddRange(GeneratePackage(0xF011, buf1[4])); //Min Temp
                    allBytes.AddRange(GeneratePackage(0xF012, buf1[5])); //Min Temp #
                    allBytes.AddRange(GeneratePackage(0xF013, buf2[2])); //Max temp
                    allBytes.AddRange(GeneratePackage(0xF014, buf2[3])); //Max temp #
                    break;
                case 0x628:
                    allBytes.AddRange(GeneratePackage(0xF015, ToBigUInt16(buf1, 2))); //Pack resistance
                    allBytes.AddRange(GeneratePackage(0xF016, buf1[4])); //Min Res
                    allBytes.AddRange(GeneratePackage(0xF017, buf1[5])); //Min Res#
                    allBytes.AddRange(GeneratePackage(0xF018, buf2[2])); //Max Res
                    allBytes.AddRange(GeneratePackage(0xF019, buf2[3])); //Max Res#
                    break;
            }
            return allBytes.ToArray();
        }

        public static ushort ToBigUInt16(byte[] buffer, int startIndex)
        {
            return (ushort)(buffer[startIndex] << 8 | buffer[startIndex + 1]);
        }

        public static UInt32 ToBigUInt32(byte[] buffer, int startIndex)
        {
            return (ushort)(buffer[startIndex] << 24 | buffer[startIndex + 1] << 16 | buffer[startIndex + 2] << 8 | buffer[startIndex + 3]);
        }

        public static byte[] GeneratePackage(int id, uint data)
        {
            byte[] bytes = BitConverter.GetBytes((ushort)id);
            byte[] dataBytes = BitConverter.GetBytes(data);
            return new byte[] { bytes[0], bytes[1], dataBytes[0], dataBytes[1], dataBytes[2], dataBytes[3] };
        }

        public static byte[] SubArray(this byte[] source, int start, int length)
        {
            byte[] temp = new byte[6];
            Array.Copy(source, start, temp, 0, length);
            return temp;
        }
    }
}
