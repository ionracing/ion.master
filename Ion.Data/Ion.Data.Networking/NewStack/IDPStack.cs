﻿using Ion.Data.Networking.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.NewStack
{
    public class IDPStackLink : NetworkStack
    {
        public override void ProcessPackage(InterfacePackage package)
        {
            IDPStack.ProcessIdp(package);
        }
    }

    public class IDPExStackLink : NetworkStack
    {
        public override void ProcessPackage(InterfacePackage package)
        {
            IDPStack.ProcessExtended(package);
        }
    }

    public static class IDPStack
    {
        static IDPStack()
        {
            NetworkManager.RegisterStack(new IDPStackLink(), IDPHeader.IDPHeaderID);
            NetworkManager.RegisterStack(new IDPExStackLink(), IDPExHeader.IDPExHeaderID);
        }

        static List<IDPPackage> allPackages = new List<IDPPackage>();

        public static void ProcessIdp(InterfacePackage package)
        {
            IDPPackage pack = IDPPackage.Parse(package.Data);
            lock (allPackages)
            {
                allPackages.Add(pack);
            }
        }

        public static void ProcessExtended(InterfacePackage package)
        {

        }

        public static byte[] ReadAtPort(int port)
        {
            while (true)
            {
                for (int i = 0; i < allPackages.Count; i++)
                {
                    IDPPackage p;
                    lock (allPackages)
                    {
                        p = allPackages[i];
                    }
                    if (p.Header.DestPort == port)
                    {
                        allPackages.Remove(p);
                        return p.Data;
                    }

                }
                System.Threading.Thread.Sleep(1);
            }
        }

        public static void SendToPort(int iid, int port, byte[] bytes)
        {
            IDPPackage package = new IDPPackage(bytes, (byte)port);

            NetworkManager.Send(iid, new InterfacePackage(package));
        }
    }

    public class IdpClient : DataClient
    {
        public int DefaultInterface { get; set; }

        public int SendPort { get; set; }
        public int ReceivePort { get; set; }

        public IdpClient()
        {

        }

        public IdpClient(int sourcePort)
        {
            this.ReceivePort = sourcePort;
        }

        public void Connect(int port)
        {
            this.SendPort = port;
        }

        public void Connect(int port, int defaultInterface)
        {
            Connect(port);
            this.DefaultInterface = defaultInterface;
        }

        public void Send(int port, byte[] bytes)
        {
            Send(DefaultInterface, port, bytes);
        }

        public void Send(int iid, int port, byte[] bytes)
        {
            IDPStack.SendToPort(iid, port, bytes);
        }

        public byte[] Receive(int port)
        {
            return IDPStack.ReadAtPort(port);
        }

        public override byte[] Receive()
        {
            return Receive(ReceivePort);
        }

        public override void Send(byte[] data)
        {
            Send(SendPort, data);
        }
    }
}
