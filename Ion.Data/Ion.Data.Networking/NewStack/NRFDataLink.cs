﻿using NicroWare.Pro.RPiSPITest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.NewStack
{
    public sealed class NRFDataLink : DataLinkInterface, IDisposable
    {
        NRFRadio baseRadio;
        bool disposed = false;
        private bool fastWriteMode;
        public bool FastWriteMode
        {
            get
            {
                return fastWriteMode;
            }
            set
            {
                fastWriteMode = value;
                if (fastWriteMode)
                {
                    baseRadio.StopListening();
                }
                else
                {
                    baseRadio.StartListening();
                }
            }
        }
        public NRFDataLink()
        {
            baseRadio = new NRFRadio();
            baseRadio.Begin();
            baseRadio.SetPALevel(RF24PaDbm.RF24_PA_HIGH);
            baseRadio.SetDataRate(RF24Datarate.RF24_250KBPS);
            baseRadio.SetRetries(0, 0);
            baseRadio.SetAutoAck(false);
            baseRadio.OpenReadingPipe(0, "00001");
            baseRadio.OpenWritingPipe("00001");
            FastWriteMode = false;
        }

        ~NRFDataLink()
        {
            Dispose(false);
        }

        long totalReceived;
        public override long TotalReceived
        {
            get
            {
                return totalReceived;
            }
        }

        long totalSent;
        public override long TotalSent
        {
            get
            {
                return totalSent;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                baseRadio?.Dispose();
                //Managed resources go here
            }
            disposed = true;

        }

        protected override byte[] ReceiveBase()
        {
            byte[] buffer = new byte[32];
            while (fastWriteMode || !baseRadio.Available()) //Sleeps while nothing is available
            {
                System.Threading.Thread.Sleep(10);
            }
            if (baseRadio.Available())
            {
                while (true)
                {
                    baseRadio.Read(buffer, 32);
                    if (buffer[0] == 0 && buffer[1] == 0)
                        System.Threading.Thread.Sleep(1);
                    else
                        break;
                }
                totalReceived += 32;
            }
            return buffer;
        }

        public byte[] RawReceive()
        {
            return ReceiveBase();
        }

        public void RawSend(byte[] bytes)
        {
            Send(bytes);
        }

        protected override void Send(byte[] data)
        {
            if (!fastWriteMode)
                baseRadio.StopListening();
            //Console.WriteLine("NRF sending nr: " + (counter++).ToString());
            totalSent += 32; //Because it receives only in 32 so to not make confusion this is also 32

            baseRadio.Write(data, (byte)data.Length);
            //baseRadio.WriteFast(data, (byte)data.Length);
            //Console.WriteLine("NRF total sent" + (totalSent).ToString());
            if (!fastWriteMode)
                baseRadio.StartListening();
        }
    }
}
