﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking
{
    /// <summary>
    /// List of currents ports
    /// </summary>
    public class NetPorts
    {
        public const ushort DataRecive = 160;
        public const ushort DataSend = 161;
        public const ushort ScreenRecive = 16200;
        public const ushort ScreenSend = 16300;

        public const ushort SensorDataList = 9940;

        public const ushort CoreComServer = 9950;
        public const ushort LogServerUDP = 9965;
        public const ushort LogServerTCP = 9965;
        public const ushort WebServer = 9966;
    }
}
