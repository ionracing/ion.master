﻿using Ion.Data.Networking.Manager;
using Ion.Data.Networking.NewStack;
using Ion.Data.Sensor;
using Ion.Graphics.IonEngine;
using Ion.Graphics.IonEngine.Drawing;
using Ion.Graphics.IonEngine.Events;
using Ion.Graphics.IonEngine.Font;
using Ion.Graphics.IonEngine.Gui;
using NicroWare.Lib.IonEngine.Gui;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forms = System.Windows.Forms;

namespace Ion.Pro.Analyzer
{
    class MainWindow : GuiWindow
    {
        SDLRenderer mainRenderer;
        KeyboardState prev;
        KeyboardState cur;
        

#pragma warning disable
        PlotDisplay plotDisplay_test;
        Label label_packages;
        Label label_curfile;
        ScrollView scrollView_DataPlot;
        ScrollView scrollView_FileList;
        Checkbox checkbox_ToggleLines;

#pragma warning restore
        FileNetworkClient fileClient;

        DataPlot speedPlot = new DataPlot(SDLColor.DarkRed, "Speed");
        DataPlot socPlot = new DataPlot(SDLColor.DarkGreen, "SOC");
        DataPlot tempBatPlot = new DataPlot(new SDLColor(127, 127, 0), "TempBat");
        DataPlot tempPlot = new DataPlot(new SDLColor(127, 0, 127), "TempCool");
        DataPlot accelX = new DataPlot(new SDLColor(40, 80, 120), "AccelerometerX");
        DataPlot accelY = new DataPlot(new SDLColor(120, 80, 40), "AccelerometerY");
        DataPlot accelZ = new DataPlot(new SDLColor(40, 120, 80), "AccelerometerZ");

        string logDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "DataLog") + "\\";
        FileInfo currentFile;

        bool createCSVFile = false;
        bool parseBMSValues = true;

        public MainWindow()
        {
            mainRenderer = SDLRenderer.Create(this);
            mainRenderer.DrawColor = SDLColor.DarkGray;
            FileInfo fontFile = new FileInfo("Content/unispace.ttf");
            Global.Font = SDLFont.LoadFont(fontFile.FullName, 50);
            InitializeComponents(mainRenderer);
            FrameRate = 60;
            FrameLock = true;
            ShowCursor = true;
            checkbox_ToggleLines.Checked = true;
            LoadPlotFileInfo();
        }

        public void checkbox_CheckChange(object sender, EventArgs e)
        {
            plotDisplay_test.DrawLines = checkbox_ToggleLines.Checked;
        }

        public void LoadPlotFileInfo()
        {
            int counter = -1;
            DirectoryInfo di = new DirectoryInfo(logDir);
            if (!di.Exists)
                di.Create();
            
            foreach (FileInfo fi in di.GetFiles("*.log"))
            {
                int num = int.Parse(fi.Name.Split('_')[0]);
                counter = Math.Max(num, counter);
                Label l = new Label();
                l.Text = fi.Name;
                l.Size = new SDLPoint(200, 20);
                scrollView_FileList.Elements.Add(l);
                l.Click += (sender, e) =>
                {
                    string fileName = fi.Name;
                    currentFile = fi;
                    fileClient = new FileNetworkClient(logDir + fileName) { PackageMode = WrapperMode.NormalMode };
                    label_curfile.Text = fileName;
                    LoadPlot(fileClient);
                };
            }
            if (counter > -1)
            {
                string fileName = counter.ToString("000") + "_usart_data.log";
                currentFile = new FileInfo(logDir + fileName);
                fileClient = new FileNetworkClient(logDir + fileName) { PackageMode = WrapperMode.NormalMode };
                label_curfile.Text = fileName;
                LoadPlot(fileClient);
            }
        }

        Dictionary<int, List<DataWrapper>> allValues;
        

        public void LoadPlot(DataClient client)
        {
            allValues = new Dictionary<int, List<DataWrapper>>();
            scrollView_DataPlot.Elements.Clear();
            plotDisplay_test.Clear();
            DataWrapper[] all = client.ReadDataWrappers(true);
            int lastHighRes = 0;
            StreamWriter writer = new StreamWriter("gpsData.csv");
            Dictionary<int, DataPlot> allPlots = new Dictionary<int, DataPlot>();
            DataWrapper lastDw = default(DataWrapper);
            for (int i = 0; i < all.Length; i++)
            {
                DataWrapper dw = all[i];
                if (!allValues.ContainsKey(dw.SensorID))
                    allValues.Add(dw.SensorID, new List<DataWrapper>());
                allValues[dw.SensorID].Add(dw);
                if (dw.SensorID == 252)
                {
                    lastHighRes = dw.Value;
                }
                else if (dw.SensorID == 251 || dw.SensorID == 250)
                {
                    char dir = 'N';
                    int raw = dw.Value;
                    int deg = raw / (100 * 60);
                    int min = (raw - (deg * 100 * 60)) / (100);
                    int sec = (raw - (deg * 100 * 60) - (min * 100)) * 100;
                    if (dw.SensorID == 250)
                    {
                        sec += lastHighRes >> 12;
                        if (deg >= 90)
                        {
                            deg -= 90;
                            dir = 'S';
                        }
                        writer.Write($"\"{deg} {min}.{sec}{dir}\", ");
                    }
                    if (dw.SensorID == 251)
                    {
                        dir = 'E';
                        sec += lastHighRes & 0xFFF;
                        if (deg >= 180)
                        {
                            deg -= 180;
                            dir = 'W';
                        }
                        writer.WriteLine($"\"{deg} {min}.{sec}{dir}\"");
                    }
                }
                else if (dw.SensorID >= 0x623 && dw.SensorID <= 0x628 && parseBMSValues)
                {
                    byte[] bytes = dw.GetBytes(WrapperMode.NormalMode);
                    byte[] bytes2 = all[++i].GetBytes(WrapperMode.NormalMode);
                    List<DataWrapper> allWrappers = new List<DataWrapper>();
                    byte[] parsedBytes = SensorConverter.ConvertBytes(bytes, bytes2, false);
                    for (int j = 0; j < parsedBytes.Length; j += 6)
                    {
                        DataWrapper temp = DataWrapper.ReadData(parsedBytes, j, WrapperMode.NormalMode);
                        temp.TimeStamp = dw.TimeStamp;
                        SensorLookup lookup = SensorLookup.GetById(temp.SensorID);

                        if (j > 1)
                        {
                            if (!allPlots.ContainsKey(temp.SensorID))
                            {
                                allPlots.Add(temp.SensorID, new DataPlot(SDLColor.RandomColor(0, 255 + 128), lookup?.DisplayName ?? "Unkown id: " + temp.SensorID));
                            }
                            allPlots[temp.SensorID].Add(new SDLDPoint(temp.TimeStamp, lookup?.ConvertValue(temp.Value) ?? temp.Value));
                            if (!allValues.ContainsKey(temp.SensorID))
                                allValues.Add(temp.SensorID, new List<DataWrapper>());
                            allValues[temp.SensorID].Add(temp);
                        }
                    }
                }
                else
                {
                    //if (lastDw.TimeStamp > dw.TimeStamp)
                      //  continue;
                    lastDw = dw;
                    SensorLookup lookup = SensorLookup.GetById(dw.SensorID);
                    if (!allPlots.ContainsKey(dw.SensorID))
                    {
                        allPlots.Add(dw.SensorID, new DataPlot(SDLColor.RandomColor(0, 255 + 128), lookup?.DisplayName ?? "Unkown id: " + dw.SensorID));
                    }
                    allPlots[dw.SensorID].Add(new SDLDPoint(dw.TimeStamp, lookup?.ConvertValue(dw.Value) ?? dw.Value));
                }
                label_packages.Text = "Package count: " + all.Length.ToString();
                
            }
            writer.Close();
            TextWriter tw = null;
            if (createCSVFile)
                tw = new StreamWriter("CSVData.csv");
            foreach (DataPlot plot in allPlots.Values)
            {
                if (createCSVFile)
                {
                    tw.Write(plot.Title + ";");
                    foreach (var value in plot.Points)
                    {
                        tw.Write(value.Y + ";");
                    }
                    tw.WriteLine();
                }
                plotDisplay_test.Add(plot);
                Checkbox l = new Checkbox();
                l.Text = plot.Title + "(" + plot.Points.Count + ")";
                l.Size = new SDLPoint(200, 20);
                scrollView_DataPlot.Elements.Add(l);
                plot.Visible = false;
                l.CheckedChange += (sender, e) =>
                {
                    plot.Visible = l.Checked;
                };
            }
            if (createCSVFile)
                tw.Close();
            scroller.CurrentPage = 1;
        }

        public void button_SaveRawCsv_Click(object sender, EventArgs e)
        {
            if (currentFile == null || allValues == null)
                return;
            TextWriter tw = new StreamWriter(new FileStream(logDir + currentFile.Name + ".raw.csv", FileMode.Create, FileAccess.ReadWrite));
            int max = 0;

            foreach (int i in allValues.Keys)
            {
                max = Math.Max(allValues[i].Count, max);
                tw.Write((SensorLookup.GetById((ushort)i)?.DisplayName ?? "Unknown id: " + i.ToString()) + ";");
            }
            tw.WriteLine();
            for (int i = 0; i < max; i++)
            {
                foreach (int key in allValues.Keys)
                {
                    if (allValues[key].Count > i)
                    {
                        tw.Write(allValues[key][i].Value.ToString());
                    }
                    tw.Write(";");
                }
                tw.WriteLine();
            }
            tw.Close();

                //TODO: Fix problem with downloading file from raspberry pi, should close and reopen, not have it open all the time;
        }

        public void button_SaveCsv_Click(object sender, EventArgs e)
        {
            if (currentFile == null || allValues == null)
                return;
            TextWriter tw = new StreamWriter(new FileStream(logDir + currentFile.Name + ".csv", FileMode.Create, FileAccess.ReadWrite));
            int max = 0;

            foreach (int i in allValues.Keys)
            {
                max = Math.Max(allValues[i].Count, max);
                tw.Write((SensorLookup.GetById((ushort)i)?.DisplayName ?? "Unknown id: " + i.ToString()) + ";");
            }
            tw.WriteLine();
            for (int i = 0; i < max; i++)
            {
                foreach (int key in allValues.Keys)
                {
                    if (allValues[key].Count > i)
                    {
                        tw.Write(allValues[key][i].CalculateValue().ToString());
                    }
                    tw.Write(";");
                }
                tw.WriteLine();
            }
            tw.Close();

            //TODO: Fix problem with downloading file from raspberry pi, should close and reopen, not have it open all the time;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            prev = cur;
            cur = Keyboard.GetState();

            if (prev.IsKeyUp(Keys.F5) && cur.IsKeyDown(Keys.F5))
            {
                scroller.Elements.Clear();
                InitializeComponents(mainRenderer);
            }

            scroller.Update(gameTime);
            //counter += Math.PI / 100;
        }

        //double counter = 0;

        public override void Draw(GameTime gameTime)
        {
            mainRenderer.Clear();
            scroller.Draw(mainRenderer, gameTime);
            //mainRenderer.DrawLine(new SDLPoint(200, 200), new SDLPoint(200 + (int)(Math.Sin(counter)* 200), 200 + (int)(Math.Cos(counter) * 200)), SDLColor.White);
            mainRenderer.Present();
        }
    }
}
