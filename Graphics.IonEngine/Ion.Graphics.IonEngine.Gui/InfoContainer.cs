﻿using System;
using Ion.Graphics.IonEngine.Drawing;

namespace Ion.Graphics.IonEngine.Gui
{
    public class InfoContainer : CarGuiElement
    {

        SDLTexture infoContainerTexture;

        public InfoContainer()
        {
            BackgroundColor = new SDLColor(10, 10, 10);
            Size = new SDLPoint(100, 100);
            Location = new SDLPoint(0, 0);
        }

        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);
            DrawInfoContainer(renderer);
        }

        public void DrawInfoContainer(SDLRenderer renderer)
        {
            DrawSurface infoContainerSurf = new DrawSurface(Size.X, Size.Y);

            for (int y = 0; y < Size.Y; y++)
            {
                for (int x = 0; x < Size.X; x++)
                {
                    bool leftTriangle = x < (Size.Y - y);
                    bool rightTriangle = (Size.X - x) < (Size.Y - y);

                    if (!leftTriangle && !rightTriangle)
                        infoContainerSurf[x, y] = BackgroundColor;
                }
            }
            infoContainerTexture = infoContainerSurf.MakeTexture(renderer);
            infoContainerTexture.BlendMode = BlendMode.Blend;
        }    
        
        public override void Draw(SDLRenderer renderer, GameTime gameTime)
        {
            renderer.DrawTexture(infoContainerTexture, new SDLRectangle(0, 0, Size.X, Size.Y));
        }
    }
}