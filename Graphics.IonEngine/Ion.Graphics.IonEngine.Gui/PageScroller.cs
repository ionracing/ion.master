﻿using Ion.Graphics.IonEngine.Drawing;
using Ion.Graphics.IonEngine.Events;
using System.Collections.Generic;


namespace Ion.Graphics.IonEngine.Gui
{
    public class PageScroller : GuiContainerElement
    {
        public GuiElement ActiveElement { get; set; }
        int currentPage = 0;
        public static bool SideScroll = true;
        public bool ScrollActive { get; set; } = true;
        public int CurrentPage
        {
            get
            {
                return currentPage;
            }
            set
            {
                if (Elements.Count == 0)
                    return;
                else if (value < 0)
                    currentPage = 0;
                else if (value >= Elements.Count)
                    currentPage = Elements.Count - 1;
                else
                    currentPage = value;
                ActiveElement = Elements[currentPage];
                ActiveElement.Location = new SDLPoint(0, 0);
            }
        }
        MouseState beginDrag;
        bool draging = false;
        bool preDrag = false;

        public override void Update(GameTime gameTime)
        {
            if (ActiveElement == null && Elements.Count > 0)
                CurrentPage = 0;
            if (ActiveElement != null)
                ActiveElement.Update(gameTime);
            PullEvents();
            if (last.IsKeyUp(MouseButtons.Left) && current.IsKeyDown(MouseButtons.Left) && ScrollActive)
            {
                beginDrag = current;
                if (Global.SideScroll && SideScroll)
                    preDrag = true;
            }
            if (preDrag && Global.SideScroll && SideScroll)
                draging = true;
            else
                preDrag = false;
            if (current.IsKeyDown(MouseButtons.Left) && draging)
            {
                //TODO: Possible error if PageScroller is not at 0,0
                if (current.Location.X > 6 && current.Location.X < Size.X - 6) // Just because if became funky and unstable in the sides
                    ActiveElement.Location = new SDLPoint((current.Location.X - beginDrag.Location.X), 0);
            }
            else if (current.IsKeyUp(MouseButtons.Left) && last.IsKeyDown(MouseButtons.Left) && draging)
            {
                //Check if drag distance is long enough to do page switch
                int dragDistance = current.Location.X - beginDrag.Location.X;
                if (dragDistance < -100)
                    CurrentPage++;
                else if (dragDistance > 100)
                    CurrentPage--;
                else
                    ActiveElement.Location = new SDLPoint(0, 0);
                draging = false;
                preDrag = false;
            }
            //base.Update(gameTime);
        }

        public override void Draw(SDLRenderer renderer, GameTime gameTime)
        {
            ActiveElement?.Draw(ActiveElement.Renderer, gameTime);
        }
    }
}
