﻿using Ion.Graphics.IonEngine.Gui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ion.Graphics.IonEngine;
using Ion.Graphics.IonEngine.Drawing;

namespace Ion.Graphics.IonEngine.Gui
{
    public class ScrollView : GuiContainerElement
    {
        int elementMargin = 5;
        int scroll = 0;
        int startScroll = 0;
        int mouseDownHeight = 0;
        bool isScrolling = false;
        SDLTexture frame;
        int maxHeight;//+ = 0;
        public ScrollView()
        {
            maxHeight = elementMargin;
        }

        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);
            DrawSurface surf = new DrawSurface(Size.X, Size.Y);
            surf.DrawRectangle(new SDLRectangle(SDLPoint.Zero, Size), SDLColor.White);
            frame = surf.MakeTexture(renderer);
        }

        public override void Update(GameTime gameTime)
        {
            //base.Update(gameTime);
            base.PullEvents();
            if (IsMouseControlClicked())
            {
                isScrolling = true;
                Global.SideScroll = false;
                startScroll = scroll;
                mouseDownHeight = current.Location.Y - base.Renderer.GetRealLocation().Y;
                Console.WriteLine("Mouse down!");
            }
            else if (isScrolling && current.IsKeyUp(Events.MouseButtons.Left))
            {
                isScrolling = false;
                Global.SideScroll = true;
                Console.WriteLine("Mouse Down!");
            }
            if (isScrolling)
            {
                
                int curMouseHeight = current.Location.Y - base.Renderer.GetRealLocation().Y;
                int mouseMove = mouseDownHeight - curMouseHeight;
                scroll = startScroll + mouseMove;
                if (scroll < 0 || maxHeight < this.Size.Y)
                    scroll = 0;
                else if (scroll > maxHeight - this.Size.Y)
                    scroll = maxHeight - this.Size.Y;
                Console.WriteLine($"Scroll update: {scroll} curMouseHeight: {curMouseHeight} mouseMove: {mouseMove} maxHeight: {maxHeight} height: {this.Size.Y}");
            }

            int height = -scroll + elementMargin;

            foreach (GuiElement element in Elements)
            {
                int lastHeight = element.Size.Y;
                element.Location = new Ion.Graphics.IonEngine.Drawing.SDLPoint(elementMargin/*element.Location.X*/, height);
                if (height > 0 && height + lastHeight <= this.Size.Y)
                    element.Update(gameTime);
                height += lastHeight + elementMargin;
            }
        }

        public override void OnElementAdd(GuiElement e)
        {
            maxHeight += e.Size.Y + elementMargin;
        }

        public override void OnElementsCleard()
        {
            maxHeight = elementMargin;
        }

        public override void Draw(SDLRenderer renderer, GameTime gameTime)
        {
            renderer.DrawTexture(frame, new SDLRectangle(0, 0, Size.X, Size.Y));
            int height = -scroll + elementMargin;

            foreach (GuiElement element in Elements)
            {
                int lastHeight = element.Size.Y;
                element.Location = new Ion.Graphics.IonEngine.Drawing.SDLPoint(elementMargin/*element.Location.X*/, height);
                if (height > 0 && height + lastHeight <= this.Size.Y)
                    element.Draw(element.Renderer, gameTime);
                height += lastHeight + elementMargin;
            }
        }
    }
}
