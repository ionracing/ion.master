﻿using Ion.Graphics.IonEngine.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Graphics.IonEngine.Gui
{
    public class Checkbox : Label
    {
        SDLTexture square;
        SDLTexture check;
        bool isChecked;
        public bool Checked
        {
            get
            {
                return isChecked;
            }
            set
            {
                isChecked = value;
                OnCheckedChange(new EventArgs());
            }
        }

        public event EventHandler CheckedChange;

        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);
            DrawSurface checkedSurface = new DrawSurface(Size.Y, Size.Y);
            DrawSurface squareSurface = new DrawSurface(Size.Y, Size.Y).Initialize(
                s => s.CustomDraw((x, y) =>
                    {
                        if (x == 0 || x == Size.Y - 1 || y == 0 || y == Size.Y - 1)
                        {
                            s[x, y] = TextColor;
                        }
                        else
                            s[x, y] = SDLColor.NoColor;
                        if (x == y || x + y == Size.Y - 1)
                        {
                            checkedSurface[x, y] = TextColor;
                        }
                    })
                );
            square = squareSurface.MakeTexture(renderer);
            check = checkedSurface.MakeTexture(renderer);
        }

        public override void OnClick(EventArgs e)
        {
            base.OnClick(e);
            Checked = !Checked;
        }

        public void OnCheckedChange(EventArgs e)
        {
            CheckedChange?.Invoke(this, e);
        }

        public override void OnDispose()
        {
            square.Dispose();
            check.Dispose();
        }

        public override void Draw(SDLRenderer renderer, GameTime gameTime)
        {
            renderer.DrawTexture(square, new Drawing.SDLRectangle(0, 0, Size.Y, Size.Y));
            if (Checked)
                renderer.DrawTexture(check, new Drawing.SDLRectangle(0, 0, Size.Y, Size.Y));
            base.Draw(renderer, gameTime, Size.Y + 5, 0);
        }


    }
}
