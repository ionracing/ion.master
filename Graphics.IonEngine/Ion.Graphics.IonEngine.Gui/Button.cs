﻿using System;
using Ion.Graphics.IonEngine.Drawing;
using Ion.Graphics.IonEngine.Events;

namespace Ion.Graphics.IonEngine.Gui
{
    public class Button : GuiTextElement
    {
        public delegate void ButtonPress(object sender, EventArgs e);
        public event ButtonPress Click;

        SDLColor frameColor;
        public SDLColor FrameColor 
        { 
            get
            { 
                return frameColor;
            }
            set
            { 
                frameColor = value;
                CreateButtonFrame().FillTexture(buttonTexture);
            }
        }

        SDLTexture buttonTexture;

        public Button()
        {
            Location = new SDLPoint(0, 0);
            Size = new SDLPoint(200, 70);
            Text = "button";
            TextColor = SDLColor.White;
            frameColor = SDLColor.White;
        }

        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);
            buttonTexture = CreateButtonFrame().MakeTexture(renderer);
        }

        private DrawSurface CreateButtonFrame()
        {
            return new DrawSurface(Size.X, Size.Y).Initialize(surface => 
            {
                surface.CustomDraw((x,y) => 
                {
                    if (x == 0 || y == 0 || x == surface.Width - 1 || y == surface.Height - 1)
                        surface[x, y] = frameColor;
                });
            });
        }


        public override void Update(GameTime gameTime)
        {
            PullEvents();
            if (IsMouseClicked())
                if(Intersect(current.Location))
            {
                if (Click != null)
                    Click(this, new EventArgs());
            }
            /*
            if (MouseData.LastPos.X > Location.X && MouseData.LastPos.X < Location.X + Size.X && MouseData.LastPos.Y > Location.Y && MouseData.LastPos.Y < Location.Y + Size.Y)
            {
                if (Click != null)
                    Click(this, new EventArgs());
            }
            */
        }

        public override void Draw(SDLRenderer renderer, GameTime gameTime)
        {
            renderer.DrawTexture(buttonTexture, new SDLRectangle(0, 0, Size.X, Size.Y));
            if (!DynamicTextSize)
            {
                renderer.DrawText(Text, Font, new SDLRectangle(5, 5, (int)(Size.Y / 1.5 * Text.Length), Size.Y), TextColor);
            }
            else
            {
                int height = (int)(((double)Size.X / (Text.Length)) * 1.3);
                int paddingTop = (int)((Size.Y) / 2.0 - height / 1.7);

                renderer.DrawText(Text, Font, new SDLRectangle(5, 5 + paddingTop, Size.X - 10, height), TextColor);
            }
        }
    }
}

