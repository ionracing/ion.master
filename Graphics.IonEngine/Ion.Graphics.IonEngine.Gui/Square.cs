﻿using System;
using Ion.Graphics.IonEngine.Drawing;

namespace Ion.Graphics.IonEngine.Gui
{
    public class Square : CarGuiElement
    {

        SDLTexture squareTexture;
        public int FrameWidth { get; set; }

        public Square()
        {
            Size = new SDLPoint(100, 100);
            Location = new SDLPoint(0, 0);
            FrameWidth = 5;
        }

        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);
            DrawSquare(renderer);
        }

        public void DrawSquare(SDLRenderer renderer)
        {
            DrawSurface squareSurf = new DrawSurface(Size.X, Size.Y);
            for (int y = 0; y < Size.Y; y++)
            {
                for (int x = 0; x < Size.X; x++)
                {
                    bool frame = x < FrameWidth || x > (Size.X - 1) - FrameWidth || y < FrameWidth || y > (Size.Y - 1) - FrameWidth;
                    if (frame)
                        squareSurf[x, y] = FrameColor;
                    else
                        squareSurf[x, y] = BackgroundColor;
                }
            }
            squareTexture = squareSurf.MakeTexture(renderer);
            squareTexture.BlendMode = BlendMode.Blend;
        }    
        
        public override void Draw(SDLRenderer renderer, GameTime gameTime)
        {
            renderer.DrawTexture(squareTexture, new SDLRectangle(0, 0, Size.X, Size.Y));
        }
    }
}