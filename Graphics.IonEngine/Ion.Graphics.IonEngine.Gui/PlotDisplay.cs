﻿using Ion.Graphics.IonEngine.Gui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ion.Graphics.IonEngine;
using Ion.Graphics.IonEngine.Drawing;

namespace NicroWare.Lib.IonEngine.Gui
{
    public class PlotDisplay : GuiTextElement
    {
        SDLTexture backTexture;
        SDLTexture dotTexture;
        SDLTexture blankDotTexture;
        SDLTexture lineTexture;

        SDLColor BackColor { get; set; } = new SDLColor(250, 250, 250);
        SDLColor LineColor { get; set; }

        //double xPixelSpan = 0.001;
        double xPixelSpan = 0.002;
        //double yPixelSpan = 0.002;
        double yPixelSpan = 10;
        double xOrigin = 20;
        
        double yOrigin = 20;
        public bool UpToDate = false;
        public bool DrawLines { get; set; } = true;

        List<SDLDPoint> points = new List<SDLDPoint>();
        List<DataPlot> allPlots = new List<DataPlot>();
        List<SDLTexture> preMadePlots = new List<SDLTexture>();

        public PlotDisplay()
        {
            Size = new SDLPoint(540, 300);
            this.FontSize = 20;
            //points.Add(new SDLDPoint(5, 5));
            //points.Add(new SDLDPoint(10, 10));
            //points.Add(new SDLDPoint(15, 15));
            //points.Add(new SDLDPoint(20, 5));
        }

        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);
            DrawSurface dot = new DrawSurface(3, 3);
            dot.CustomDraw((x, y) =>
            {
                if (x == 1 || y == 1)
                    dot[x, y] = SDLColor.Red;
                else
                    dot[x, y] = SDLColor.NoColor;
            });
            dotTexture = dot.MakeTexture(renderer);
            dotTexture.BlendMode = BlendMode.Blend;
            DrawSurface blankDot = new DrawSurface(3, 3);
            blankDot.CustomDraw((x, y) =>
            {
                if (x == 1 || y == 1)
                    blankDot[x, y] = SDLColor.White;
                else
                    blankDot[x, y] = SDLColor.NoColor;
            });
            blankDotTexture = blankDot.MakeTexture(renderer);
            blankDotTexture.BlendMode = BlendMode.Blend;
            // BackColor = new SDLColor(200, 200, 200);
            // GridColor = new SDLColor(150, 150, 150);
            DrawSurface surface = new DrawSurface(Size.X, Size.Y);
            surface.CustomDraw((x, y) =>
            {
                surface[x, y] = BackColor;
            });
            DrawSurface lines = new DrawSurface(Size.X * 2 + 1, Size.Y * 2 + 1);
            lines.CustomDraw((x, y) =>
            {
                if (x == Size.X + 1 || y == Size.Y + 1)
                {
                    lines[x, y] = new SDLColor(150, 150, 150);
                }
                else
                {
                    lines[x, y] = SDLColor.NoColor;
                }
            });
            backTexture = surface.MakeTexture(renderer);
            lineTexture = lines.MakeTexture(renderer);
            lineTexture.BlendMode = BlendMode.Blend;
        }

        public void Clear()
        {
            allPlots.Clear();
        }

        int clickX = 0;
        int clickY = 0;
        double xOriginStart = 0;
        double yOriginStart = 0;
        bool dragging = false;

        public override void Update(GameTime gameTime)
        {
            //xOrigin += 0.05;
            //yOrigin -= 0.05;
            PullEvents();
            if (IsMouseClicked() && Intersect(current.Location))
            {
                Global.SideScroll = false;
                PageScroller.SideScroll = false;
                clickX = current.Location.X;
                clickY = current.Location.Y;
                xOriginStart = xOrigin;
                yOriginStart = yOrigin;
                dragging = true;
            }
            if (dragging)
            {
                xOrigin = xOriginStart + (current.Location.X - clickX);
                yOrigin = yOriginStart - (current.Location.Y - clickY);
            }
            if (IsMouseReleased())
            {
                Global.SideScroll = true;
                PageScroller.SideScroll = true;
                dragging = false;
            }

            if (false)
            {
                foreach (SDLTexture texture in preMadePlots)
                {
                    texture.Dispose();
                }
                preMadePlots.Clear();
                foreach (DataPlot plot in allPlots)
                {
                    DrawSurface surface = new DrawSurface((int)((plot.End - plot.Start) / xPixelSpan), (int)((plot.High - plot.Low) / yPixelSpan));
                    foreach (SDLDPoint point in plot.Points)
                    {
                        //surface[(point.X -plot./ xPixelSpan)]
                    }
                }
                UpToDate = true;
            }

            base.Update(gameTime);
        }

        public override void Draw(SDLRenderer renderer, GameTime gameTime)
        {
            base.Draw(renderer, gameTime);
            renderer.DrawTexture(backTexture, new SDLRectangle(0, 0, backTexture.Width, backTexture.Height));
            int lineDrawX = Size.X - (int)xOrigin;
            int lineDrawY = (int)yOrigin;
            if (lineDrawX > Size.X || lineDrawX < 0)
            {
                lineDrawX = 0;
            }
            if (lineDrawY > Size.Y || lineDrawY < 0)
            {
                lineDrawY = 0;
            }
            renderer.DrawTexture(lineTexture, new SDLRectangle(lineDrawX, lineDrawY, Size.X, Size.Y), new SDLRectangle(0, 0, Size.X, Size.Y), 0, new SDLPoint(0, 0), RendererFlip.None);
            foreach (SDLDPoint point in points)
            {
                double xCalc = point.X * xPixelSpan + xOrigin;
                double yCalc = point.Y * yPixelSpan + yOrigin;
                if (xCalc >= 0 && xCalc < Size.X && yCalc >= 0 && yCalc < Size.Y)
                    renderer.DrawTexture(dotTexture, new SDLRectangle((int)(xCalc) - 1, Size.Y - (int)(yCalc) - 1, 3, 3));
            }
            //TODO: Fix plotting algorithm to draw to image, then display image for massive increase in performence with many dots
            int counter = 0;
            foreach (DataPlot plot in allPlots)
            {
                if (plot.Points.Count == 0 || !plot.Visible)
                    continue;
                int min = 0;
                int max = allPlots.Count;
                int startIndex = plot.Points.Count / 2;
                int steps = 0;
                while (true)
                {
                    steps++;
                    double xCalc = plot.Points[startIndex].X * xPixelSpan + xOrigin;
                    //double yCalc = plot.Points[startIndex].Y * yPixelSpan + yOrigin;
                    if (xCalc < 0)
                    {
                        min = startIndex;
                    }
                    else if (xCalc > 0)
                    {
                        max = startIndex;
                    }
                    else
                    {
                        min = startIndex;
                        break;
                    }
                    startIndex = min + (max - min) / 2;
                    if (max - min < 3)
                    {
                        break;
                    }
                    //if (xCalc >= 0 && xCalc < Size.X && yCalc >= 0 && yCalc < Size.Y)

                }

                SDLPoint lastPoint = new SDLPoint((int)xOrigin, Size.Y - (int)yOrigin);
                for (int i = min; i < plot.Points.Count; i++)
                {
                    double xCalc = plot.Points[i].X * xPixelSpan + xOrigin;
                    double yCalc = plot.Points[i].Y * yPixelSpan + yOrigin;
                    SDLPoint basePoint = new SDLPoint((int)xCalc, (int)(Size.Y - yCalc));

                    if (xCalc >= 0 && xCalc < Size.X && yCalc >= 0 && yCalc < Size.Y)
                    {
                        if (DrawLines && lastPoint.X >= 0 && lastPoint.Y < Size.X && lastPoint.Y >= 0 && lastPoint.Y < Size.Y)
                            renderer.DrawLine(lastPoint, basePoint, plot.Color);
                        renderer.DrawTexture(blankDotTexture, new SDLRectangle(0, 0, 3, 3), new SDLRectangle((int)(xCalc) - 1, Size.Y - (int)(yCalc) - 1, 3, 3), 0, SDLPoint.Zero, RendererFlip.None, plot.Color);
                    }
                    lastPoint = basePoint;
                    if (xCalc > Size.X)
                        break;
                    
                }/*
                foreach (SDLDPoint point in plot.Points)
                {
                    double xCalc = point.X * xPixelSpan + xOrigin;
                    double yCalc = point.Y * yPixelSpan + yOrigin;

                    if (xCalc >= 0 && xCalc < Size.X && yCalc >= 0 && yCalc < Size.Y)
                        renderer.DrawTexture(blankDotTexture, new SDLRectangle(0, 0, 3, 3), new SDLRectangle((int)(xCalc) - 1, Size.Y - (int)(yCalc) - 1, 3, 3), 0, SDLPoint.Zero, RendererFlip.None, plot.Color);
                }*/
                if (plot.Title != null)
                {
                    renderer.DrawText(plot.Title, Font, new SDLRectangle(10, 10 + counter * (FontSize + 5), CalculateWidth(plot.Title), FontSize), plot.Color);
                    counter++;
                }
            }

            for (int x = 0; x < Size.X - 20; x += 50)
            {
                string text = (((x - xOrigin) / xPixelSpan)/ 1000).ToString();
                int width = CalculateWidth(text) / 2;
                renderer.DrawText(text, Font, new SDLRectangle(x, Size.Y - 20, width, 20), SDLColor.Black);
            }
            for (int y = 0; y < Size.Y - 20; y += 50)
            {
                string text = ((((Size.Y - y) - yOrigin) / yPixelSpan)).ToString();
                int width = CalculateWidth(text) / 2;
                renderer.DrawText(text, Font, new SDLRectangle(5, y, width, 20), SDLColor.Black);
            }
        }

        public void Add(SDLDPoint point)
        {
            points.Add(point);
        }

        public void Add(DataPlot plot)
        {
            this.allPlots.Add(plot);
        }
    }

    public class DataPlot
    {

        public SDLColor Color { get; set; }
        public string Title { get; set; }
        public List<SDLDPoint> Points { get; } = new List<SDLDPoint>();
        public double Start { get; set; } = 0;
        public double End { get; set; } = 0;
        public double High { get; set; } = 0;
        public double Low { get; set; } = 0;
        public bool Visible { get; set; }

        public DataPlot(SDLColor color)
        {
            this.Color = color;
        }

        public DataPlot(SDLColor color, string title) : this(color)
        {
            this.Title = title;
        }

        public void Add(SDLDPoint point)
        {
            Start = Math.Max(point.X, Start);
            End = Math.Min(point.X, End);
            High = Math.Max(point.Y, High);
            Low = Math.Min(point.Y, Low);
            this.Points.Add(point);
        }
    }
}
