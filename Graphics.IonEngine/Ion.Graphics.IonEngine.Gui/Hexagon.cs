﻿using Ion.Graphics.IonEngine.Gui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ion.Graphics.IonEngine;
using Ion.Graphics.IonEngine.Drawing;

namespace NicroWare.Lib.IonEngine.Gui
{
    public class Hexagon : GuiElement
    {
        SDLTexture hexagon;

        public Hexagon()
        {
            
        }

        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);

            DrawSurface test = new DrawSurface(Size.X, Size.Y);
            int radius = test.Width / 2;
            int height = (int)(radius * Math.Cos(Math.PI / 6));
            int start = (int)(radius * Math.Sin(Math.PI / 6));
            double rel = (radius - start) / (double)height;

            test.CustomDraw((rX, rY) =>
            {
                if (rY > radius - height && rY < radius + height)
                {
                    double tester = Math.Abs(radius - rY);
                    if (Math.Abs(radius - rX) < (Math.Abs(height - tester) * rel + start))
                        test[rX, rY] = SDLColor.IonBlue;
                    else
                        test[rX, rY] = new SDLColor(0, 0, 0, 0);

                }
            });

            hexagon = test.MakeTexture(renderer);
            hexagon.BlendMode = BlendMode.Blend;
        }

        public override void Draw(SDLRenderer renderer, GameTime gameTime)
        {
            renderer.DrawTexture(hexagon, new SDLRectangle(new SDLPoint(0,0), Size));
        }
    }
}
