﻿using System;
using Ion.Graphics.IonEngine.Drawing;

namespace Ion.Graphics.IonEngine.Gui
{
    public class NewInfoMeter : CarGuiElement
    {
        public RendererFlip Flip { get; set; }

        SDLTexture upperContainerTexture;
        SDLTexture lowerContainerTexture;
        SDLTexture valueTexture;
        private int maxRadius;
        private int screenHeight, screenWidth;
        private int width, height;
        private int upperValuebarHeight, lowerValuebarHeight;
        private int valuebarMargin;
        private double upperDisplayValue, lowerDisplayValue;
        SDLColor upperDynamicColor;
        SDLColor lowerDynamicColor;

        public NewInfoMeter()
        {
            Value = 0;
            MaxValue = 1;
            Flip = RendererFlip.None;
            screenHeight = 480;
            screenWidth = 800;
            width = (int)(screenWidth * 0.3);
            height = (int)(screenHeight * 0.5);
            maxRadius = screenWidth / 2;
            upperValuebarHeight = (int)(height * 1);
            lowerValuebarHeight = (int)(height * 0.8);
            valuebarMargin = 5;
            ValueColor = SDLColor.White;
            upperDynamicColor = new SDLColor(150, 0, 0); 
            lowerDynamicColor = new SDLColor(0, 0, 150);
        }

        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);
            DrawInfometers(renderer);
        }

        public void DrawInfometers(SDLRenderer renderer)
        {
            int centerX = screenWidth / 2, centerY = screenHeight / 2;

            DrawSurface upperSurface = new DrawSurface(width, height);
            DrawSurface lowerSurface = new DrawSurface(width, height);
            DrawSurface valueSurf = new DrawSurface(width, height);            

            for (int y = 0; y < screenHeight; y++)
            {
                int relY = y - centerY;
                for (int x = 0; x < screenWidth; x++)
                {
                    int relX = x - centerX;
                    int curRadius = (int)Math.Sqrt(relX * relX + relY * relY);
                    int outerRadius = (int)(maxRadius * 0.9);
                    int innerRadius = (int)(maxRadius * 0.77);

                    bool outerBorder = curRadius == outerRadius;
                    bool outerBorder2 = curRadius == outerRadius;
                    bool innerBorder = curRadius == innerRadius;
                    bool heightLimit1 = relY <= -valuebarMargin && relY >= -(upperValuebarHeight);
                    bool heightLimit2 = relY <= -valuebarMargin && relY >= -(lowerValuebarHeight);
                    bool widthLimit = relX < 0 && x > 0;
                    bool valueBarWidth = curRadius >= innerRadius && curRadius <= outerRadius;
                    bool lowerBorder = relY == -valuebarMargin;
                    bool upperBorder1 = relY == -(upperValuebarHeight);
                    bool upperBorder2 = relY == -(lowerValuebarHeight);

                    if (x <= width && y <= height)
                    {
                        if ((outerBorder || innerBorder || lowerBorder || upperBorder1) && heightLimit1 && widthLimit && valueBarWidth)
                            upperSurface[x, y] = FrameColor;

                        else if (heightLimit1 && valueBarWidth)
                            upperSurface[x, y] = new SDLColor(0, 0, 0, 0);

                        else
                            upperSurface[x, y] = BackgroundColor;

                        if ((outerBorder || innerBorder || lowerBorder || upperBorder2) && heightLimit2 && widthLimit && valueBarWidth)
                            lowerSurface[x, y] = FrameColor;


                        else if (heightLimit2 && valueBarWidth)
                            lowerSurface[x, y] = new SDLColor(0, 0, 0, 0);

                        else
                            lowerSurface[x, y] = BackgroundColor;

                        valueSurf[x, y] = ValueColor;
                    }

                }
            }
            upperContainerTexture = upperSurface.MakeTexture(renderer);
            upperContainerTexture.BlendMode = BlendMode.Blend;

            lowerContainerTexture = lowerSurface.MakeTexture(renderer);
            lowerContainerTexture.BlendMode = BlendMode.Blend;

            valueTexture = valueSurf.MakeTexture(renderer);
            valueTexture.BlendMode = BlendMode.Blend;
        }

        public override void Update(GameTime gameTime)
        {
            if (Persent < 0.5)
            {
                upperDynamicColor.G = (byte)(Persent * 150 * 2);

                lowerDynamicColor.G = (byte)(Persent * 150 * 2);
                lowerDynamicColor.R = (byte)(Persent * 150 * 2);
            }
            else
            {
                upperDynamicColor.G = 150;
                upperDynamicColor.R = (byte)((1 - Persent) * 150 * 2);

                lowerDynamicColor.R = 150;
                lowerDynamicColor.G = (byte)((1 - Persent) * 150 * 2);
                lowerDynamicColor.B = (byte)((1 - Persent) * 150 * 2);
            }

            upperDisplayValue = Persent * (upperValuebarHeight - valuebarMargin);
            lowerDisplayValue = Persent * (lowerValuebarHeight - valuebarMargin);
            base.Update(gameTime);
        }

        public override void Draw(SDLRenderer renderer, GameTime gameTime)
        {
            if (Flip.HasFlag(RendererFlip.Vertical))
            {
                renderer.DrawTexture(valueTexture
                    , new SDLRectangle(0
                        , valuebarMargin
                        , lowerContainerTexture.Width
                        , (int)lowerDisplayValue)
                    , new SDLRectangle(0
                        , valuebarMargin
                        , lowerContainerTexture.Width
                        , (int)lowerDisplayValue)
                    , 0
                    , new SDLPoint(0,0)
                    , RendererFlip.None
                    , lowerDynamicColor);

                renderer.DrawTexture(lowerContainerTexture
                    , new SDLRectangle(0, 0, width, height)
                    , new SDLRectangle(0, 0, width, height)
                    , 0
                    , new SDLPoint(0, 0)
                    , Flip);
            }
            else
            {
                renderer.DrawTexture(valueTexture
                    , new SDLRectangle(0
                        , ((upperValuebarHeight - (int)upperDisplayValue) + (height - upperValuebarHeight) - valuebarMargin)
                        , width
                        , (int)upperDisplayValue)
                    , new SDLRectangle(0
                        , ((upperValuebarHeight - (int)upperDisplayValue) + (height - upperValuebarHeight) - valuebarMargin)
                        , width
                        , (int)upperDisplayValue)
                    , 0
                    , new SDLPoint(0, 0)
                    , RendererFlip.None
                    , upperDynamicColor);

                renderer.DrawTexture(upperContainerTexture
                    , new SDLRectangle(0, 0, width, height)
                    , new SDLRectangle(0, 0, width, height)
                    , 0
                    , new SDLPoint(0, 0)
                    , Flip);
            }
        }
    }
}
