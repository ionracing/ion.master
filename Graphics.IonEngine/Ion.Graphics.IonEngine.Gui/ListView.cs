﻿using System;
using Ion.Graphics.IonEngine.Drawing;
using System.Collections.Generic;
using Ion.Graphics.IonEngine.Events;

namespace Ion.Graphics.IonEngine.Gui
{
    public class ListView : GuiTextElement
    {
        SDLTexture frame;
        SDLColor frameColor;
        public List<object> Items { get; private set; }
        public int Scroll { get; set; }
        int lastScroll = 0;
        int lastCtrlPos = 0;
        bool CapturedMouse;


        public ListView()
        {
            frameColor = new SDLColor(255, 255, 255);
            this.Items = new List<object>();
        }

        public override void Initialize(SDLRenderer renderer)
        {
            frame = new DrawSurface(Size.X, Size.Y).Initialize(surf => surf.CustomDraw((x,y) => 
            {
                if (x == 0 || y == 0 || y == Size.Y - 1 || x == Size.X - 1)
                {
                    surf[x,y] = frameColor;
                }
            })).MakeTexture(renderer);
            base.Initialize(renderer);
        }

        public override void Update(GameTime gameTime)
        {
            PullEvents();

            //TODO: Recode scrolling code
            if (current.IsKeyDown(MouseButtons.Left)
                && new SDLRectangle(Location, Size).Intersect(current.Location))
            {
                CapturedMouse = true;
                Global.SideScroll = false;
                lastScroll = Scroll;
                if (last.IsKeyUp(MouseButtons.Left))
                {
                    lastCtrlPos = current.Location.Y - Location.Y;
                }
            }
            else if (MouseData.Down == false || current.IsKeyUp(MouseButtons.Left))
            {
                CapturedMouse = false;
                Global.SideScroll = true;
            }
            if (CapturedMouse)
            {
                int ctrlPos = current.Location.Y - Location.Y;
                Scroll = (int)((lastCtrlPos - ctrlPos)) + lastScroll;
                //Console.WriteLine($"{nameof(Scroll)}: {Scroll}, {nameof(ctrlPos)}: {ctrlPos}, {nameof(lastCtrlPos)}: {lastCtrlPos}, {nameof(current.Location.Y)}: {current.Location.Y},");
                if (Scroll < 0)
                    Scroll = 0;
                if (Items.Count * FontSize > Size.Y && Scroll > FontSize * (Items.Count + 1)  - Size.Y)
                    Scroll = FontSize * (Items.Count + 1) - Size.Y;
                lastCtrlPos = ctrlPos;
            }

        }

        public override void Draw(SDLRenderer renderer, GameTime gameTime)
        {
            renderer.DrawTexture(frame, new SDLRectangle(0, 0, Size.X, Size.Y));
            for (int i = (int)(Scroll / FontSize); i < Items.Count && (i) < ((Size.Y + Scroll) / FontSize); i++)
            {
                if (i >= 0 && i < Items.Count)
                {
                    string current = Items[i].ToString();
                    int maxChars = 80;
                    if (current.Length > maxChars)
                    {
                        renderer.DrawText(current.Substring(0, maxChars), Font, new SDLRectangle(0, i * FontSize - Scroll, CalculateWidth(current.Substring(0, maxChars)), FontSize), TextColor);
                    }
                    else
                    {
                        renderer.DrawText(current, Font, new SDLRectangle(0, i * FontSize - Scroll, CalculateWidth(current), FontSize), TextColor);
                    }
                }
            }
        }
    }
}