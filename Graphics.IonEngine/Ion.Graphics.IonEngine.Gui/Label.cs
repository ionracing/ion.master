﻿using System;
using Ion.Graphics.IonEngine.Drawing;

namespace Ion.Graphics.IonEngine.Gui
{
    public class Label : GuiTextElement
    {

        /*SDLSurface preText;
        SDLTexture text;*/

        public override int FontSize
        {
            get
            {
                return base.FontSize;
            }
            set
            {
                base.FontSize = value;
                base.Size = new SDLPoint(base.Size.X, value);
            }
        }
        public override SDLPoint Size
        {
            get
            {
                return base.Size;
            }
            set
            {
                base.Size = value;
                base.FontSize = value.Y;
            }
        }
        public event EventHandler Click;

        public Label()
        {
            Location = new SDLPoint(0, 0);
            Size = new SDLPoint(100, 40);
            Text = "Label";
            DynamicTextSize = true;

        }

        public override void Update(GameTime gameTime)
        {
            base.PullEvents();
            base.Update(gameTime);
            if (base.IsMouseClicked()
                && Intersect(current.Location))
            {
                OnClick(new EventArgs());
            }
        }

        public virtual void OnClick(EventArgs e)
        {
            Click?.Invoke(this, e);
        }

        public override void Draw(SDLRenderer renderer, GameTime gameTime)
        {
            this.Draw(renderer, gameTime, 0, 0);
        }

        public void Draw(SDLRenderer renderer, GameTime gameTime, int x, int y)
        {
            //text = SDLTexture.CreateFrom(renderer, preText);
            //renderer.Draw(text, new SDLRectangle(Location.X, Location.Y, /*(int)(Size.Y / 1.5 * Text.Length)*/ Size.X, Size.Y));
            if (!DynamicTextSize)
            {
                renderer.DrawText(Text, Font, new SDLRectangle(x, y, Size.X - x, Size.Y - y), TextColor);
            }
            else
            {
                int startX = CalculateAlign(Text);
                int width = (int)(Size.Y * 0.6 * Text.Length);
                renderer.DrawText(Text, Font, new SDLRectangle(startX + x, y, width, FontSize - y), TextColor);
            }
        }
    }
}

