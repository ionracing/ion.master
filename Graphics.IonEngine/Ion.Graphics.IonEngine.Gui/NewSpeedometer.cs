﻿using System;
using Ion.Graphics.IonEngine.Drawing;

namespace Ion.Graphics.IonEngine.Gui
{
    public class NewSpeedometer : CarGuiElement
    {
        public double Rotater { get; set; }
        public double RealRotater { get; set; }

        SDLTexture smallCircle; 
        SDLTexture movingSlice;
        SDLTexture hideSlice;
        SDLTexture noiseDisguise; // thick, black ring behind outer border to hide rouge pixels
        SDLTexture border;
        SDLTexture borderEndings;
        private int width, height;
        private int maxRadius;

        public NewSpeedometer()
        {
            MaxValue = 200;
            width = 490; height = 490;
            maxRadius = width / 2;
        }

        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);
            DrawComponents(renderer);
        }

        public void DrawComponents(SDLRenderer renderer)
        {
            int centerX = width / 2, centerY = height / 2;

            DrawSurface smallCircleSurf = new DrawSurface(width, height);
            DrawSurface noiseDisguiseSurf = new DrawSurface(width, height);
            DrawSurface outerEdgeSurf = new DrawSurface(width, height);
            DrawSurface innerEdgeSurf = new DrawSurface(width, height);
            DrawSurface hideSliceSurf = new DrawSurface(width, height);
            DrawSurface movingSliceSurf = new DrawSurface(width, height);
            DrawSurface borderEndingsSurf = new DrawSurface(width, height);
            DrawSurface borderSurf = new DrawSurface(width, height);

            if (Global.TanPos == null)
                Global.TanPos = MathExt.MathHelper.GenerateDegreeTanArray(width, height);
            for (int y = 0; y < height; y++)
            {
                int relY = y - centerY;
                for (int x = 0; x < width; x++)
                {
                    int relX = x - centerX;
                    int curRadius = (int)Math.Sqrt(relX * relX + relY * relY);

                    bool noiseDisguise = curRadius <= (int)(maxRadius * 0.96) && curRadius > (int)(maxRadius * 0.94);
                    bool smallCircle = curRadius <= (int)(maxRadius * 0.83);
                    bool outerEdge = curRadius == (int)(maxRadius * 0.94);
                    bool innerEdge = curRadius == (int)(maxRadius * 0.83);
                    bool movingSliceSize = curRadius <= (int)(maxRadius * 0.94);
                    bool hideSliceSize = curRadius <= (int)(maxRadius * 0.95);
                    bool degreeCheck = Global.TanPos[x, y] <= -90 || Global.TanPos[x, y] == 180;
                    bool diagonalLine = Math.Abs(relX) == Math.Abs(relY) && (curRadius <= (int)(maxRadius * 0.94) && curRadius >= (int)(maxRadius * 0.83));
                    bool lowerLeftQuadrant = Global.TanPos[x, y] > 90 && Global.TanPos[x, y] < 180;
                    bool lowerRightQuadrant = Global.TanPos[x, y] > 0 && Global.TanPos[x, y] < 90;

                    if (smallCircle)
                        smallCircleSurf[x, y] = BackgroundColor;
                    if (noiseDisguise)
                        noiseDisguiseSurf[x, y] = BackgroundColor;
                    if (outerEdge)
                        borderSurf[x, y] = FrameColor;
                    if (innerEdge)
                        borderSurf[x, y] = FrameColor;

                    if (lowerLeftQuadrant && diagonalLine)
                        borderEndingsSurf[x + 2, y + 1] = FrameColor; // alignment tweaked for fitment
                    if (lowerRightQuadrant && diagonalLine)
                        borderEndingsSurf[x + 1, y - 1] = FrameColor; // alignment tweaked for fitment
                    if (hideSliceSize && degreeCheck)
                        hideSliceSurf[x, y] = BackgroundColor;
                    if (movingSliceSize && degreeCheck)
                        movingSliceSurf[x, y] = ValueColor;

                }
            }

            smallCircle = smallCircleSurf.MakeTexture(renderer);
            smallCircle.BlendMode = BlendMode.Blend;

            noiseDisguise = noiseDisguiseSurf.MakeTexture(renderer);
            noiseDisguise.BlendMode = BlendMode.Blend;

            hideSlice = hideSliceSurf.MakeTexture(renderer);
            hideSlice.BlendMode = BlendMode.Blend;

            movingSlice = movingSliceSurf.MakeTexture(renderer);
            movingSlice.BlendMode = BlendMode.Blend;

            border = borderSurf.MakeTexture(renderer);
            border.BlendMode = BlendMode.Blend;

            borderEndings = borderEndingsSurf.MakeTexture(renderer);
            borderEndings.BlendMode = BlendMode.Blend;

        }
       
        public override void Update(GameTime gameTime)
        {
            Rotater = (Persent * (3.0 / 2.0) * Math.PI);
            RealRotater = Rotater - (3.0 / 4.0) * Math.PI;
        }

        public override void Draw(SDLRenderer renderer, GameTime gameTime)
        {
            renderer.DrawTexture(movingSlice
                , new SDLRectangle(0, 0, width, height)
                , new SDLRectangle(0, 0, width, height)
                , RealRotater
                , new SDLPoint(width/2, height/2) 
                , RendererFlip.None);

            if (RealRotater >= -(Math.PI / 4))
            {
                renderer.DrawTexture(movingSlice
                    , new SDLRectangle(0, 0, width, height)
                    , new SDLRectangle(0, 0, width, height)
                    , -(Math.PI / 4)
                    , new SDLPoint(width / 2, height / 2)
                    , RendererFlip.None);
            }
            if (RealRotater >= (Math.PI / 4))
            {
                renderer.DrawTexture(movingSlice
                    , new SDLRectangle(0, 0, width, height)
                    , new SDLRectangle(0, 0, width, height)
                    , (Math.PI / 4)
                    , new SDLPoint(width / 2, height / 2)
                    , RendererFlip.None);
            }
            renderer.DrawTexture(smallCircle, new SDLRectangle(0, 0, width, height));
            renderer.DrawTexture(noiseDisguise, new SDLRectangle(0, 0, width, height));
            renderer.DrawTexture(border, new SDLRectangle(0, 0, width, height));
            renderer.DrawTexture(hideSlice
                , new SDLRectangle(0, 0, width, height) 
                , new SDLRectangle(0, 0, width, height)
                , -(Math.PI * 3 / 4)
                , new SDLPoint(width/2, height/2) 
                , RendererFlip.None);
            renderer.DrawTexture(borderEndings, new SDLRectangle(0, 0, width, height));
        }
    }
}
