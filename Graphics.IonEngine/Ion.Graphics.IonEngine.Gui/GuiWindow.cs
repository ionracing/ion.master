﻿using Ion.Graphics.IonEngine.Drawing;
using Ion.Graphics.IonEngine.Font;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Ion.Graphics.IonEngine.Gui
{
    public class GuiWindow : SDLRenderWindow
    {
        public PageScroller scroller;

        Dictionary<string, Linker> allGuiTypes = new Dictionary<string, Linker>();
        Dictionary<Type, Func<string, object>> typeLink = new Dictionary<Type, Func<string, object>>();

        public GuiWindow()
        {
            FindElements();

            typeLink.Add(typeof(string), (x => x.Replace("\\n", "\n")));
            typeLink.Add(typeof(int), (x => int.Parse(x)));
            typeLink.Add(typeof(bool), (x => bool.Parse(x)));
            typeLink.Add(typeof(SDLPoint), (x => { string[] point = x.Split(';'); return new SDLPoint(int.Parse(point[0]), int.Parse(point[1])); }));
            typeLink.Add(typeof(SDLColor), (x => {
                string tempValue = "";
                if (x[0] == '#')
                    tempValue = x.Remove(0, 1);
                else
                    throw new FormatException("Color not in right format");

                SDLColor color = new SDLColor(
                    byte.Parse(tempValue.Substring(0, 2), System.Globalization.NumberStyles.HexNumber)
                    , byte.Parse(tempValue.Substring(2, 2), System.Globalization.NumberStyles.HexNumber)
                    , byte.Parse(tempValue.Substring(4, 2), System.Globalization.NumberStyles.HexNumber));
                return color;
            }));
            typeLink.Add(typeof(SDLFont), (x => {
                foreach (FieldInfo fInfo in GetType().GetRuntimeFields())
                {
                    if (fInfo.Name == x)
                    {
                        return fInfo.GetValue(this);
                        //pi.SetValue(obj, );
                        //break;
                    }
                }
                return null;
            }));
        }

        private void FindElements()
        {
            Assembly[] allAssems = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly asm in allAssems)
            {
                Type[] allTypes = asm.GetTypes();
                foreach (Type t in allTypes)
                {
                    if (typeof(GuiElement).IsAssignableFrom(t))
                    {
                        Linker temp = new Linker();
                        temp.LinkType = t;
                        allGuiTypes.Add(t.Name, temp);
                        foreach (PropertyInfo pi in t.GetProperties())
                        {
                            temp.AllProperties.Add(pi.Name, pi);
                        }
                    }
                }
            }
        }

        public void InitializeComponents(SDLRenderer renderer)
        {
            //string xmlPath = @"C:\Users\Nicolas\Desktop\CarGui.xml";
            string xmlName = this.GetType().Name + ".xml";
            XmlDocument document = new XmlDocument();
            document.Load(xmlName);
            ParseRoot(renderer, document.ChildNodes);
        }

        public void ParseRoot(SDLRenderer renderer, XmlNodeList list)
        {
            XmlNode window = null;// = list[0];
            foreach (XmlNode node in list)
            {
                if (node.Name == "Window")
                {
                    window = node;
                    break;
                }
            }
            if (window == null)
            {
                throw new Exception("Window node not found");
            }
            string[] winSize = (window.Attributes.GetNamedItem("Size")?.Value ?? "800;480").Split(';');
            string[] winLoc = (window.Attributes.GetNamedItem("Location")?.Value ?? "10;10").Split(';');
            string winTitle = window.Attributes.GetNamedItem("Title")?.Value ?? "Display1";
            this.Size = new SDLPoint(int.Parse(winSize[0]), int.Parse(winSize[1]));
            this.Location = new SDLPoint(int.Parse(winLoc[0]), int.Parse(winLoc[1]));
            this.Title = winTitle;
            foreach (XmlNode node in window.ChildNodes)
            {
                if (node.Name == "PageScroller")
                {
                    CreatePageScroller(renderer, node);
                }
            }
        }

        public void CreatePageScroller(SDLRenderer renderer, XmlNode node)
        {
            scroller = new PageScroller() { Location = new SDLPoint(0, 0), Size = Size };
            foreach (XmlNode chld in node.ChildNodes)
            {
                if (chld.Name == "Page")
                {
                    Panel page = CreatePanel(renderer, chld);
                    page.Size = Size;
                    page.Initialize(renderer);
                    scroller.Elements.Add(page);
                }
            }
        }

        private void ClassLink(string name, object obj)
        {
            foreach (FieldInfo fi in GetType().GetRuntimeFields())
            {
                if (fi.Name == name)
                {
                    fi.SetValue(this, obj);
                    break;
                }
            }
        }

        private void PropertyLink(string name, string value, PropertyInfo pi, Linker curLink, object obj)
        {
            if (pi.PropertyType.IsEnum)
                pi.SetValue(obj, Enum.Parse(pi.PropertyType, value));
            else if (typeLink.ContainsKey(pi.PropertyType))
                pi.SetValue(obj, typeLink[pi.PropertyType](value));
        }

        private void EventLink(string name, string value, Linker curLink, object obj)
        {
            EventInfo ei = curLink.LinkType.GetRuntimeEvent(name);
            MethodInfo[] all = GetType().GetRuntimeMethods().ToArray();
            MethodInfo mi = null;
            foreach (MethodInfo minfo in all)
            {
                if (minfo.Name == value)
                {
                    mi = minfo;
                    break;
                }
            }
            if (ei != null && mi != null)
            {
                ei.AddEventHandler(obj, mi.CreateDelegate(ei.EventHandlerType, this));
            }
        }

        private Panel CreatePanel(SDLRenderer renderer, XmlNode child)
        {
            Panel curPanel = new Panel();
            FillGuiContainer(curPanel, child);
            return curPanel;
        }

        private void FillGuiContainer(GuiContainerElement element, XmlNode child)
        {
            foreach (XmlNode curChild in child.ChildNodes)
            {
                if (allGuiTypes.ContainsKey(curChild.Name))
                {
                    Linker curLink = allGuiTypes[curChild.Name];
                    object temp = Activator.CreateInstance(curLink.LinkType);
                    foreach (XmlAttribute attr in curChild.Attributes)
                    {
                        if (attr.Name == "Name")
                        {
                            ClassLink(attr.Value, temp);
                        }
                        else
                        {
                            if (curLink.AllProperties.ContainsKey(attr.Name))
                                PropertyLink(attr.Name, attr.Value, curLink.AllProperties[attr.Name], curLink, temp);
                            else
                                EventLink(attr.Name, attr.Value, curLink, temp);
                        }
                    }
                    if (temp is GuiContainerElement)
                    {
                        FillGuiContainer(temp as GuiContainerElement, curChild);
                    }
                    element.Elements.Add(temp as GuiElement);
                }
            }
        }

    }

    public class Linker
    {
        public Type LinkType { get; set; }
        public Dictionary<string, PropertyInfo> AllProperties { get; set; } = new Dictionary<string, PropertyInfo>();
    }
}
