﻿using System;
using Ion.Graphics.IonEngine.Drawing;

namespace Ion.Graphics.IonEngine.Gui
{
    public class CarGuiElement : GuiValueElement
    {
        public SDLColor FrameColor { get; set; }
        public SDLColor ValueColor { get; set; }
        public SDLColor BackgroundColor { get; set; }
        public CarGuiElement()
        {
            FrameColor = SDLColor.LightGrey;
            ValueColor = SDLColor.IonBlue;
            BackgroundColor = SDLColor.Black;
        }
    }
}

