﻿using System;
using Ion.Graphics.IonEngine.Drawing;
using System.IO;

namespace Ion.Graphics.IonEngine.Gui
{
    /// <summary>
    /// An image controller that can hold an image
    /// </summary>
    public class Image : GuiElement
    {
        SDLRenderer renderer;

        /// <summary>
        /// Get or Set the texture that should be drawen in the draw call
        /// </summary>
        public SDLTexture ImageTexture { get; set; }
        /// <summary>
        /// Get or Set the tint of the Image
        /// </summary>
        public SDLColor ImageColor { get; set; }
        string source;
        /// <summary>
        /// Get or Set the file path to the images to be used. This will load the file when you set the source. Current support .bmp and .png
        /// </summary>
        public string Source
        {
            get
            {
                return source;
            }
            set
            {
                source = value;
                FileInfo fi = new FileInfo(source);
                if (fi.Exists)
                {
                    if (renderer != null) //Probobly because this is called before the Init is called
                    {
                        switch (fi.Extension.ToLower())
                        {
                            case ".bmp":
                                ImageTexture = SDLTexture.CreateFrom(renderer, SDLSurface.LoadBitmap(source));
                                break;
                            case ".png":
                                ImageTexture = SDLTexture.CreateFrom(renderer, SDLSurface.LoadPNG(source));
                                break;
                            default:
                                ImageTexture = SDLTexture.CreateFrom(renderer, SDLSurface.LoadBitmap(source));
                                break;
                        }
                    }
                }
                else
                    throw new FileNotFoundException("The spessified file was not found on the file system. " + source);
            }
        }

        /// <summary>
        /// Creates a blank image with zero size
        /// </summary>
        public Image()
        {
            Size = new SDLPoint(0, 0);
            ImageColor = SDLColor.White;
        }

        /// <summary>
        /// Initialize the controller and loads the image if the source is set
        /// </summary>
        /// <param name="renderer">The SDLRenderer to use</param>
        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);
            this.renderer = renderer;
            if (source != null)
                Source = Source; // HACK: Fix this hack
        }

        /// <summary>
        /// Draws the controller
        /// </summary>
        /// <param name="renderer">SDLRenderer</param>
        /// <param name="gameTime">GameTime</param>
        public override void Draw(SDLRenderer renderer, GameTime gameTime)
        {
            //TODO: Do some work on the image class
            if (Size.X == 0 && Size.Y == 0)
            {
                renderer.DrawTexture(ImageTexture, new SDLRectangle(0, 0, ImageTexture.Width, ImageTexture.Height), new SDLRectangle(0,0,ImageTexture.Width, ImageTexture.Height), 0, new SDLPoint(0,0), RendererFlip.None, ImageColor);
            }
            else
                renderer.DrawTexture(ImageTexture, new SDLRectangle(0, 0, ImageTexture.Width, ImageTexture.Height), new SDLRectangle(0,0,Size.X, Size.Y), 0, new SDLPoint(0,0), RendererFlip.None, ImageColor);
        }

        /// <summary>
        /// Loads a file with the spessiged renderer at the spessified path, supported formats: .png, .bmp
        /// </summary>
        /// <param name="renderer">SDLRenderer</param>
        /// <param name="path">The path to the image</param>
        /// <returns>A new, not initialized image controller</returns>
        public static Image LoadFromFile(SDLRenderer renderer, string path)
        {
            return LoadFromFile(renderer,new FileInfo( path));
        }

        /// <summary>
        /// Loads a file with the spessiged renderer at the spessified FileInfo, supported formats: .png, .bmp
        /// </summary>
        /// <param name="renderer">SDLRenderer</param>
        /// <param name="info">The FileInfo to the image</param>
        /// <returns>A new, not initialized image controller</returns>
        public static Image LoadFromFile(SDLRenderer renderer, FileInfo info)
        {
            if (!info.Exists)
                throw new FileNotFoundException();
            else if (info.Extension.ToUpper() == ".BMP")
                return new Image() { ImageTexture = SDLTexture.CreateFrom(renderer, SDLSurface.LoadBitmap(info.FullName)) };
            else if (info.Extension.ToUpper() == ".PNG")
                return new Image() { ImageTexture = SDLTexture.CreateFrom(renderer, SDLSurface.LoadPNG(info.FullName)) };
            else
                throw new NotSupportedException("The spessified file format is not supported");

        }
    }
}

