﻿using System;
using Ion.Graphics.IonEngine.Drawing;

namespace Ion.Graphics.IonEngine.Gui
{
    public class GraphDisplay : GuiElement
    {
        SDLTexture graphTexture;
        SDLTexture overlay;
        public ListEvent<int> Values { get; private set; }
        public int MaxValue { get; set; }
        public int DotSpan { get; set; }
        public SDLColor DotColor { get; set; }
        public SDLColor BackColor { get; set; }
        public SDLColor GridColor { get; set; }
        public SDLColor LineColor { get; set; }
        public bool DrawLine { get; set; }
        public bool DrawGrid { get; set; }
        public bool DisplayText { get; set; }
        public bool RealTime { get; set; }
        public bool SuspendDraw { get; set; }
        //TextBlock textBlock;

        public GraphDisplay()
        {
            Size = new SDLPoint(540, 300);
            MaxValue = 400;
            DotSpan = 5;
            DotColor = SDLColor.Red;
            BackColor = new SDLColor(200, 200, 200);
            GridColor = new SDLColor(150, 150, 150);
            LineColor = SDLColor.Black;
            DrawLine = true;
            DrawGrid = true;
            RealTime = true;
            SuspendDraw = false;

            
            Values = new ListEvent<int>().Initialize(x =>
            {
                x.OnAdd += Values_OnAdd;
            });
             
            //values.Add(50);
        }
        
        void Values_OnAdd (object sender, EventArgs e)
        {
            
            if (SuspendDraw)
                return;
            //int dotSpan = 10;
            int preRealValue = 0;

            DrawSurface drawer = new DrawSurface(Size.X, Size.Y);
            int listValue = Values.Count - Size.X / DotSpan;
            if (listValue < 0)
                listValue = 0;
            for (int i = 0; i < Values.Count && i < Size.X / DotSpan; i++)
            {
                int realValue = (int)(Size.Y - (Values[listValue + i] / (double)MaxValue * ((double)Size.Y)));
                //SDLColor pointColor = new SDLColor((byte)(i * 10 % 256), 0, 0);
                if (DrawLine && i > 0)
                {
                    double claim = (realValue - preRealValue) / (double)DotSpan;
                    if (claim < 1 && claim > -1 )
                    {
                        for (int j = 0; j < DotSpan; j++)
                        {
                            drawer[(i - 1) * DotSpan + j, (int)(preRealValue + (claim * j))] = LineColor;
                        }
                    }
                    else
                    {
                        int numSteps = realValue - preRealValue;
                        double xStep = DotSpan / (double)Math.Abs(numSteps);
                        int xStart = (i - 1) * DotSpan;
                        for (int j = 0; j < Math.Abs(numSteps); j++)
                        {
                            drawer[(int)(xStart + ((double)j * xStep)), preRealValue + (numSteps < 0 ? -j : j) ] = LineColor;
                        }
                    }
                }

                drawer[i * DotSpan + 1, realValue] = DotColor;
                drawer[i * DotSpan - 1, realValue] = DotColor;
                drawer[i * DotSpan, realValue] = DotColor;
                drawer[i * DotSpan, realValue + 1] = DotColor;
                drawer[i * DotSpan, realValue - 1] = DotColor;

                preRealValue = realValue;
            }
            drawer.FillTexture(overlay);
            drawer.Dispose();
            drawer = null;
        }

        //TimeSpan lastUpdate;

        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);
            graphTexture = new DrawSurface(Size.X, Size.Y).Initialize(s => 
                s.CustomDraw((x, y) =>
                {
                    s[x,y] = BackColor;
                    if (x == 0 || x == s.Width - 1 || (DrawGrid && (y == 0 || y == s.Height - 1 || x % 20 == 0 || y % 20 == 0)))
                        s[x, y] = GridColor;                        
                })).MakeTexture(renderer);
            overlay = SDLTexture.CreateEmpty(renderer, Size.X, Size.Y, PixelFormat.RGBA8888);
            overlay.BlendMode = BlendMode.Blend;
            /*textBlock = new TextBlock()
            {
                Size = new SDLPoint(this.Size.X, 20),
                Location = new SDLPoint(0, -200),
                FontSize = 20,
                TextAlign = Align.Center
            };
            textBlock.Initialize(renderer);*/

            
        }
        //int counter = 0;

        public override void Update(GameTime gameTime)
        {
            //TODO: Fix this
            //TODO: Dose not like refresh of entire site
            if (RealTime)
                Values_OnAdd(null, null);
            /*if (gameTime.TotalElapsed > lastUpdate + TimeSpan.FromSeconds(0.01))
            {
                Values.Add((int)(((Math.Sin(counter / (Math.PI * 1)) + 1) / 2) * (400)));
                counter++;
                lastUpdate = gameTime.TotalElapsed;
            }*/
            base.Update(gameTime);
        }

        public override void Draw(SDLRenderer renderer, GameTime gameTime)
        {
            renderer.DrawTexture(graphTexture, new SDLRectangle(0, 0, Size.X, Size.Y));
            renderer.DrawTexture(overlay, new SDLRectangle(0, 0, Size.X, Size.Y));
            /*if (DisplayText)
            {
                textBlock.Text = this.Text;
                textBlock.Draw(renderer, gameTime);
            }*/
        }
    }
}

