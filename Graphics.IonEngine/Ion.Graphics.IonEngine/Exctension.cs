﻿using SDL2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Graphics.IonEngine
{
    public static class Extension
    {
        public static void ThrowIfError(this int i)
        {
            if (i != 0)
            {
                Console.WriteLine("An error eccured: " + SDL.SDL_GetError());
                throw new Exception("SDL Error");//
            }
        }

        public static T Initialize<T>(this T t, Action<T> action)
        {
            action(t);
            return t;
        }
    }

    public class SwitchBody<T>
    {
        Dictionary<T, Action> switchBody = new Dictionary<T, Action>();
        T switchElement;
        Action defaultAction;

        public SwitchBody<T> Case(T t, Action action)
        {
            if (!switchBody.ContainsKey(t))
                switchBody.Add(t, action);
            return this;
        }

        public SwitchBody<T> Default(Action action)
        {
            this.defaultAction = action;
            return this;
        }

        public SwitchBody<T> Switch(T t)
        {
            this.switchElement = t;
            return this;
        }

        public static SwitchBody<T> CreateSwitch()
        {
            return new SwitchBody<T>();
        }

        public void Run()
        {
            if (this.switchBody.ContainsKey(switchElement))
            {
                switchBody[switchElement]();
            }
            else if (defaultAction != null)
                defaultAction();
        }
    }
}
