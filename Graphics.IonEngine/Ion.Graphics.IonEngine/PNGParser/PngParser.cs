﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NicroWare.Lib.IonEngine.PNGParser
{
    public class PngFile
    {
        public byte[] signature;
        List<PngChunk> pngChunks = new List<PngChunk>();
        public PngIHDRHeader IHDRHeader { get; set; }
        public PngIDATHeader IDATHeader { get; set; }

        public PngFile()
        {

        }

        public static PngFile ReadFile(string path)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException($"Could not find {path}");
            }
            BinaryReader reader = new BinaryReader(new FileStream(path, FileMode.Open));
            PngFile file = new PngFile();
            file.signature = reader.ReadBytes(8);
            while (reader.BaseStream.Position < reader.BaseStream.Length)
            {
                file.pngChunks.Add(new PngChunk().ReadFrom(file, reader));
            }
            reader.Close();
            return file;
        }

        public byte[] GenerateBytes()
        {
            int width = (int)IHDRHeader.Width;
            int height = (int)IHDRHeader.Height;
            byte[] allBytes = new byte[width * height * 4];
            int scanLine = width * 4 + 1;
            for (int y = 0; y < height; y++)
            {
                byte filterMethod = IDATHeader.Data[y * scanLine];
                for (int x = 1; x < scanLine; x++)
                {
                    int curX = x - 1;
                    byte current = IDATHeader.Data[x + y * scanLine];
                    int calc = current;
                    switch (filterMethod)
                    {

                        //TODO: Make check to see if x gose to prev line, it works for now, but can result in errors.
                        case 1://Sub
                            calc = (current + allBytes[curX - 4 + (y) * (scanLine - 1)]) % 256;
                            break;
                        case 2: //Up
                            calc = (current + allBytes[curX + (y - 1) * (scanLine - 1)]) % 256;
                            break;
                        case 3: //Average
                            calc = (current + (allBytes[curX - 4 + y * (scanLine - 1)] + allBytes[curX + (y - 1) * (scanLine - 1)]) / 2) % 256;
                            break;
                        case 4: //Paeth
                            if (x > 5)
                            {
                                calc = (current + PaethPredicator(allBytes[curX - 4 + y * (scanLine - 1)], allBytes[curX + (y - 1) * (scanLine - 1)], allBytes[curX - 4 + (y - 1) * (scanLine - 1)])) % 256;
                            }
                            else
                            {
                                calc = (current + PaethPredicator(0, allBytes[curX + (y - 1) * (scanLine - 1)], 0)) % 256;
                            }
                            break;
                        default:
                            //http://www.libpng.org/pub/png/spec/1.2/PNG-Filters.html
                            break;
                    }
                    allBytes[curX + y * (scanLine - 1)] = (byte)Math.Min(calc, 255);
                }
            }
            return allBytes;
        }

        public byte PaethPredicator(byte left, byte above, byte upperLeft)
        {
            int p = left + above - upperLeft;
            int pa = Math.Abs(p - left);
            int pb = Math.Abs(p - above);
            int pc = Math.Abs(p - upperLeft);
            if (pa <= pb && pa <= pc)
                return left;
            else if (pb <= pc)
                return above;
            else
                return upperLeft;
        }

        public Image GenerateImage()
        {
            Bitmap bitmap = new Bitmap((int)IHDRHeader.Width, (int)IHDRHeader.Height);
            for (int y = 0; y < bitmap.Width; y++)
            {

            }
            
            return bitmap;
        }
    }

    public class PngChunk
    {
        public uint Length { get; set; }
        public byte[] ChunkType { get; set; }
        public byte[] Data { get; set; }
        public byte[] CRC { get; set; }
        public PngChunkHeader Header { get; set; }

        public string Type
        {
            get
            {
                return Encoding.ASCII.GetString(ChunkType);
            }
        }

        /// <summary>
        /// False if critical, True of ancillary
        /// </summary>
        public bool Ancillary
        {
            get
            {
                return (ChunkType[0] & 0x20) > 0;
            }
        }

        /// <summary>
        /// False : public part of PNG spesification, True, not part of png spesification
        /// </summary>
        public bool Private
        {
            get
            {
                return (ChunkType[1] & 0x20) > 0;
            }
        }

        /// <summary>
        /// Must be false, reserved for future use
        /// </summary>
        public bool Reserved
        {
            get
            {
                return (ChunkType[2] & 0x20) > 0;
            }
        }

        /// <summary>
        /// Indicates if a editor can copy this field if it is not recogised
        /// </summary>
        public bool SafeToCopy
        {
            get
            {
                return (ChunkType[3] & 0x20) > 0;
            }
        }

        public PngChunk ReadFrom(PngFile file, BinaryReader reader)
        {
            this.Length = reader.ReadUInt32BE();
            ChunkType = reader.ReadBytes(4);
            Data = reader.ReadBytes((int)this.Length);
            CRC = reader.ReadBytes(4);

            switch (Type)
            {
                case "IHDR":
                    Header = new PngIHDRHeader().ReadFrom(Data);
                    file.IHDRHeader = (PngIHDRHeader)Header;
                    break;
                case "PLTE":
                    Header = new PngPLTEHeader().ReadFrom(Data);
                    break;
                case "IDAT":
                    Header = new PngIDATHeader().ReadFrom(Data);
                    file.IDATHeader = (PngIDATHeader)Header;
                    break;
            }

            return this;
        }

        public override string ToString()
        {
            return $"{Type} Chunk";
        }
    }

    public class PngChunkHeader
    {

    }

    public class PngIHDRHeader : PngChunkHeader
    {
        public uint Width { get; set; }
        public uint Height { get; set; }
        public byte BitDepth { get; set; }
        public byte ColorType { get; set; }
        public byte CompressionMethod { get; set; }
        public byte FilterMethod { get; set; }
        public byte InterlaceMethod { get; set; }

        public PngIHDRHeader ReadFrom(byte[] bytes)
        {
            BinaryReader reader = new BinaryReader(new MemoryStream(bytes));
            Width = reader.ReadUInt32BE();
            Height = reader.ReadUInt32BE();
            BitDepth = reader.ReadByte();
            ColorType = reader.ReadByte();
            CompressionMethod = reader.ReadByte();
            FilterMethod = reader.ReadByte();
            InterlaceMethod = reader.ReadByte();
            return this;
        }
    }

    public class PngPLTEHeader : PngChunkHeader
    {
        public List<PalettEntry> Paletts { get; set; } = new List<PalettEntry>();

        public PngPLTEHeader ReadFrom(byte[] bytes)
        {
            for (int i = 0; i < bytes.Length; i+=3)
            {
                Paletts.Add(new PalettEntry() { Red = bytes[i], Green = bytes[i + 1], Blue = bytes[i + 2] });
            }
            return this;
        }
    }

    public class PngIDATHeader : PngChunkHeader
    {
        public byte[] Data { get; set; }

        public PngIDATHeader ReadFrom(byte[] bytes)
        {
            MemoryStream baseStream = new MemoryStream(bytes);
            baseStream.ReadByte();
            baseStream.ReadByte();
            DeflateStream stream = new DeflateStream(baseStream, CompressionMode.Decompress);
            MemoryStream temp = new MemoryStream();

            stream.CopyTo(temp);
            Data = temp.ToArray();

            return this;
        }
    }

    public class PalettEntry
    {
        public byte Red { get; set; }
        public byte Green { get; set; }
        public byte Blue { get; set; }
    }

    /// <summary>
    /// BinaryReader Extended reading, support for BigEndian like PNG
    /// </summary>
    public static class BREx
    {
        public static uint ReadUInt32BE(this BinaryReader reader)
        {
            return BitConverter.ToUInt32(reader.ReadBytesReversed(4), 0);
        }

        public static double ReadDoubleBE(this BinaryReader reader)
        {
            return BitConverter.ToDouble(reader.ReadBytesReversed(8), 0);
        }

        public static byte[] ReadBytesReversed(this BinaryReader reader, int count)
        {
            byte[] bytes = reader.ReadBytes(count);
            Array.Reverse(bytes);
            return bytes;
        }
    }
}
