﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Graphics.IonEngine.MathExt
{
    public class MathHelper
    {
        public const double TWO_PI = Math.PI * 2;

        
        public static int[,] GenerateDegreeTanArray(int width, int height)
        {

            return Generate2DArray<int>(width, height, (x, y) => (int)(ToDegree(Math.Atan2(y - height / 2, x - width / 2)) + 0.5));
        }

        public static double ToDegree(double rad) => (rad / TWO_PI * 360);
        public static double ToRadience(double degree) => degree / 360 * TWO_PI;

        public static T[,] Generate2DArray<T>(int width, int height, Func<int, int, T> spotFunc)
        {
            T[,] array = new T[width, height];
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    array[x, y] = spotFunc(x, y);
                }
            }
            return array;
        }
    }
}
