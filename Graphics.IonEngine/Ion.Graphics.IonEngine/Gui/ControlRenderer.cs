﻿using Ion.Graphics.IonEngine.Drawing;
using Ion.Graphics.IonEngine.Font;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Graphics.IonEngine.Gui
{
    public class ControlRenderer : SDLRenderer
    {
        SDLRenderer parrent;
        GuiElement container;

        public SDLPoint Location
        {
            get
            {
                return container.Location;
            }
        }

        public ControlRenderer(SDLRenderer renderer, GuiElement container)
            : base(renderer.BasePointer)
        {
            this.container = container;
            parrent = renderer;
        }

        private SDLRectangle ConvertRectangle(SDLRectangle rectangle)
        {
            if (container.Rotation == 0)
            {
                return new SDLRectangle(rectangle.X + Location.X, rectangle.Y + Location.Y, rectangle.Width, rectangle.Height);
            }
            else
            {
                SDLDPoint rectCenter = rectangle.Center;
                SDLDPoint rectVector = new SDLDPoint(rectangle.X, rectangle.Y) - rectangle.Center;
                SDLDPoint controllVector = rectCenter - new SDLDPoint(container.Size.X / 2.0, container.Size.Y / 2.0);
                rectVector = rectVector.Rotate(container.Rotation);
                controllVector = controllVector.Rotate(container.Rotation);
                SDLDPoint ctrlCenter = new SDLRectangle(Location, container.Size).Center;

                return new SDLRectangle((int)(ctrlCenter.X + controllVector.X + rectVector.X),(int)(ctrlCenter.Y + controllVector.Y + rectVector.Y), rectangle.Width, rectangle.Height);
            }
        }

        public SDLRectangle GetRealLocation()
        {
            SDLRectangle rect = container.Area;
            return (parrent as ControlRenderer)?.GetRealLocation(rect) ?? rect;
        }

        private SDLRectangle GetRealLocation(SDLRectangle rect)
        {
            return (parrent as ControlRenderer)?.GetRealLocation(ConvertRectangle(rect)) ?? rect;
        }

        public override void DrawTexture(SDLTexture texture, SDLRectangle destination)
        {
            //parrent.DrawTexture(texture, ConvertRectangle(destination));
            DrawTexture(texture, new SDLRectangle(0, 0, texture.Width, texture.Height), destination, 0, new SDLPoint(0,0) /*new SDLPoint(texture.Width / 2, texture.Height / 2)*/, RendererFlip.None);
        }

        public override void DrawTexture(SDLTexture texture, SDLRectangle sourceRect, SDLRectangle destRect, double angle, SDLPoint centerPoint, RendererFlip flip)
        {
            SDLRectangle newRect = ConvertRectangle(destRect);
            parrent.DrawTexture(texture, sourceRect, newRect , angle + container.Rotation, centerPoint, flip);
        }

        public override void DrawTexture(SDLTexture texture, SDLRectangle sourceRect, SDLRectangle destRect, double angle, SDLPoint centerPoint, RendererFlip flip, SDLColor color)
        {
            parrent.DrawTexture(texture, sourceRect, ConvertRectangle(destRect), angle + container.Rotation, centerPoint, flip, color);
        }

        public override void DrawLine(SDLPoint beginPoint, SDLPoint endPoint, SDLColor color)
        {
            parrent.DrawLine(beginPoint + container.Location, endPoint + container.Location, color);
        }

        public override void DrawText(string text, SDLFont font, SDLRectangle destination, SDLColor color)
        {
            parrent.DrawText(text, font, ConvertRectangle(destination), color);
        }
    }
}
