﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Graphics.IonEngine.Gui
{
    public class GuiContainerElement : GuiElement
    {
        bool isInitialzed = false;
        public ElementCollection Elements { get; private set; }

        public GuiContainerElement()
        {
            Elements = new ElementCollection();
            Elements.Added += Elements_Added;
            Elements.Cleard += Elements_Cleard; ;
        }

        private void Elements_Cleard(object sender, EventArgs e)
        {
            OnElementsCleard();
        }

        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);
            isInitialzed = true;
            foreach (GuiElement ge in Elements)
            {
                ge.Initialize(base.Renderer);
                OnElementAdd(ge);
            }
        }

        private void Elements_Added(object sender, ValueEventArgs<GuiElement> e)
        {
            if (isInitialzed)
            {
                e.Value.Initialize(base.Renderer);
                OnElementAdd(e.Value);
            }
        }

        public virtual void OnElementAdd(GuiElement e)
        {

        }

        public virtual void OnElementsCleard()
        {

        }

        public override void Update(GameTime gameTime)
        {
            foreach (GuiElement element in Elements)
            {
                element.Update(gameTime);
            }
        }

        public override void Draw(SDLRenderer renderer, GameTime gameTime)
        {
            foreach (GuiElement element in Elements)
            {
                element.Draw(element.Renderer, gameTime);
            }
        }
    }

    public class ElementCollection : IList<GuiElement>
    {
        List<GuiElement> baseList = new List<GuiElement>();
        public int Count
        {
            get
            {
                return baseList.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public GuiElement this[int index]
        {
            get
            {
                return baseList[index];
            }

            set
            {
                baseList[index] = value;
            }
        }

        public event EventHandler<ValueEventArgs<GuiElement>> Added;
        public event EventHandler Cleard;

        public ElementCollection()
            : base()
        {
        }

        public void Add(GuiElement element)
        {
            Added?.Invoke(this, new ValueEventArgs<GuiElement>(element));
            baseList.Add(element);
            
        }

        public void AddRange(IEnumerable<GuiElement> collection)
        {
            if (Added != null)
            {
                foreach (GuiElement ge in collection)
                {
                    Added(this, new ValueEventArgs<GuiElement>(ge));
                }
            }
            baseList.AddRange(collection);
        }

        public int IndexOf(GuiElement item)
        {
            return baseList.IndexOf(item);
        }

        public void Insert(int index, GuiElement item)
        {
            baseList.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            baseList.RemoveAt(index);
        }

        public void Clear()
        {
            foreach (GuiElement element in baseList)
            {
                element.Dispose();
            }
            baseList.Clear();
            Cleard?.Invoke(this, new EventArgs());
        }

        public bool Contains(GuiElement item)
        {
            return baseList.Contains(item);
        }

        public void CopyTo(GuiElement[] array, int arrayIndex)
        {
            baseList.CopyTo(array, arrayIndex);
        }

        public bool Remove(GuiElement item)
        {
            return baseList.Remove(item);
        }

        public IEnumerator<GuiElement> GetEnumerator()
        {
            return baseList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public class ValueEventArgs<T> : EventArgs
    {
        public T Value { get; private set; }

        public ValueEventArgs(T value)
        {
            this.Value = value;
            
        }
    }
}
