﻿using Ion.Graphics.IonEngine.Drawing;
using Ion.Graphics.IonEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Graphics.IonEngine.Gui
{
    public abstract class GuiElement : IDisposable
    {
        public virtual SDLPoint Location { get; set; }
        public virtual SDLPoint Size { get; set; }
        public virtual string Text { get; set; }
        public ControlRenderer Renderer { get; set; }
        protected MouseState current;
        protected MouseState last;
        public virtual double Rotation { get; set; }
        public SDLRectangle Area => new SDLRectangle(Location, Size);
        bool Disposed = false;

        public GuiElement()
        {
            Location = new SDLPoint(0, 0);
            Size = new SDLPoint(100, 100);
            Text = "";
        }

        protected void PullEvents()
        {
            last = current;
            current = Mouse.GetState();
        }

        public virtual void Initialize(SDLRenderer renderer)
        {
            Renderer = new ControlRenderer(renderer, this);
        }

        /// <summary>
        /// Checks if point intersects controller
        /// </summary>
        /// <returns>Returns true if point is inside the Area of the controller</returns>
        public bool Intersect(SDLPoint point)
        {
            return Renderer.GetRealLocation().Intersect(point);
        }

        public bool IsMouseControlDown()
        {
            return current.IsKeyDown(MouseButtons.Left) && Intersect(current.Location);
        }

        public bool IsMouseControlUp()
        {
            return current.IsKeyUp(MouseButtons.Left) && Intersect(current.Location);
        }

        /// <summary>
        /// Checks if Mouse is clicked
        /// </summary>
        /// <returns>Returns true if the mouse intersects the controller and the left mouse button is pressed down</returns>
        public bool IsMouseClicked()
        {
            return last.IsKeyUp(MouseButtons.Left) && current.IsKeyDown(MouseButtons.Left);
        }

        /// <summary>
        /// Checks if Mouse is released
        /// </summary>
        /// <returns>Returns true if the mouse intersects the controller and the left mouse button is pressed down</returns>
        public bool IsMouseReleased()
        {
            return last.IsKeyDown(MouseButtons.Left) && current.IsKeyUp(MouseButtons.Left);
        }

        /// <summary>
        /// Checks if Mouse is over controleller and clicked
        /// </summary>
        /// <returns>Returns true if the mouse intersects the controller and the left mouse button is pressed down</returns>
        public bool IsMouseControlClicked()
        {
            return IsMouseClicked() && Intersect(current.Location);
        }

        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual void Draw(SDLRenderer renderer, GameTime gameTime)
        {

        }

        public virtual void OnDispose()
        {

        }

        public void Dispose()
        {
            if (!Disposed)
            {
                OnDispose();
            }
            else
            {
                throw new ObjectDisposedException(nameof(GuiElement));
            }
        }
    }
}
