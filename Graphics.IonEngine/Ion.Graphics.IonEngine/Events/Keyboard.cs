﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Graphics.IonEngine.Events
{
    public enum Keys : int
    {
        A = 4,
        B = 5,
        C = 6,
        D = 7,
        E = 8,
        F = 9,
        G = 10,
        H = 11,
        I = 12,
        J = 13,
        K = 14,
        L = 15,
        M = 16,
        N = 17,
        O = 18,
        P = 19,
        Q = 20,
        R = 21,
        S = 22,
        T = 23,
        U = 24,
        V = 25,
        W = 26,
        X = 27,
        Y = 28,
        Z = 29,
        Num1 = 30,
        Num2 = 31,
        Num3 = 32,
        Num4 = 33,
        Num5 = 34,
        Num6 = 35,
        Num7 = 36,
        Num8 = 37,
        Num9 = 38,
        Num0 = 39,
        ESC = 41,
        F1 = 58,
        F2 = 59,
        F3 = 60,
        F4 = 61,
        F5 = 62,
        F6 = 63,
        F7 = 64,
        F8 = 65,
        F9 = 66,
        F10 = 67,
        F11 = 68,
        F12 = 69,
        RIGHT = 79,
        LEFT = 80,
        DOWN = 81,
        UP = 82,
        SPECIAL1 = 200,
        SPECIAL2 = 201,
        SPECIAL3 = 202,
        SPECIAL4 = 203
    }

    public class Keyboard
    {
        static List<Keys> keyDown = new List<Keys>();

        public Keyboard()
        {

        }

        public static void AddKey(Keys key)
        {
            if (!keyDown.Contains(key))
                keyDown.Add(key);
        }

        public static void RemoveKey(Keys key)
        {
            keyDown.Remove(key);
        }

        public static KeyboardState GetState()
        {
            return new KeyboardState(keyDown.ToArray());
        }
    }

    public struct KeyboardState
    {
        public Keys[] Keys { get; private set; }

        public KeyboardState(Keys[] keys)
        {
            this.Keys = keys;
        }

        public bool IsKeyDown(Keys key)
        {
            if (Keys == null)
                return false;
            foreach (Keys keyC in Keys)
            {
                if (keyC == key)
                    return true;
            }
            return false;
        }

        public bool IsKeyUp(Keys key)
        {
            return !IsKeyDown(key);
        }
    }
}
