﻿using SDL2;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Graphics.IonEngine.Drawing
{
    public struct SDLPoint
    {
        public static readonly SDLPoint Zero = new SDLPoint(0,0);

        public int X { get; set; }
        public int Y { get; set; }

        [DebuggerStepThrough]
        public SDLPoint(int x, int y)
            : this()
        {
            this.X = x;
            this.Y = y;
        }

        public double Length { get { return Math.Sqrt(X * X + Y * Y); } }

        public SDLPoint Rotate(double rotation)
        {
            double curRot = Math.Atan2(Y, X);
            return new SDLPoint((int)(Math.Cos(curRot + rotation) * Length), (int)(Math.Sin(curRot + rotation) * Length));
        }

        public static SDLPoint operator +(SDLPoint a, SDLPoint b)
        {
            return new SDLPoint(a.X + b.X, a.Y + b.Y);
        }

        public static SDLPoint operator -(SDLPoint a, SDLPoint b)
        {
            return new SDLPoint(a.X - b.X, a.Y - b.Y);
        }

        

        public static implicit operator SDLPoint(SDL.SDL_Point point)
        {
            return new SDLPoint(point.x, point.y);
        }

        public static implicit operator SDL.SDL_Point(SDLPoint point)
        {
            return new SDL.SDL_Point() { x = point.X, y = point.Y };
        }

        public override string ToString()
        {
            return $"X: {X}, Y: {Y}";
        }
    }

    public struct SDLDPoint
    {
        public double X { get; set; }
        public double Y { get; set; }

        public SDLDPoint(double x, double y)
            : this()
        {
            this.X = x;
            this.Y = y;
        }

        public double Length { get { return Math.Sqrt(X * X + Y * Y); } }

        public SDLDPoint Rotate(double rotation)
        {
            double curRot = Math.Atan2(Y, X);
            return new SDLDPoint(Math.Cos(curRot + rotation) * Length, Math.Sin(curRot + rotation) * Length);
        }

        public static SDLDPoint operator +(SDLDPoint a, SDLDPoint b)
        {
            return new SDLDPoint(a.X + b.X, a.Y + b.Y);
        }

        public static SDLDPoint operator -(SDLDPoint a, SDLDPoint b)
        {
            return new SDLDPoint(a.X - b.X, a.Y - b.Y);
        }

        public static implicit operator SDLDPoint(SDLPoint point)
        {
            return new SDLDPoint(point.X, point.Y);
        }

        public static explicit operator SDLPoint(SDLDPoint point)
        {
            return new SDLPoint((int)point.X, (int)point.Y);
        }


        public override string ToString()
        {
            return $"X: {X}, Y: {Y}";
        }
    }
}
