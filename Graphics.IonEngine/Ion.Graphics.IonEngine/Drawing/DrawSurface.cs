﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Graphics.IonEngine.Drawing
{
    public class DrawSurface : IDisposable
    {
        public byte[] pixels;
        int width, height;
        public int Width { get { return width; } }
        public int Height { get { return height; } }
        public DrawSurface(int width, int height)
        {
            this.width = width;
            this.height = height;
            pixels = new byte[width * height * 4];
        }

        ~DrawSurface()
        {
            Dispose();
        }

        public byte this[int x, int y, byte c]
        {
            get
            {
                return pixels[(x + y * width) * 4 + c];
            }
            set
            {
                if (x >= 0 && x < Width && y >= 0 && y < Height)
                {
                    pixels[(x + y * width) * 4 + c] = value;
                }
            }
        }

        public SDLColor this[int x, int y]
        {
            get
            {
                int index = (x + y * width) * 4;
                return new SDLColor(pixels[index], pixels[index + 3], pixels[index + 2], pixels[index + 1]);
            }
            set
            {
                if (x >= 0 && x < Width && y >= 0 && y < Height)
                {
                    int index = (x + y * width) * 4;
                    pixels[index] = value.A;
                    pixels[index + 3] = value.R;
                    pixels[index + 2] = value.G;
                    pixels[index + 1] = value.B;
                }
            }
        }

        public void Clear()
        {
            pixels = new byte[width * height * 4];
        }

        public void DrawRectangle(SDLRectangle rect, SDLColor color)
        {
            for (int x = rect.X; x < rect.Width; x++)
            {
                this[x, rect.Y] = color;
                this[x, rect.Y + rect.Height - 1] = color;
            }
            for (int y = rect.Y; y < rect.Height; y++)
            {
                this[rect.X, y] = color;
                this[rect.X + rect.Width - 1, y] = color;
            }
        }

        public void DrawLine(SDLPoint beginPoint, SDLPoint endPoint, SDLColor color)
        {
            SDLPoint diff = endPoint - beginPoint;
            double delta = diff.Y / diff.X;
            //double claim = (endPoint.Y - preRealValue) / (double)DotSpan;
            if (delta < 1 && delta > -1)
            {
                for (int x = 0; x < diff.X; x++)
                {
                    this[beginPoint.X + x, beginPoint.Y + (int)(x * delta)] = color;
                }
            }
            else
            {
                delta = 1 / delta;
                for (int y = 0; y < diff.Y; y++)
                {
                    this[beginPoint.X + (int)(y * delta), beginPoint.Y + y] = color;
                }
            }
        }

        public void CustomDraw(Action<int, int> action)
        {
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    action(x, y);
                }
            }
        }

        public void FillTexture(SDLTexture texture)
        {
            Marshal.Copy(pixels, 0, texture.Lock(), pixels.Length);
            texture.UnLock();
        }

        public SDLTexture MakeTexture(SDLRenderer renderer)
        {
            SDLTexture texture = SDLTexture.CreateEmpty(renderer, width, height, SDL2.SDL.SDL_PIXELFORMAT_RGBA8888);

            FillTexture(texture);
            return texture;
        }

        public bool IsDisposed { get; private set; }

        public void Dispose()
        {
            if (!IsDisposed)
            {
                pixels = null;
                IsDisposed = true;
            }
        }
    }
}
