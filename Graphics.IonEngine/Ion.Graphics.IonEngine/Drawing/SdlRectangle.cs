﻿using SDL2;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Graphics.IonEngine.Drawing
{
    public struct SDLRectangle
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public SDLPoint Center
        {
            get
            {
                return new SDLPoint(Width / 2 + X, Height / 2 + Y);
            }
        }

        public SDLRectangle(SDLPoint location, SDLPoint size)
            : this(location.X, location.Y, size.X, size.Y)
        {

        }

        [DebuggerStepThrough]
        public SDLRectangle(int x, int y, int width, int height)
            : this()
        {
            this.X = x;
            this.Y = y;
            this.Width = width;
            this.Height = height;
        }

        public bool Intersect(SDLPoint point)
        {
            return point.X > X 
                && point.X < X + Width 
                && point.Y > Y 
                && point.Y < Y + Height;
        }

        public bool Intersect(SDLRectangle rect)
        {
            return X + Width > rect.X
                && Y + Height > rect.Y
                && X < rect.X + rect.Width
                && Y < rect.Y + rect.Height;
        }

        


        public static implicit operator SDL.SDL_Rect(SDLRectangle rect)
        {
            return new SDL.SDL_Rect() { x = rect.X, y = rect.Y, w = rect.Width, h = rect.Height };
        }

        public static implicit operator SDLRectangle(SDL.SDL_Rect rect)
        {
            return new SDLRectangle(rect.x, rect.y, rect.w, rect.h);
        }
    }
}
