﻿using SDL2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Graphics.IonEngine.Drawing
{
    public struct SDLColor
    {
        static Random rnd = new Random();
        public byte R { get; set; }
        public byte G { get; set; }
        public byte B { get; set; }
        public byte A { get; set; }

        public SDLColor(byte red, byte green, byte blue)
            : this()
        {
            this.A = 255;
            this.R = red;
            this.G = green;
            this.B = blue;
        }

        public SDLColor(byte alpha, byte red, byte green, byte blue)
            : this(red, green, blue)
        {
            this.A = alpha;
        }

        public static implicit operator SDL.SDL_Color(SDLColor color)
        {
            return new SDL.SDL_Color() { r = color.R, g = color.G, b = color.B, a = color.A };
        }

        public static implicit operator SDLColor(SDL.SDL_Color color)
        {
            return new SDLColor(color.a, color.r, color.g, color.b);
        }

        public static implicit operator System.Drawing.Color(SDLColor color)
        {
            return System.Drawing.Color.FromArgb(color.A, color.R, color.G, color.B);
        }

        public static implicit operator SDLColor(System.Drawing.Color color)
        {
            return new SDLColor(color.A, color.R, color.G, color.B);
        }

        public static SDLColor RandomColor()
        {
            return new SDLColor((byte)rnd.Next(256), (byte)rnd.Next(256), (byte)rnd.Next(256));
        }

        public static SDLColor RandomColor(int minMean, int maxMean)
        {
            byte r = 0;
            byte g = 0;
            byte b = 0;
            do
            {
                r = (byte)rnd.Next(256);
                g = (byte)rnd.Next(256);
                b = (byte)rnd.Next(256);
            }
            while (r + g + b < minMean || r + g + b > maxMean);
            return new SDLColor(r, g, b);
        }

        public static readonly SDLColor NoColor = new SDLColor(0, 0, 0, 0);
        public static readonly SDLColor White = new SDLColor(255, 255, 255);
        public static readonly SDLColor Black = new SDLColor(0, 0, 0);
        public static readonly SDLColor Red = new SDLColor(255, 0, 0);
        public static readonly SDLColor Green = new SDLColor(0, 255, 0);
        public static readonly SDLColor Blue = new SDLColor(0, 0, 255);
        public static readonly SDLColor DarkRed = new SDLColor(175, 0, 0);
        public static readonly SDLColor DarkGreen = new SDLColor(0, 175, 0);
        public static readonly SDLColor DarkBlue = new SDLColor(0, 0, 175);
        public static readonly SDLColor DarkGray = new SDLColor(50, 50, 50);
        public static readonly SDLColor IonBlue = new SDLColor(0, 183, 221);
        public static readonly SDLColor Grey = new SDLColor(30, 30, 30);
        public static readonly SDLColor DarkGrey = new SDLColor(15, 15, 15);
        public static readonly SDLColor LightGrey = new SDLColor(100, 100, 100);

    }
}
