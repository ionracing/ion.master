﻿using System;
using SDL2;
using System.Diagnostics;
using Ion.Graphics.IonEngine.Drawing;
using Ion.Graphics.IonEngine.Events;

namespace Ion.Graphics.IonEngine
{
    public class SDLRenderWindow : SDLWindow
    {
        bool running = true;
        StartableGameTime baseTime;
        string title;
        public string Title
        {
            get
            {
                if (title == null)
                {
                    title = SDL.SDL_GetWindowTitle(BasePointer);
                }
                return title;
            }
            set
            {
                title = value;
                SDL.SDL_SetWindowTitle(BasePointer, title);
            }
        }

        public SDLPoint Size
        {
            get
            {
                return size;
            }
            set
            {
                this.size = value;
                SDL.SDL_SetWindowSize(BasePointer, size.X, size.Y);
            }
        }
        SDLPoint size;

        SDLPoint location;
        public SDLPoint Location
        {
            get
            {
                return location;
            }
            set
            {
                this.location = value;
                SDL.SDL_SetWindowPosition(BasePointer, location.X, location.Y);
            }
        }

        public bool ShowCursor
        {
            get
            {
                //1 = Show
                //0 = Hide

                return SDL.SDL_ShowCursor(-1) == 1;
            }
            set
            {
                SDL.SDL_ShowCursor(value ? 1 : 0);
            }
        }

        /// <summary>
        /// Get or set if there should be a lock on max frame rate. Use FrameRate to set the desired framerate
        /// <para>Default: false</para>
        /// </summary>
        public bool FrameLock { get; set; }

        /// <summary>
        /// Get or set the desired frame rate too use when FrameLock is true
        /// <para>Default: 60</para>
        /// </summary>
        public double FrameRate { get; set; }

        public SDLRenderWindow()
            :base()
        {
            baseTime = new StartableGameTime();
            FrameLock = false;
            FrameRate = 60;
        }

        public override IntPtr LateInit()
        {
            title = "Render window";
            int num = 0;
            SDL.SDL_VideoInit(SDL.SDL_GetVideoDriver(num)).ThrowIfError();
            SDL.SDL_Init(SDL.SDL_INIT_VIDEO).ThrowIfError();
            SDL_ttf.TTF_Init().ThrowIfError();
            //SDL.SDL_ShowCursor(0);
            ShowCursor = false;
            //TODO: Move code

            //Input.Initialize();
            size = new SDLPoint(800, 480);
            location = new SDLPoint(0, 0);
            return SDLWindow.CreatePointer(title, Location.X, Location.Y, Size.X, Size.Y);
        }

        public void Run()
        {
            Initialize();
            LoadContent();
            while (running)
            {
                baseTime.Update();
                Event.DoEvents();
                if (Event.Exit)
                    running = false;
                Update(baseTime);
                Draw(baseTime);
                if (FrameLock)
                {
                    double num = 1.0 / FrameRate * 1000.0 - baseTime.SinceLastUpdate.TotalMilliseconds;
                    SDL.SDL_Delay((uint)(num < 0 ? 0 : num));
                }
            }
            UnloadContent();
            this.Dispose();
            SDL_ttf.TTF_Quit();
            SDL.SDL_Quit();
        }

        public void Quit()
        {
            running = false;
        }

        public virtual void Initialize()
        {

        }

        public virtual void LoadContent()
        {

        }

        public virtual void UnloadContent()
        {

        }

        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual void Draw(GameTime gameTime)
        {

        }
    }
}


