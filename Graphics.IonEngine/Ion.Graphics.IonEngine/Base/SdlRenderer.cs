﻿using Ion.Graphics.IonEngine.Drawing;
using Ion.Graphics.IonEngine.Font;
using SDL2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Graphics.IonEngine
{
    /// <summary>
    /// Determins what flip orientation the renderer should use
    /// </summary>
    public enum RendererFlip : int
    {
        None = 0,
        Horisontal = 1,
        Vertical = 2,
        Both = 3,
    }

    public class SDLRenderer : SDLBase
    {
        SDLColor drawColor;
        public SDLColor DrawColor
        {
            get
            {
                return drawColor;
            }
            set
            {
                drawColor = value;
                SDL.SDL_SetRenderDrawColor(BasePointer, value.R, value.G, value.B, value.A);
            }
        }
        static int renderDisposeCounter = 0;

        public SDLRenderer(IntPtr basePointer)
            :base(basePointer)
        {
            renderDisposeCounter++;
        }

        public static SDLRenderer Create(SDLWindow window)
        {
            return new SDLRenderer(SDL.SDL_CreateRenderer(window.BasePointer, -1, (uint)SDL.SDL_RendererFlags.SDL_RENDERER_ACCELERATED));
        }

        public void Clear()
        {
            SDL.SDL_RenderClear(BasePointer);
        }

        public void Present()
        {
            SDL.SDL_RenderPresent(BasePointer);
        }

        public void Copy(SDLTexture texture, ref SDLRectangle rect)
        {
            SDL.SDL_Rect temp = rect;
            int error = SDL.SDL_RenderCopy(BasePointer, texture.BasePointer, IntPtr.Zero, ref temp);
            if (error != 0)
                Console.WriteLine(SDL.SDL_GetError());
            rect = temp;
        }

        public void Copy(SDLTexture texture, ref SDLRectangle sourceRect, ref SDLRectangle destRect, double angle, ref SDLPoint centerPoint, RendererFlip flip)
        {
            SDL.SDL_Rect src = sourceRect;
            SDL.SDL_Rect dst = destRect;
            SDL.SDL_Point p = centerPoint;
            SDL.SDL_RenderCopyEx(BasePointer, texture.BasePointer, ref src, ref dst, angle, ref p, (SDL.SDL_RendererFlip)flip);
        }

        public void Copy(SDLTexture texture, ref SDLRectangle sourceRect, ref SDLRectangle destRect, double angle, ref SDLPoint centerPoint, RendererFlip flip, SDLColor color)
        {
            SDL.SDL_Rect src = sourceRect;
            SDL.SDL_Rect dst = destRect;
            SDL.SDL_Point p = centerPoint;
            SDL.SDL_SetTextureColorMod(texture.BasePointer, color.R, color.G, color.B);
            SDL.SDL_RenderCopyEx(BasePointer, texture.BasePointer, ref src, ref dst, angle, ref p, (SDL.SDL_RendererFlip)flip);
        }

        public virtual void DrawTexture(SDLTexture texture, SDLRectangle destination)
        {
            Copy(texture, ref destination);
        }

        public virtual void DrawTexture(SDLTexture texture, SDLRectangle sourceRect, SDLRectangle destRect, double angle, SDLPoint centerPoint, RendererFlip flip)
        {
            Copy(texture, ref sourceRect, ref destRect, angle / (Math.PI * 2) * 360, ref centerPoint, flip);
        }

        public virtual void DrawTexture(SDLTexture texture, SDLRectangle sourceRect, SDLRectangle destRect, double angle, SDLPoint centerPoint, RendererFlip flip, SDLColor color)
        {
            Copy(texture, ref sourceRect, ref destRect, angle / (Math.PI * 2) * 360, ref centerPoint, flip, color);
        }

        public virtual void DrawText(string text, SDLFont font, SDLRectangle destination, SDLColor color)
        {
            if (text.Length > 0)
            {
                SDLSurface temp = SDLSurface.FromPointer(SDL_ttf.TTF_RenderText_Solid(font.BasePointer, text, color));
                SDLTexture temp2 = SDLTexture.CreateFrom(this, temp);
                DrawTexture(temp2, destination);
                temp.Dispose();
                temp2.Dispose();
            }
        }

        public virtual void DrawLine(SDLPoint beginPoint, SDLPoint endPoint, SDLColor color)
        {
            SDLDPoint diff = endPoint - beginPoint;
            double delta = (diff.X == 0 ? 0 : (diff.Y / diff.X));
            //double claim = (endPoint.Y - preRealValue) / (double)DotSpan;
            if (delta < 1 && delta > -1 && diff.X != 0)
            {
                for (int x = 0; x < diff.X; x++)
                {
                    DrawPoint(beginPoint.X + x, beginPoint.Y + (int)(x * delta /*+ 0.5*/), color);
                }
                for (int x = 0; x > diff.X; x--)
                {
                    DrawPoint(beginPoint.X + x, beginPoint.Y + (int)(x * delta /*+ 0.5*/), color);
                }
            }
            else
            {
                if (delta != 0)
                    delta = 1 / delta;
                for (int y = 0; y < diff.Y; y++)
                {
                    DrawPoint(beginPoint.X + (int)(y * delta /*+ 0.5*/), beginPoint.Y + y, color);
                }
                for (int y = 0; y > diff.Y; y--)
                {
                    DrawPoint(beginPoint.X + (int)(y * delta /*+ 0.5*/), beginPoint.Y + y, color);
                }

            }
        }

        public void DrawPoint(int x, int y)
        {
            SDL.SDL_RenderDrawPoint(BasePointer, x, y);
        }

        public void DrawPoint(int x, int y, SDLColor color)
        {
            SDLColor pre = DrawColor;
            DrawColor = color;
            DrawPoint(x, y);
            DrawColor = pre;
        }

        protected override void OnDispose()
        {
            if ((--renderDisposeCounter) == 0)
                SDL.SDL_DestroyRenderer(BasePointer);
        }
    }
}
