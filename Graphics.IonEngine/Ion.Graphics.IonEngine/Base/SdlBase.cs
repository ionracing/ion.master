﻿using System;
using SDL2;

namespace Ion.Graphics.IonEngine
{
    /// <summary>
    /// Base class for SDL Components
    /// </summary>
    public abstract class SDLBase : IDisposable
    {
        public IntPtr BasePointer { get; protected set; }
        protected bool Disposed { get; private set; }

        /// <summary>
        /// This constructer is used when you need to initialize things before the Pointer. 
        /// If this is used, LateInit must be overridden, and return the correct pointer.
        /// </summary>
        protected SDLBase()
        {
            IntPtr basePointer = LateInit();
            if (basePointer == IntPtr.Zero)
                throw new NullReferenceException("Null pointer exception: " + SDL.SDL_GetError());
            this.BasePointer = basePointer;
        }

        /// <summary>
        /// Late initialize method, returns the correct pointer when overridden in a derived class
        /// </summary>
        /// <returns>Int Pointer to SDL resource</returns>
        public virtual IntPtr LateInit()
        {
            return IntPtr.Zero;
        }

        /// <summary>
        /// Creates a new instance of the base wrapper
        /// </summary>
        /// <param name="basePointer">The pointer to the SDL resource</param>
        public SDLBase(IntPtr basePointer)
        {
            if (basePointer == IntPtr.Zero)
                throw new NullReferenceException("Null pointer exception: " + SDL.SDL_GetError());
            this.BasePointer = basePointer;
        }

        ~SDLBase()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (!Disposed)
            {
                OnDispose();
                BasePointer = IntPtr.Zero;
                Disposed = true;
            }
        }

        /// <summary>
        /// Protected method for derived methods to dispose
        /// </summary>
        protected abstract void OnDispose();
    }
}
