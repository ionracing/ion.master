﻿using NicroWare.Lib.IonEngine.PNGParser;
using SDL2;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace Ion.Graphics.IonEngine
{
    public class SDLSurface : SDLBase
    {
        /// <summary>
        /// Creates a new SDLSurface with a pointer to the native SDLSurface object
        /// </summary>
        /// <param name="basePointer">Native Pointer</param>
        public SDLSurface(IntPtr basePointer)
            : base(basePointer)
        {

        }

        /// <summary>
        /// Loads an SDLSurface with the specefied Bitmap(.bmp) image from the path
        /// </summary>
        /// <param name="path">The path to the bitmap image(.bmp)</param>
        /// <returns>A fully loaded SDLSurface</returns>
        public static SDLSurface LoadBitmap(string path)
        {
            return new SDLSurface(SDL.SDL_LoadBMP(path));
        }

        /// <summary>
        /// Loads an SDLSurface with the specefied Protable Network Graphic(.png) image from the path
        /// </summary>
        /// <param name="path">The path to the Protable Network Graphic(.png)</param>
        /// <returns>A fully loaded SDLSurface</returns>
        public static SDLSurface LoadPNG(string path)
        {
            /*Bitmap img = (Bitmap)Bitmap.FromFile(path);
            BitmapData bitLock = img.LockBits(new Rectangle(0, 0, img.Width, img.Height), System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            //byte[] bytes = new byte[bitLock.Height * bitLock.Stride];
            //Marshal.Copy(bitLock.Scan0, bytes, 0, bytes.Length);
            IntPtr temp = SDL2.SDL.SDL_CreateRGBSurfaceFrom(bitLock.Scan0, img.Width, img.Height, 32, img.Width * 4, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
            img.UnlockBits(bitLock);
            return new SDLSurface(temp);*/

            return FromPNGFile(PngFile.ReadFile(path));
        }

        /// <summary>
        /// Wraps an intPtr inside an SDLSurface object, this means that the intPtr have to point at a SDLSurface object
        /// </summary>
        /// <param name="intPtr">Int Pointer to SDLSurface object</param>
        /// <returns>SDLSurface wrapper class</returns>
        public static SDLSurface FromPointer(IntPtr intPtr)
        {
            return new SDLSurface(intPtr);
        }

        public static SDLSurface FromPNGFile(PngFile file)
        {
            byte[] bytes = file.GenerateBytes();
            IntPtr temp = Marshal.AllocHGlobal(bytes.Length);
            Marshal.Copy(bytes, 0, temp, bytes.Length);
            IntPtr temp2 = SDL2.SDL.SDL_CreateRGBSurfaceFrom(temp, (int)file.IHDRHeader.Width, (int)file.IHDRHeader.Height, 32, (int)file.IHDRHeader.Width * 4, 0x000000FF, 0x0000FF00, 0x00FF0000, 0xFF000000);
            //Marshal.FreeHGlobal(temp);
            return new SDLSurface(temp2);
        }

        protected override void OnDispose()
        {
            SDL.SDL_FreeSurface(BasePointer);
        }
    }
}