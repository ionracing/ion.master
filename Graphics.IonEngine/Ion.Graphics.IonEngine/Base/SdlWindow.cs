﻿using SDL2;
using System;

namespace Ion.Graphics.IonEngine
{
    public class SDLWindow : SDLBase
    {
        /// <summary>
        /// Used by SDLRenderWindow to be able for late initialize
        /// </summary>
        protected SDLWindow()
            : base()
        {

        }

        /// <summary>
        /// Creates a new SDLWindow from pointer
        /// </summary>
        /// <param name="basePointer">Pointer to SDLWindow</param>
        public SDLWindow(IntPtr basePointer)
            : base(basePointer)
        {

        }

        /// <summary>
        /// Creates a new SDLWindow from window
        /// </summary>
        /// <param name="window">The window to wrap around</param>
        public SDLWindow(SDLWindow window)
            :this(window.BasePointer)
        {

        }

        public static SDLWindow Create(string title, int x, int y, int width, int height)
        {
            return Create(title, x, y, width, height, SDL.SDL_WindowFlags.SDL_WINDOW_SHOWN);
        }

        public static SDLWindow Create(string title, int x, int y, int width, int height, SDL.SDL_WindowFlags flags)
        {
            return new SDLWindow(CreatePointer(title, x, y, width, height, flags));
        }

        protected static IntPtr CreatePointer(string title, int x, int y, int width, int height)
        {
            
            return SDL.SDL_CreateWindow(title, x, y, width, height, SDL.SDL_WindowFlags.SDL_WINDOW_SHOWN);
        }

        protected static IntPtr CreatePointer(string title, int x, int y, int width, int height, SDL.SDL_WindowFlags flags)
        {
            return SDL.SDL_CreateWindow(title, x, y, width, height, flags);
        }

        public static void InitSdl()
        {
            //SDL.SDL_Init(SDL.SDL_INIT_VIDEO).ThrowIfError();
            SDL.SDL_Init(SDL.SDL_INIT_EVERYTHING).ThrowIfError();

            int num = 0;
            SDL.SDL_VideoInit(SDL.SDL_GetVideoDriver(num)).ThrowIfError();
        }

        public static void QuitSdl()
        {
            SDL.SDL_Quit();
        }


        protected override void OnDispose()
        {
            SDL.SDL_DestroyWindow(BasePointer);
        }
    }
}