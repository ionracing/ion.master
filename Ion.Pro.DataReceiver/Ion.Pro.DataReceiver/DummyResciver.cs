﻿using NicroWare.Pro.RPiSPITest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.DataReceiver
{
    class DummyResciver
    {
        NRFRadio baseRadio = new NRFRadio();
        byte[] temp = new byte[32];
        int counter = 0;

        public DummyResciver()
        {
            baseRadio.Begin();
            baseRadio.SetAutoAck(false);
            baseRadio.SetPALevel(RF24PaDbm.RF24_PA_MAX);
            baseRadio.SetDataRate(RF24Datarate.RF24_2MBPS);
            baseRadio.OpenReadingPipe(0, "00001");
            //baseRadio.SetRetries(1, 1);
            baseRadio.StartListening();
            while (true)
            {
                while (!baseRadio.Available())
                    System.Threading.Thread.Sleep(1);
                baseRadio.Read(temp, (byte)temp.Length);
                if (temp[25] == 0x69)
                {
                    counter++;
                }
                if (counter%500 == 0)
                {
                    Console.WriteLine(counter);
                }


            }
            
        }
    }
}
