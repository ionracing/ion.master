﻿using Ion.Data.Networking;
using Ion.Data.Networking.Logging;
using Ion.Data.Networking.Manager;
using Ion.Data.Networking.NewStack;
using NicroWare.Pro.RPiSPITest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ion.Pro.DataReceiver
{
    class Program
    {
        static void Main(string[] args)
        {

            //DummyResciver Reciver = new DummyResciver();

            //return;
            Console.Title = "Data receiver";
            LogManager manager = new LogManager();
            manager.CreateLogger("SYSTEM");
            manager.OnVerbose += Manager_OnVerbose;
            //Data.Networking.NewStack.NetworkManager.LoggReceived = true;
            Data.Networking.NewStack.NetworkManager.DefaultLogger = manager.CreateLogger("NETWORMMANAGER");


            UDPDataLink dataLink = new UDPDataLink(10111);
            
            dataLink.Connect(10110);
            Ion.Data.Networking.NewStack.NetworkManager.RegisterInterface(6, dataLink);

            //LoopbackDataLink loopback = new LoopbackDataLink();

            //NetworkManager.RegisterInterface(7, loopback);

            NRFDataLink link = null;
            try
            {
                link = new NRFDataLink();
                Data.Networking.NewStack.NetworkManager.RegisterInterface(5, link);
                Console.WriteLine("NRF available");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            IdpClient client = new IdpClient(151) { PackageMode = WrapperMode.CompressedMode };
            client.Connect(150);
            LocalNetworkClient sender = new LocalNetworkClient(NetPorts.ScreenRecive);
            sender.Connect(IPAddress.Parse("10.0.0.1"), NetPorts.ScreenSend);
            int counter = 0;
            Console.WriteLine("Starting receinving");
            //Thread t = new Thread(TestThread);
            //t.Start();
            while (true)
            {
                DataWrapper[] wrappers = client.ReadDataWrappers(false);
                sender.SendDataWrappers(wrappers, false);
                if (true || counter % 1000 == 0)
                {
                    Console.WriteLine($"Received {wrappers.Length} wrappers count: {counter}");
                }
                counter++;
            }
        }

        public static void TestThread()
        {
            IdpClient client = new IdpClient() { PackageMode = WrapperMode.CompressedMode, DefaultInterface = 7 };
            client.Connect(151);

            short value = 0;
            while (true)
            {
                DataWrapper[] dws = new[] { new DataWrapper() { SensorID = 0, Value = value & 0x0FFF } };
                client.SendDataWrappers(dws, false);
                value++;
                Thread.Sleep(10);

            }
        }

        private static void Manager_OnVerbose(object sender, LogWriteEventArgs e)
        {
            Console.WriteLine($"{e.Entry.Time} {e.Entry.Sender} {e.Entry.Level} {e.Entry.Value}");
            if (e.Entry is ExceptionEntry)
            {
                Console.WriteLine((e.Entry as ExceptionEntry).Exception);
            }
        }
    }
}
