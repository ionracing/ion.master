﻿using Ion.Data.Networking.Logging;
using Ion.Data.Networking.Manager;
using Ion.Data.Sensor;
using Ion.Graphics.IonEngine;
using Ion.Graphics.IonEngine.Drawing;
using Ion.Graphics.IonEngine.Events;
using Ion.Graphics.IonEngine.Font;
using Ion.Graphics.IonEngine.Gui;
using NicroWare.Lib.IonEngine.Gui;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.PitGui
{
    class MainWindow : GuiWindow
    {
        SDLRenderer mainRenderer;
        KeyboardState prevKey;
        KeyboardState curKey;
        public SDLFont numberFont;
        public SDLFont digiFont;
        int maxSpeed = 200;
        ILogManager mainLogger;
        ILogger pitGuiLogger;




#pragma warning disable

        #region Page1
        NewInfoMeter newinfometer_bat;
        NewInfoMeter newinfometer_volt;
        NewInfoMeter newinfometer_tempBat;
        NewInfoMeter newinfometer_tempCool;
        NewInfoMeter[] newinfometers;
        NewSpeedometer newSpeedometer;
        Label label_Speed;

        TextBlock textBox_infoBlock;
        Image image_bat;
        Image image_bat12;
        Image image_coolTemp;
        Image image_batTemp;
        Image image_warningBox;
        Image image_startupWarning;
        Image image_breakWarning;
        Image image_temperature;
        Image image_pedalWarning;
        Image image_engineWarning;
        Image[] warningLights;

        GraphDisplay graphDisplay_speed;
        GraphDisplay graphDisplay_lowcell;
        GraphDisplay graphDisplay_soc;
        GraphDisplay graphDisplay_tempbat;
        GraphDisplay graphDisplay_tempcool;
        GraphDisplay graphDisplay_current;

        Label label_ValueSoc;
        Label label_ValueLowCell;
        Label label_ValueCurrent;
        Label label_ValueTempBat;
        Label label_ValueTempCool;
        Label label_ValueSOC12V;
        #endregion

#pragma warning restore

        public MainWindow()
        {

            mainRenderer = SDLRenderer.Create(this);
            mainRenderer.DrawColor = SDLColor.DarkGray;

            mainLogger = new LogManagerClient();
            //mainLogger.AddEventListner(MainWindow_OnVerbose, LogLevel.Error);
            pitGuiLogger = mainLogger.CreateLogger("PITGUI");

            FileInfo fontFile = new FileInfo("Content/unispace.ttf");
            FileInfo font2File = new FileInfo("Content/SFDigital.ttf");
            FileInfo font3File = new FileInfo("Content/Catamaran-Light.ttf");
            Global.Font = SDLFont.LoadFont(fontFile.FullName, 50);
            Global.SideScroll = true;
            digiFont = SDLFont.LoadFont(font2File.FullName, 150);
            numberFont = SDLFont.LoadFont(font3File.FullName, 150);
            InitializeComponents(mainRenderer);
            DataReceiver.Initialize(mainLogger);
            FrameRate = 60;
            FrameLock = true;
            ShowCursor = true;

            newinfometer_bat.MaxValue = 100;
            newinfometer_tempBat.MaxValue = 100;
            newinfometer_tempCool.MaxValue = 100;
            newinfometer_volt.MaxValue = 100;
            
            graphDisplay_soc.DotSpan = 1;
            graphDisplay_speed.DotSpan = 1;
            graphDisplay_tempbat.DotSpan = 1;
            graphDisplay_tempcool.DotSpan = 1;

            #region OldSensorValueUpdateCode
            /*while (File.Exists(logDir + counter.ToString("000") + "_usart_data.log"))
                counter++;

            if (counter > 0)
            {
                graphDisplay_speed.SuspendDraw = true;
                graphDisplay_soc.SuspendDraw = true;
                graphDisplay_tempcool.SuspendDraw = true;
                graphDisplay_tempbat.SuspendDraw = true;
                counter--;
                fileClient = new FileNetworkClient(logDir + counter.ToString("000") + "_usart_data.log") { PackageMode= WrapperMode.NormalMode };
                DataWrapper[] all = fileClient.ReadDataWrappers(true);

                int lastSpeed = 0;
                int lastSoc = 0;
                int lastTempbat = 0;
                int lastTempcool = 0;

                foreach (DataWrapper dw in all)
                {
                    switch (dw.SensorID)
                    {
                        case 0:
                            lastSpeed = dw.Value;
                            //plotDisplay_test.Add(new SDLDPoint(dw.TimeStamp, dw.Value));
                            speedPlot.Add(new SDLDPoint(dw.TimeStamp, lastSpeed));
                            break;
                        case 1:
                            lastSoc = dw.Value;
                            socPlot.Add(new SDLDPoint(dw.TimeStamp, lastSoc));
                            break;
                        case 5:
                            lastTempbat = dw.Value;
                            tempBatPlot.Add(new SDLDPoint(dw.TimeStamp, lastTempbat));
                            break;
                        case 6:
                            lastTempcool = dw.Value;
                            tempPlot.Add(new SDLDPoint(dw.TimeStamp, lastTempcool));
                            break;
                    }

                    graphDisplay_speed.Values.Add(lastSpeed);
                    graphDisplay_soc.Values.Add(lastSoc);
                    graphDisplay_tempbat.Values.Add(lastTempbat);
                    graphDisplay_tempcool.Values.Add(lastTempcool);
                 
                }                
                graphDisplay_speed.SuspendDraw = false;
                graphDisplay_soc.SuspendDraw = false;
                graphDisplay_tempcool.SuspendDraw = false;
                graphDisplay_tempbat.SuspendDraw = false;
                graphDisplay_speed.Values.Add(lastSpeed);
                graphDisplay_soc.Values.Add(lastSoc);
                graphDisplay_tempbat.Values.Add(lastTempbat);
                graphDisplay_tempcool.Values.Add(lastTempcool);

            }*/
            #endregion

        }

        private void MainWindow_OnVerbose(object sender, LogWriteEventArgs e)
        {
            //listView_event.Items.Add($"[{e.Entry.Time}] ({e.Entry.Level}) {e.Entry.Sender} {e.Entry.Value}");
        }

        public void UpdateValues()
        {
            ushort[] sensors = SensorLookup.SensorIDs;
            DataWrapper? wrapper = null;

            graphDisplay_speed.SuspendDraw = true;
            graphDisplay_soc.SuspendDraw = true;
            graphDisplay_tempcool.SuspendDraw = true;
            graphDisplay_tempbat.SuspendDraw = true;

            //Advanced switch body, updates sensor values as it should
            SwitchBody<SensorLookup> switcher = SwitchBody<SensorLookup>.CreateSwitch()
                .Case(SensorLookup.GetByName(SensorLookup.SPEED), () =>
                    {
                        double value = wrapper.Value.CalculateValue();
                        newSpeedometer.Value = value;
                        graphDisplay_speed.Values.Add((int)value);
                    })
                .Case(SensorLookup.GetByName(SensorLookup.SOC), () =>
                    {
                        double value = wrapper.Value.CalculateValue();
                        newinfometer_bat.Value = value;
                        graphDisplay_soc.Values.Add((int)value);
                    })
                .Case(SensorLookup.GetByName(SensorLookup.TEMPBAT), () =>
                    {
                        double value = wrapper.Value.CalculateValue();
                        newinfometer_tempBat.Value = value;
                        graphDisplay_tempbat.Values.Add((int)value);
                    })
                .Case(SensorLookup.GetByName(SensorLookup.TEMPCOOL), () =>
                    {
                        double value = wrapper.Value.CalculateValue();
                        newinfometer_tempCool.Value = value;
                        graphDisplay_tempcool.Values.Add((int)value);
                    })
                .Case(SensorLookup.GetByName(SensorLookup.VOLTAGE12), () =>
                    {
                        double value = wrapper.Value.CalculateValue();
                        newinfometer_volt.Value = value;
                    })
                ;

            for (ushort i = 0; i < sensors.Length; i++)
            {
                //Get last wrapper
                wrapper = DataReceiver.GetData(sensors[i]);

                if (wrapper != null)
                {
                    switcher.Switch(SensorLookup.GetById(sensors[i])).Run();
                }

            }

            graphDisplay_speed.SuspendDraw = false;
            graphDisplay_soc.SuspendDraw = false;
            graphDisplay_tempcool.SuspendDraw = false;
            graphDisplay_tempbat.SuspendDraw = false;
        }

        public override void Update(GameTime gameTime)
        {

            UpdateValues();

            label_Speed.TextColor = new SDLColor(200, 200, 200);

            if (newSpeedometer.Persent * maxSpeed < 9.5)
                label_Speed.Text = (newSpeedometer.Persent * maxSpeed).ToString("  0");
            else if (newSpeedometer.Persent * maxSpeed < 99.5)
                label_Speed.Text = (newSpeedometer.Persent * maxSpeed).ToString(" 00");
            else
                label_Speed.Text = (newSpeedometer.Persent * maxSpeed).ToString("000");
            scroller.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            mainRenderer.Clear();
            scroller.Draw(mainRenderer, gameTime);
            mainRenderer.Present();
        }
    }
}
