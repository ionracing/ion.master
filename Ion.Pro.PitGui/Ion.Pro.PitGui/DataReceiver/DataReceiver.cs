﻿using Ion.Data.Networking;
using Ion.Data.Networking.Logging;
using Ion.Data.Networking.Manager;
using Ion.Data.Sensor;
using Ion.Graphics.IonEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.PitGui
{

    public class DataReceiver
    {
        public DataReceiver()
        {

        }
        //static LogManager manager;
        static ILogger receiveLogger;

        public static bool DriveToSide = false;
        public static DateTime MessageReceived = DateTime.Now;
        //public static CanNetworkClient canClient;

        public static DataClient carClient;
        public static DataClient telemetryClient;

        public static bool running = false;


        static Dictionary<SensorLookup, DataWrapper> lastesTable = new Dictionary<SensorLookup, DataWrapper>();

        public static DataWrapper? GetData(ushort sensorID)
        {
            return GetData(SensorLookup.GetById(sensorID));
        }

        public static DataWrapper? GetData(string name)
        {
            return GetData(SensorLookup.GetByName(name));
        }

        public static DataWrapper? GetData(SensorLookup lookup)
        {
            if (lookup == null)
                return null;
            if (lastesTable.ContainsKey(lookup))
                return lastesTable[lookup];
            return null;
        }

        public static void Initialize()
        {
            LogManager manager = new LogManager();
            manager.OnVerbose += (object sender, LogWriteEventArgs e) => Console.WriteLine("(" + e.Entry.Time.ToString("HH:mm:ss") + ")[" + e.Entry.Sender + "]\t" + e.Entry.Value);
            Initialize(manager);
        }

        public static void Initialize(ILogManager manager)
        {
            receiveLogger = manager.CreateLogger("RECEIVER");
            try
            {
                receiveLogger.WriteLine("Started to listen to local interface", "INFO", 2);
                carClient = new LocalNetworkClient(NetPorts.ScreenSend) { PackageMode= WrapperMode.NormalMode };

                Task.Run(new Action(ReceiveLoop));
            }
            catch (Exception e)
            {
                receiveLogger.WriteException("Error opening connection", e, 4);
            }
        }

        public static bool Contains(string[] devices, string dev)
        {
            foreach (string s in devices)
            {
                if (s == dev)
                    return true;
            }
            return false;
        }

        public static void ReceiveLoop()
        {
            receiveLogger.WriteLine("Started car receive loop", "INFO", 1);
            int packagesReceived = 0;
            try
            {
                while (true)
                {

                    DataWrapper[] data = carClient.ReadDataWrappers(false);
                    packagesReceived++;
                    //usartLogWriter.Write(carClient.LastRecivedBytes);
                    AddValue(data);

                    //receiveLogger.WriteLine("Recved message, bytes left to read: " + piPort.BytesToRead + " package count: " + packagesReceived, "INFO", 1);
                }
            }
            catch (Exception e)
            {
                receiveLogger.WriteException("There has been an error in the recive loop", e, 4);
            }
        }

        private static void AddValue(DataWrapper[] wrapper)
        {
            foreach (DataWrapper dw in wrapper)
            {
                AddValue(dw);
            }
        }

        private static void AddValue(DataWrapper wrapper)
        {
            wrapper.TimeStamp = (int)GameTime.GlobalTime.TotalElapsed.TotalMilliseconds;

            SensorLookup lookup = SensorLookup.GetById(wrapper.SensorID);
            if (lookup != null)
            {
                if (lastesTable.ContainsKey(lookup))
                    lastesTable[lookup] = wrapper;
                else
                    lastesTable.Add(lookup, wrapper);
            }
        }
    }



    public class OBJWrap<T>
    {
        public T obj { get; set; }

        public OBJWrap(T t)
        {
            this.obj = t;
        }
    }
}
