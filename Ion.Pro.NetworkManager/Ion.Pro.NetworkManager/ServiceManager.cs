﻿using Ion.Data.Networking.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.NetworkManager
{
    public interface IService
    {
        Service Container { get; set; }

        void Initialize(Service container);
        void Start();
        void Stop(); // TODO: Is this nessesary
    }

    public enum ServiceState
    {
        NotInitialized,
        Initialized,
        Stopped,
        Running,
        Crashed,
    }

    public class ServiceManager
    {
        public ILogManager LogManager { get; set; }
        public List<Service> AllServices { get; private set; } = new List<Service>();

        public ServiceManager()
        {
            LogManager = new LogManager();
        }

        public ServiceManager(ILogManager logManager)
        {
            LogManager = logManager;
        }

        public T GetServiceOfType<T>()
            where T : IService
        {
            foreach (Service s in AllServices)
            {
                if (s.ServiceObject is T)
                {
                    return (T)s.ServiceObject;
                }
            }
            return default(T);
        }

        public void Add(IService service)
        {
            AllServices.Add(new Service(service, LogManager.CreateLogger(service.GetType().Name.ToUpper()), this));
        }

        public void Initialize()
        {
            AllServices.ForEach(x => x.Initialize());
        }

        public void Start()
        {
            AllServices.ForEach(x => x.Start());
        }

        public void WaitForExit()
        {
            bool IsAllRunning = true;
            do
            {
                System.Threading.Thread.Sleep(1000);
                IsAllRunning = false;
                foreach (Service s in AllServices)
                {
                    IsAllRunning |= s.State == ServiceState.Running;
                }
            }
            while (IsAllRunning);

        }
    }
}
