﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.NetworkManager
{
    public class GlobalTime
    {
        public static DateTime StartTime { get; private set; } = DateTime.Now;
        public static TimeSpan ElapsedTime => DateTime.Now - StartTime;
    }
}
