﻿using Ion.Data.Networking.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.NetworkManager
{
    public sealed class Service
    {
        public IService ServiceObject { get; private set; }
        public ILogger Logger { get; private set; }
        public ServiceManager Manager { get; private set; }
        public string Name { get; private set; }

        public ServiceState State { get; private set; } = ServiceState.NotInitialized;

        public Service(IService serviceObject, ILogger logger, ServiceManager manager)
        {
            ServiceObject = serviceObject;
            Name = serviceObject.GetType().Name;
            ServiceObject.Container = this;
            Logger = logger;
            Manager = manager;
        }

        private void ThrowIfNull(object o)
        {
            if (o == null)
            {
                State = ServiceState.Crashed;
                throw new NullReferenceException();
            }
        }

        private void FailFast(Exception e, string text)
        {
            State = ServiceState.Crashed;
            Logger.WriteException(text, e, 4);
        }

        public void Initialize()
        {
            ThrowIfNull(ServiceObject);
            try
            {
                Logger.WriteInfo("Initializing");
                ServiceObject.Initialize(this);
                State = ServiceState.Initialized;
            }
            catch (Exception e)
            {
                FailFast(e, "Crashed in initialize");
            }
        }

        public void Start()
        {
            ThrowIfNull(ServiceObject);
            try
            {
                Logger.WriteInfo("Starting");
                ServiceObject.Start();
                State = ServiceState.Running;
            }
            catch (Exception e)
            {
                FailFast(e, "Crashed in start");
            }

        }

        public void Stop()
        {
            ThrowIfNull(ServiceObject);
            try
            {
                Logger.WriteInfo("Stopping");
                ServiceObject.Stop();
                State = ServiceState.Stopped;
            }
            catch (Exception e)
            {
                FailFast(e, "Crashed in stop");
            }
        }

        public void Crash(IService service, string message)
        {
            if (service == ServiceObject)
            {
                FailFast(new Exception(message), message);
            }
        }
    }
}
