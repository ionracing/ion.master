﻿#if RELEASE || DEBUG
#define NODHCP
#endif
//#define PIMODE

using Ion.Data.Networking.Logging;
using NicroWare.Lib.NetUtility.Dhcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.NetworkManager
{
    public class DhcpService : IService
    {
        DHCPServer server;
        public Service Container { get; set; }
        ILogger logger;

        public void Initialize(Service container)
        {
#if PIMODE
            server = new DHCPServer(
                new AdvIPAddress(new byte[] { 10, 0, 0, 3 }, 24)
                , new List<System.Net.IPAddress>(new[] { new AdvIPAddress(new byte[] { 10, 0, 0, 1 }) })
                , 1
                )
            { SingleAddressMode = true };
#else
            server = new DHCPServer(new AdvIPAddress(new byte[] { 10, 0, 0, 1 }, 24)
                , new List<System.Net.IPAddress>(new[] { new AdvIPAddress(new byte[] { 10, 0, 0, 1 }) })
                , 3
                );
#endif
            logger = container.Logger;
        }

        public void Start()
        {
            //Temporary stopping the DHCP server from running
            //Container.Stop();
#if NODHCP
            logger.WriteInfo("Currently stopping DHCP Server from running, because of not on raspberry PI");
#else
            server.Start();
#endif
        }

        public void Stop()
        {

        }
    }
}
