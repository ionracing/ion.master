﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.NetworkManager.Services.Models
{
    public class RawDataModel
    {
        public string ID { get; set; }
        public string Data { get; set; }
        public string Data0 { get; set; }
        public string Data1 { get; set; }
        public string Data2 { get; set; }
        public string Data3 { get; set; }
        public string TimeStamp { get; set; }
        public string TimeDelta { get; internal set; }
        public string TimeAvg { get; set; }
        public string Count { get; set; }
        
    }

    public class RawDataContainer
    {
        public List<RawDataModel> Data { get; set; } = new List<RawDataModel>();
    }
}
