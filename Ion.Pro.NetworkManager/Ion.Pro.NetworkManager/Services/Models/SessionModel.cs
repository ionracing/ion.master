﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.NetworkManager.Services.Models
{
    public class SessionModel
    {
        public string CurrentSession { get; set; }
        public SessionEntry[] Data { get; set; }
    }

    public class SessionEntry
    {
        public string SessionID { get; set; }
    }

}
