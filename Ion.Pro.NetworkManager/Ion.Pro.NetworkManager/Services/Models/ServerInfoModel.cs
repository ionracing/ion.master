﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.NetworkManager.Services.Models
{
    public class ServerInfo
    {
        public string Version { get; set; }
        public string[] Apps { get; set; }
    }
}
