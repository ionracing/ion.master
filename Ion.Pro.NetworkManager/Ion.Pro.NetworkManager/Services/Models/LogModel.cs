﻿using Ion.Data.Networking.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.NetworkManager.Services.Models
{
    public class StringLogEntry
    {
        public string ID { get; set; }
        public string Category { get; set; }
        public string Sender { get; set; }
        public string Level { get; set; }
        public string Time { get; set; }
        public virtual string Value { get; set; }

        public static StringLogEntry FromLogEntry(LogEntry entry)
        {
            StringLogEntry temp = new StringLogEntry()
            {
                ID = entry.ID.ToString(),
                Category = entry.Category,
                Sender = entry.Sender,
                Level = entry.Level.ToString(),
                Time = entry.Time.ToString(),
                Value = entry.Value
            };
            return temp;
        }
    }

    public class LogData
    {
        public StringLogEntry[] Data { get; set; }
        public LogData(LogEntry[] data)
        {
            this.Data = new StringLogEntry[data.Length];
            for (int i = 0; i < this.Data.Length; i++)
            {
                this.Data[i] = StringLogEntry.FromLogEntry(data[i]);
            }
        }
    }
}
