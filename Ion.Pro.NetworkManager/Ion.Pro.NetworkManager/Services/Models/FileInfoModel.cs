﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.NetworkManager.Services.Models
{
    public class NInfo
    {
        public string Name { get; set; }
        public string DateMod { get; set; }
        public string Type { get; set; }
        public string Size { get; set; }
    }

    public class DirContainer
    {
        public string CurPath { get; set; }
        public NInfo[] DirectoryInfo { get; set; }
        public NInfo[] FileInfo { get; set; }
    }
}
