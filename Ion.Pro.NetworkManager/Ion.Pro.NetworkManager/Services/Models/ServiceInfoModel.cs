﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.NetworkManager.Services.Models
{
    public class ServiceInfoModel
    {
        public ServiceInfoEntry[] Data { get; set; }
    }

    public class ServiceInfoEntry
    {
        public string Name { get; set; }
        public string State { get; set; }
    }
}
