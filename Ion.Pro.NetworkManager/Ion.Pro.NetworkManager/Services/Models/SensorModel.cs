﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.NetworkManager.Services.Models
{
    public class DataWrap
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class SensorDataContainer
    {
        public string PackageCount { get; set; }
        public DataWrap[] Data { get; set; }
    }
}
