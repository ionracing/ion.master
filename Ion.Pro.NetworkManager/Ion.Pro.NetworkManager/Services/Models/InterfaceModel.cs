﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.NetworkManager.Services.Models
{
    class InterfaceModel
    {
        public string Name { get; set; }
        public int ID { get; set; }
        public long SentBytes { get; set; }
        public long ReceivedBytes { get; set; }
    }

    class NetManModel
    {
        public List<InterfaceModel> AllInterfaces { get; set; } = new List<InterfaceModel>();
    }
}
