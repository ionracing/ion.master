﻿using Ion.Data.Networking.CoreCom;
using Ion.Data.Networking.Html.Controllers;
using Ion.Data.Networking.Logging;
using Ion.Data.Networking.Manager;
using Ion.Data.Sensor;
using Ion.Pro.NetworkManager.Services.Models;
using NicroWare.Lib.NetUtility.Html;
using NicroWare.Pro.DmxControl.JSON;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.NetworkManager.Services
{
    public class WebController : Controller
    {
        int counter = 0;
        public override string DefaultFile { get; } = "index.html";

        public ActionResult Index(string[] path)
        {
            return Overview(path);
        }

        public ActionResult Overview(string[] path)
        {
            HtmlBuilder builder = new HtmlBuilder();
            HtmlNode div = new HtmlNode("div");
            builder.BaseNode = div;
            HtmlTable table = new HtmlTable();
            table.TableParams = "class=\"table\"";
            table.AddHeader("Name", "Version");
            table.AddRow("NetworkManager", Program.Version);
            if (Program.CoreOnline)
            {
                CommandSender sender = new CommandSender();
                string version = sender.SendCommand("Version", new string[0]);
                sender.Close();
                table.AddRow("Core", version);
            }
            else
            {
                table.AddRow("Core", "Offline");
            }
            table.AddRow("Web Gui", "Unknown");
            div.AddNodes(table.ToDom());
            div.AddNodes(new HtmlNode("span", new TextNode("You have visited: " + (++counter).ToString())));
            return View(builder.ToString(), new Dictionary<string, string>() { ["Title"] = "Overview" });
            //return View("<div style='text-align: left;'>NetworkManager version: " + Program.Version + "<br />Core offline</div><span>You have visited: " + counter++ + "</span>", "Overview", true);
        }

        public ActionResult Log(string[] path)
        {
            HtmlBuilder builder = new HtmlBuilder();
            int startIndex;
            if (!(path.Length > 2 && int.TryParse(path[2], out startIndex)))
            {
                startIndex = 2;
            }
            HtmlTable table = new HtmlTable();
            table.AddHeader("Time", "Sender", "Level", "Info");
            table.TableParams = "class=\"table\"";
            table.AddHeaderParams("style=\"180px\"", "style=\"180px\"", "style=\"180px\"", "style=\"80px\"");
            foreach (LogEntry entry in Program.TestManager.ListEntries())
            {
                if (entry.Level >= startIndex)
                    table.AddRow(entry.Time.ToString(), entry.Sender.ToString(), entry.Level.ToString(), entry.Value.Replace("\n", "<br />"));
            }
            builder.BaseNode = table.ToDom();
            return View("<a class=\"button\" href=\"/web/log/0\">All</a><a class=\"button\" href=\"/web/log/1\">Info</a><a class=\"button\" href=\"/web/log/3\">Errors</a><br />" + builder.ToString(), new Dictionary<string, string>() { ["Title"] = "Log" });
        }

        public ActionResult File(string[] path)
        {
            string dirPath = "/";

            int preRemove = 1;
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                preRemove = 3;
            for (int i = 2; i < path.Length; i++)
            {
                dirPath += path[i] + "/";
            }
            DirectoryInfo test = new DirectoryInfo(dirPath);
            if (!test.Exists)
            {
                FileInfo fileTest = new FileInfo(dirPath.Remove(dirPath.Length - 1, 1));
                if (fileTest.Exists)
                {
                    return SendFile(fileTest.FullName);
                }
            }
            HtmlTable dirTable = new HtmlTable();
            dirTable.TableParams = "class=\"table\"";
            dirTable.AddHeader("Name", "Date modified", "Type", "Size");
            dirTable.AddRow("<a href='/web/file/" + (test.Parent?.FullName.Remove(0, preRemove).Replace('\\', '/') ?? "") + "'>..</a>", "", "Up", "");
            foreach (DirectoryInfo di in test.GetDirectories())
            {
                dirTable.AddRow("<a href='/web/file/" + di.FullName.Remove(0, preRemove).Replace('\\', '/') + "'>" + di.Name + "</a>", di.LastWriteTime.ToString(), "File folder", "");
            }
            foreach (FileInfo di in test.GetFiles())
            {
                dirTable.AddRow("<a href='/web/file/" + di.FullName.Remove(0, preRemove).Replace('\\', '/') + "'>" + di.Name + "</a>", di.LastWriteTime.ToString(), di.Extension + " file", (di.Length / 1024) + " KB");
            }
            HtmlBuilder dirBuilder = new HtmlBuilder();
            dirBuilder.BaseNode = dirTable.ToDom();
            return View("<h3>Explorer</h3><br /><span>Current dir: " + test.FullName + "</span><br />" + dirBuilder.ToString(), new Dictionary<string, string>() { ["Title"] = "Explorer" });
        }

        public ActionResult Diagnostic(string[] path)
        {
            Process[] allProcesses = Process.GetProcesses();
            HtmlTable procTable = new HtmlTable();
            procTable.TableParams = "class=\"table\"";
            procTable.AddHeader("Id", "Process Name", "Main Window Title", "State", "Total Time");
            foreach (Process p in allProcesses)
            {
                try
                {
                    procTable.AddRow(p.Id.ToString(), p.ProcessName, p.MainWindowTitle, p.Responding ? "running" : "crashed", p.TotalProcessorTime.ToString());
                }
                catch
                { }
            }
            HtmlBuilder procTemp = new HtmlBuilder();
            procTemp.BaseNode = procTable.ToDom();
            return View("<h3>Diagnostic</h3> <br/> If you see this page, the web server is running :P </br>" + procTemp.ToString(false), new Dictionary<string, string>() { ["Title"] = "Diagnostic" });
        }

        public ActionResult Update(string[] path)
        {
            if (Program.CoreOnline)
                return View(System.IO.File.ReadAllText(WebServer.HtmlBase + "update.html"), new Dictionary<string, string>() { ["Title"] = "Update" });
            else
                return View("Core not available", new Dictionary<string, string>() { ["Title"] = "Update" });
        }

        public ActionResult Services(string[] path)
        {
            HtmlTable table = new HtmlTable();
            table.TableParams = "class=\"table\"";
            table.AddHeader("Name", "Status");
            foreach (Service s in Server.Container.Manager.AllServices)
            {
                table.AddRow(s.ServiceObject.GetType().Name, s.State.ToString());
            }
            return View((new HtmlBuilder() { BaseNode = table.ToDom() }).ToString(), new Dictionary<string, string>() { ["Title"] = "Network manager services" });
        }

        public ActionResult Data(string[] path)
        {
            HtmlTable table = new HtmlTable();
            table.TableParams = "class=\"table\"";
            table.AddHeader("Name", "Latest");
            KeyValuePair<SensorLookup, DataWrapper>[] latest = SensorDataStore.Latest.ToArray();
            foreach (KeyValuePair<SensorLookup, DataWrapper> w in latest)
            {
                table.AddRow(w.Key.DisplayName, w.Key.ConvertValue(w.Value.Value).ToString());
            }
            int packageCount = Server.Container.Manager.GetServiceOfType<DataReceiver>()?.PackageCount ?? -1;
            return View("Data packages: " + packageCount + " <br />" + (new HtmlBuilder() { BaseNode = table.ToDom() }).ToString(), new Dictionary<string, string>() { ["Title"] = "Sensor Data" });
        }

        public ActionResult RawData(string[] path)
        {
            HtmlTable table = new HtmlTable();
            table.TableParams = "class=\"table\"";
            table.AddHeader("ID", "Data", "Data[0]", "Data[1]", "Data[2]", "Data[3]", "Time stamp", "Time delta (ms)", "Time avg (ms)", "Count");
            foreach (PackageInfo pi in SensorDataStore.PackageInfos.Values)
            {
                byte[] values = BitConverter.GetBytes(pi.LastValue);
                table.AddRow("0x" + pi.ID.ToString("X") + " (" + pi.ID.ToString() + ")"
                    , "0x" + pi.LastValue.ToString("X") + " (" + pi.LastValue.ToString() + ")"
                    , "0x" + values[0].ToString("X") + " (" +values[0].ToString() + ")"
                    , "0x" + values[1].ToString("X") + " (" + values[1].ToString() + ")"
                    , "0x" + values[2].ToString("X") + " (" + values[2].ToString() + ")"
                    , "0x" + values[3].ToString("X") + " (" + values[3].ToString() + ")"
                    , pi.LastRegister.ToString()
                    , pi.LastDiff.TotalMilliseconds.ToString()
                    , pi.AvgDiff.TotalMilliseconds.ToString()
                    , pi.Count.ToString());
            }
            return View((new HtmlBuilder() { BaseNode = table.ToDom() }).ToString(), new Dictionary<string, string>() { ["Title"] = "Can Messages" });
        }
    }

    public class FileController : Controller
    {
        public override string DefaultFile { get; } = "index.html";

        public ActionResult Browse(string[] path)
        {
            string dirPath = "/";

            for (int i = 2; i < path.Length; i++)
            {
                dirPath += path[i] + "/";
            }
            DirectoryInfo test = new DirectoryInfo(dirPath);
            if (!test.Exists)
            {
                FileInfo fileTest = new FileInfo(dirPath.Remove(dirPath.Length - 1, 1));
                if (fileTest.Exists)
                {

                    return SendFile(fileTest.FullName);
                    //writer.WriteFile(fileTest.FullName);
                    //test = fileTest.Directory;
                    //return null;
                }
            }
            HtmlTable dirTable = new HtmlTable();
            dirTable.TableParams = "class=\"table\"";
            dirTable.AddHeader("Name", "Date modified", "Type", "Size", "Actions");

            dirTable.AddRow(BrowseLink(test.Parent == null ? "" : FormatPath(test.Parent.FullName), ".."), "", "Up", "", "");
            foreach (DirectoryInfo di in test.GetDirectories())
            {
                string tempDirPath = FormatPath(di.FullName);
                dirTable.AddRow(BrowseLink(tempDirPath, di.Name), di.LastWriteTime.ToString(), "File folder", "", DeleteLink(tempDirPath));
            }
            foreach (FileInfo fi in test.GetFiles())
            {
                string tempFilePath = FormatPath(fi.FullName);
                dirTable.AddRow(DownloadLink(tempFilePath, fi.Name), fi.LastWriteTime.ToString(), fi.Extension + " file", PritifySize(fi.Length), DeleteLink(tempFilePath));
            }
            HtmlBuilder dirBuilder = new HtmlBuilder();
            dirBuilder.BaseNode = dirTable.ToDom();
            return View("<span>Current dir: " + test.FullName + "</span><br />" + dirBuilder.ToString(), new Dictionary<string, string> { ["Title"] = "Explorer" });
            //return input.Replace("@Content", "<h3>Explorer</h3><br /><span>Current dir: " + test.FullName + "</span><br />" + dirBuilder.ToString());
        }

        public ActionResult Down(string[] path)
        {
            string dirPath = "/";

            for (int i = 2; i < path.Length; i++)
            {
                dirPath += path[i] + "/";
            }

            FileInfo fileTest = new FileInfo(dirPath.Remove(dirPath.Length - 1, 1));
            if (fileTest.Exists)
            {
                return SendFile(fileTest.FullName);
                //writer.WriteFile(fileTest.FullName);
                //test = fileTest.Directory;
                //return null;
            }
            return View("<h3>404 File not found</h3>");
            //return input.Replace("@Content", "<h3>404 File not found</h3>");
        }

        public ActionResult Delete(string[] path)
        {
            return View("<h3>Not yet impementet</h3>");
            //return input.Replace("@Content", "<h3>Not yet impementet</h3>");
        }

        private string FormatPath(string path)
        {
            int preRemove = 1;
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                preRemove = 3;
            return path.Remove(0, preRemove).Replace('\\', '/');
        }

        private string BrowseLink(string path, string name)
        {
            return ActionLink("browse", path, name);
        }

        private string DownloadLink(string path)
        {
            return DownloadLink(path, "Download");
        }

        private string DownloadLink(string path, string name)
        {
            return ActionLink("down", path, name);
        }

        private string DeleteLink(string path)
        {
            return ActionLink("delete", path, "Delete");
        }

        private string ActionLink(string action, string path, string name)
        {
            return string.Format("<a href='/file/{0}/{1}' >{2}</a>", action, path, name);
        }

        private string PritifySize(long size)
        {
            int counter = 0;
            while (size > 1024 && counter < 5)
            {
                size /= 1024;
                counter++;
            }
            if (counter > 0)
                size += 1;
            string prefix = "B";
            switch (counter)
            {
                case 1:
                    prefix = "KB";
                    break;
                case 2:
                    prefix = "MB";
                    break;
                case 3:
                    prefix = "GB";
                    break;
                case 4:
                    prefix = "TB";
                    break;
            }

            return size.ToString() + " " + prefix;
        }
    }

    public class DesktopController : Controller
    {
        public override string DefaultFile { get; } = "index.html";

        public ActionResult Index(string[] path)
        {
            return View("");
            //return File.ReadAllText(WebServer.HtmlBase + "Desktop/index.html");
        }

        public ActionResult Log(string[] path)
        {
            LogData temp = new LogData(Program.TestManager.ListEntries());
            JSONObject baseJ = JSONObject.Create(temp);
            return Json(baseJ);
            //return baseJ.ToJsonString();
        }

        public ActionResult ServerInfo(string[] path)
        {
            ServerInfo info = new ServerInfo() { Version = Program.Version, Apps = new string[] { "Not available" } };
            return Json(JSONObject.Create(info));
            //return JSONObject.Create(info).ToJsonString();
        }

        public ActionResult Explore(string[] path)
        {
            string dirPath = "/";


            for (int i = 2; i < path.Length; i++)
            {
                dirPath += path[i] + "/";
            }
            DirectoryInfo test = new DirectoryInfo(dirPath);
            if (!test.Exists)
            {
                FileInfo fileTest = new FileInfo(dirPath.Remove(dirPath.Length - 1, 1));
                if (fileTest.Exists)
                {
                    return SendFile(fileTest.FullName);
                    //writer.WriteFile(fileTest.FullName);
                    //test = fileTest.Directory;
                    //return null;
                }
            }
            DirectoryInfo[] alldirs = test.GetDirectories();
            FileInfo[] allFiles = test.GetFiles();
            DirContainer container = new DirContainer();
            container.DirectoryInfo = new NInfo[alldirs.Length];
            container.FileInfo = new NInfo[allFiles.Length];
            container.CurPath = test.FullName;
            for (int i = 0; i < alldirs.Length; i++)
            {
                DirectoryInfo temp = alldirs[i];
                container.DirectoryInfo[i] = new NInfo() { DateMod = temp.LastWriteTime.ToString(), Name = temp.Name, Size = "", Type = "File folder" };
            }
            for (int i = 0; i < allFiles.Length; i++)
            {
                FileInfo temp = allFiles[i];
                container.FileInfo[i] = new NInfo() { DateMod = temp.LastWriteTime.ToString(), Name = temp.Name, Size = temp.Length.ToString(), Type = temp.Extension + " file" };
            }
            return Json(JSONObject.Create(container));
            //return JSONObject.Create(container).ToJsonString();
        }

        public ActionResult Data(string[] path)
        {
            SensorDataContainer container = new SensorDataContainer();
            List<DataWrap> wraps = new List<DataWrap>();
            KeyValuePair<SensorLookup, DataWrapper>[] latest = SensorDataStore.Latest.ToArray();
            int lastHighRes = 0;
            foreach (KeyValuePair<SensorLookup, DataWrapper> w in latest)
            {
                if (w.Key.ID == 252)
                {
                    lastHighRes = w.Value.Value;
                }
                else if (w.Key.ID == 250 || w.Key.ID == 251)
                {
                    char dir = 'N';
                    int raw = w.Value.Value;
                    int deg = raw / (100 * 60);
                    int min = (raw - (deg * 100 * 60)) / (100);
                    int sec = (raw - (deg * 100 * 60) - (min * 100)) * 100;
                    if (w.Key.ID == 250)
                    {
                        sec += lastHighRes >> 12;
                        if (deg >= 90)
                        {
                            deg -= 90;
                            dir = 'S';
                        }
                    }
                    if (w.Key.ID == 251)
                    {
                        dir = 'E';
                        sec += lastHighRes & 0xFFF;
                        if (deg >= 180)
                        {
                            deg -= 180;
                            dir = 'W';
                        }
                    }
                    wraps.Add(new DataWrap() { Name = w.Key.DisplayName, Value = $"{deg} {min}.{sec}{dir}" });
                }
                else
                    wraps.Add(new DataWrap() { Name = w.Key.DisplayName, Value = w.Key.ConvertValue(w.Value.Value).ToString() });
            }

            int packageCount = Server.Container.Manager.GetServiceOfType<DataReceiver>()?.PackageCount ?? -1;
            container.PackageCount = packageCount.ToString();
            container.Data = wraps.ToArray();
            return Json(container);
        }

        public ActionResult Services(string[] path)
        {
            Service[] services = Server.Container.Manager.AllServices.ToArray();
            ServiceInfoEntry[] infos = new ServiceInfoEntry[services.Length];
            for (int i = 0; i < services.Length; i++)
            {
                infos[i] = new ServiceInfoEntry() { Name = services[i].Name, State = services[i].State.ToString() };
            }
            return Json(JSONObject.Create(new ServiceInfoModel() { Data = infos }));
            //return JSONObject.Create(new ServiceInfoModel() { Data = infos }).ToJsonString();
        }

        public ActionResult Sessions(string[] path)
        {
            if (path.Length > 2 && path[2].ToLower() == "clear")
            {
                SessionInfo.Infos.Clear();
            }
            SessionModel model = new SessionModel();
            model.CurrentSession = Session.SessionID;
            model.Data = new SessionEntry[SessionInfo.Infos.Count];
            int counter = 0;
            foreach (KeyValuePair<string, SessionInfo> info in SessionInfo.Infos)
            {
                model.Data[counter++] = new SessionEntry() { SessionID = info.Value.SessionID };
            }
            return Json(JSONObject.Create(model));
            //return JSONObject.Create(model).ToJsonString();
        }

        public ActionResult Interfaces(string[] path)
        {
            NetManModel model = new NetManModel();

            foreach (var netInterface in Ion.Data.Networking.NewStack.NetworkManager.Interfaces)
            {
                model.AllInterfaces.Add(new InterfaceModel() { ID = -1, Name = netInterface.GetType().Name, ReceivedBytes = netInterface.TotalReceived, SentBytes = netInterface.TotalSent });
            }

            return Json(model);
        }

        public ActionResult RawData(string[] path)
        {
            RawDataContainer container = new RawDataContainer();
            foreach (PackageInfo pi in SensorDataStore.PackageInfos.Values)
            {
                RawDataModel model = new RawDataModel();
                byte[] values = BitConverter.GetBytes(pi.LastValue);
                model.ID = "0x" + pi.ID.ToString("X") + " (" + pi.ID.ToString() + ")";
                model.Data = "0x" + pi.LastValue.ToString("X") + " (" + pi.LastValue.ToString() + ")";
                model.Data0 = "0x" + values[0].ToString("X") + " (" + values[0].ToString() + ")";
                model.Data1 = "0x" + values[1].ToString("X") + " (" + values[1].ToString() + ")";
                model.Data2 = "0x" + values[2].ToString("X") + " (" + values[2].ToString() + ")";
                model.Data3 = "0x" + values[3].ToString("X") + " (" + values[3].ToString() + ")";
                model.TimeStamp = pi.LastRegister.ToString();
                model.TimeDelta = pi.LastDiff.TotalMilliseconds.ToString();
                model.TimeAvg = pi.AvgDiff.TotalMilliseconds.ToString();
                model.Count = pi.Count.ToString();
                container.Data.Add(model);
            }
            return Json(container);
        }
    }
}
