﻿using Ion.Data.Networking.Logging;
using Ion.Data.Networking.Manager;
using Ion.Data.Sensor;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NS = Ion.Data.Networking.NewStack;

namespace Ion.Pro.NetworkManager
{
    public class TelemetryClient : IService
    {
        public DataClient WirelessClient { get; set; }
        public Service Container { get; set; }
        ILogger logger;
        //SerialPort xbeePort;

        public void Initialize(Service container)
        {
            this.logger = container.Logger;
        }

        public void Start()
        {
            string[] availablePorts = SerialPort.GetPortNames();
            try
            {
                if (NS.NetworkManager.IsInterfaceAvailable(6))
                {
                    NS.IdpClient client = new NS.IdpClient(150) { PackageMode = WrapperMode.CompressedMode };
                    client.Connect(151, 6);
                    WirelessClient = client;
                    Task.Run(new Action(SendLoop));
                }
                else if (NS.NetworkManager.IsInterfaceAvailable(5))
                {
                    NS.IdpClient client = new NS.IdpClient(150) { PackageMode = WrapperMode.CompressedMode };
                    client.Connect(151, 5);
                    WirelessClient = client;
                    Task.Run(new Action(SendLoop));
                }
                //Old code used with XBEE wireless transivers
                /*else if (false && Contains(availablePorts, "/dev/ttyUSB0"))
                {
                    xbeePort = new SerialPort("/dev/ttyUSB0", 19200, Parity.None, 8, StopBits.One);
                    xbeePort.Open();
                    WirelessClient = new UdpNetworkClient(xbeePort, 150) { PackageMode = WrapperMode.ShortMode };
                    (WirelessClient as UdpNetworkClient).Connect(151);
                    Task.Run(new Action(SendLoop));
                    //Task.Run(new Action(XBeeReceiveLoop));
                }*/
                else
                {
                    logger.WriteLine("Telemetry(xBee) serial port not available", "ERROR", 4);
                }
            }
            catch
            {

            }
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }

        public void SendLoop()
        {
            logger.WriteLine("Creating trasmit client", "INFO", 1);
            int counter = 0;
            int packagePerSec = 1000;
            logger.WriteLine("Startet trasmit loop", "INFO", 1);
            ushort i = 0;
            while (true)
            {
                List<DataWrapper> wrappers = new List<DataWrapper>();
                try
                {
                    //byte[] tempArray = new byte[3 * (counter % 5 == 0 ? 8 : 2)];
                    //List<DataWrapper> wrappers = new List<DataWrapper>();

                    if (true)//sendFullGps)
                    {
                        DataWrapper? degWrap = SensorDataStore.GetData(200);
                        DataWrapper? minWrap = SensorDataStore.GetData(201);
                        if (degWrap != null)
                            wrappers.Add(degWrap.Value);
                        if (minWrap != null)
                            wrappers.Add(minWrap.Value);
                        //sendFullGps = false;
                    }

                    for (i = 0; i < SensorLookup.SensorIDs.Length; i++)
                    {
                        ushort id = SensorLookup.SensorIDs[i];
                        DataWrapper? wrapper = SensorDataStore.GetData(id);
                        if (wrapper != null)
                        {
                            if (counter % packagePerSec < (SensorLookup.GetById(id).Importance * packagePerSec - 0.5))
                                wrappers.Add(wrapper.Value);
                        }
                        if (wrappers.Count == 5)
                            break;
                    }
                    i = (ushort)(i % SensorLookup.SensorIDs.Length);
                }
                catch (Exception e)
                {
                    logger.WriteException("There has been an exception: ", e, 4);
                }
                try
                {
                    if (wrappers.Count > 0)
                    {
                        //logger.WriteInfo("Started sending nr: " + counter);
                        WirelessClient.SendDataWrappers(wrappers.ToArray(), false);
                        //logger.WriteLine("Sent message nr: " + counter, "INFO", 1);
                    }
                    //System.Threading.Thread.Sleep(1000 / packagePerSec);
                }
                catch (Exception e)
                {
                    logger.WriteException("There has been an exception in the send loop", e, 4);
                }
                counter++;
            }
        }

        /*public void XBeeReceiveLoop()
        {
            logger.WriteLine("Started listening", "INFO", 1);
            UdpNetworkClient client = new UdpNetworkClient(xbeePort, 150);
            logger.WriteLine("Open com port", "INFO", 1);
            int packagesReceived = 0;
            try
            {
                while (true)
                {
                    byte[] bytes = client.Receive();
                    packagesReceived++;
                    usartLogWriter.Write(bytes);

                    if (bytes == new byte[] { 200, 0, 0 })
                    {
                        DriveToSide = true;
                        MessageReceived = DateTime.Now;
                    }

                    for (int i = 0; i < bytes.Length; i += 3)
                    {
                        DataStore.AddValue(DataWrapper.ReadData(bytes, i, true));
                    }
                }
            }
            catch (Exception e)
            {
                logger.WriteException("There has been an error in the recive loop", e, 4);
            }
        }*/

        //TODO: Move to utility library/class
        public static bool Contains(string[] devices, string dev)
        {
            foreach (string s in devices)
            {
                if (s == dev)
                    return true;
            }
            return false;
        }
    }
}
