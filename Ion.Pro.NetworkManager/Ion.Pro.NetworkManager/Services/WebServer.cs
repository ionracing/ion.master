﻿//#define OPENBROWSER
using Ion.Data.Networking;
using Ion.Data.Networking.CoreCom;
using Ion.Data.Networking.Html.Controllers;
using Ion.Data.Networking.Http;
using Ion.Data.Networking.Logging;
using Ion.Data.Networking.Manager;
using Ion.Data.Sensor;
using Ion.Pro.NetworkManager.Services;
using Ion.Pro.NetworkManager.Services.Models;
using NicroWare.Lib.NetUtility.Html;
using NicroWare.Lib.NetUtility.Http;
using NicroWare.Pro.DmxControl.JSON;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


namespace Ion.Pro.NetworkManager
{
    public class WebServer : IService
    {
        ILogger logger;
        // When switching to release the to lines under auto switch
#if DEBUG
        public const string HtmlBase = "../../HTML/"; //Html source in file structure
#else
        public const string HtmlBase = "HTML/"; //Where it should look
#endif
        public Dictionary<string, Controller> controllers = new Dictionary<string, Controller>();
        public Dictionary<string, ControllerInfo> linkers = new Dictionary<string, ControllerInfo>();

        public Service Container { get; set; }

        List<WebServerApplication> WebServerApplications { get; set; } = new List<WebServerApplication>();
        List<TcpListener> TcpListeners { get; set; } = new List<TcpListener>();

        public WebServer(int port)
            : this(IPAddress.Any, port)
        {
        }

        public WebServer(IPAddress bindAddress, int port)
        {
            NWWebServerApplication application = new NWWebServerApplication(this);
            application.AllBindings.Add(new Binding { Port = port, BindAddress = bindAddress });
            application.Path = HtmlBase;
            application.Name = "Ion Diagnostics";
            application.Linkers.Add("WEB", new ControllerInfo(typeof(WebController), this) { BasePath = HtmlBase });
            application.Linkers.Add("FILE", new ControllerInfo(typeof(FileController), this) { BasePath = HtmlBase });
            application.Linkers.Add("DESKTOP", new ControllerInfo(typeof(DesktopController), this) { BasePath = HtmlBase + "/Desktop/"});
            WebServerApplications.Add(application);

            NWWebServerApplication web2 = new NWWebServerApplication(this);
            web2.AllBindings.Add(new Binding { Port = 9967, BindAddress = bindAddress });
            web2.Path = @"C:\Users\Nicolas\Dropbox\Programing\TypeScript\NicroWare.Web.DesktopV2\NicroWare.Web.DesktopV2\";
            web2.Name = "Ion DesktopV2";
            web2.DefaultControllerName = "desktop";
            web2.Linkers.Add("DESKTOP", new ControllerInfo(typeof(DesktopController), this) { BasePath = @"C:\Users\Nicolas\Dropbox\Programing\TypeScript\NicroWare.Web.DesktopV2\NicroWare.Web.DesktopV2\" });
            WebServerApplications.Add(web2);

            NWWebServerApplication mobileTestApp = new NWWebServerApplication(this);
            mobileTestApp.AllBindings.Add(new Binding { Port = 7575, BindAddress = bindAddress });
            mobileTestApp.Path = "../../HTML-Mobile/";
            mobileTestApp.Name = "Mobile Test Application";
            mobileTestApp.Linkers.Add("HOME", new ControllerInfo(typeof(MobileController), this) { BasePath = mobileTestApp.Path });
            WebServerApplications.Add(mobileTestApp);
        }

        public void Initialize(Service container)
        {
            this.logger = container.Logger;
            foreach (WebServerApplication wsa in WebServerApplications)
            {
                wsa.Logger = logger;
            }
        }

        public void Start()
        {
            /*logger.WriteVerbose("Inside start! On Port: " + listener.LocalEndpoint.ToString());
            listener.Start();
            logger.WriteVerbose("Started listener, accepting clients");
            AcceptClient(listener, null);*/
            StartAllListeners();
#if OPENBROWSER
            Process.Start("http://127.0.0.1:" + NetPorts.WebServer.ToString());
#endif
        }

        public void StartAllListeners()
        {
            foreach (WebServerApplication wsa in WebServerApplications)
            {
                foreach (Binding b in wsa.AllBindings)
                {
                    logger.WriteInfo("Binding: " + b.BindAddress.ToString() + ":" + b.Port + " to " + wsa.Name);
                    TcpListener listner = new TcpListener(b.BindAddress, b.Port);
                    TcpListeners.Add(listner);
                    listner.Start();
                    AcceptClient(listner, wsa);
                    logger.WriteInfo("Binding complete");
#if OPENBROWSER
            Process.Start("http://127.0.0.1:" + b.Port.ToString());
#endif
                }
            }
        }

        private async void AcceptClient(TcpListener listener, WebServerApplication app)
        {
            logger.WriteVerbose("Waiting for Client!");
            TcpClient client = await listener.AcceptTcpClientAsync();

            AcceptClient(listener, app);
            if (client != null)
            {
                try
                {
                    IPEndPoint remote = (client.Client.RemoteEndPoint as IPEndPoint);
                    logger.WriteVerbose("Got client from: " + remote.ToString());
                    HandleClient(client, app);
                }
                catch (OperationCanceledException e)
                {
                    logger.WriteException("Empty client exception", new OperationCanceledException("No data was available", e), 0);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    logger.WriteException("Exception", e, 3);
                }
            }
            else
            {
                logger.WriteVerbose("Got null client");
            }
        }

        private bool HandleFiles(HttpHeaderRequest request)
        {
            foreach (HttpFileInfo files in request.AllFiles)
            {
                //TODO: Make it write to TEMP folder
                File.WriteAllBytes(files.Name, files.RawData);
                FileInfo fi = new FileInfo(files.Name);
                if (fi.Extension == ".zip")
                {
                    ZipArchive zip = new ZipArchive(fi.OpenRead());
                    ZipArchiveEntry zae = zip.GetEntry("update.conf");
                    if (zae != null)
                    {
                        if (Program.CoreOnline)
                        {
                            CommandSender sender = new CommandSender();
                            sender.SendCommand("Update", new string[] { fi.FullName });
                            sender.Close();
                            logger.WriteInfo("Started Updating");
                            return true;
                        }
                        else
                        {
                            logger.WriteInfo("Updating is not available sins the core is not running");
                        }
                    }
                }
            }
            return false;
        }

        private void HandleClient(TcpClient client, WebServerApplication app)
        {
            Stream networkStream = client.GetStream();

            HttpReader reader = new HttpReader(networkStream);
            HttpHeaderRequest header = reader.ReadMessage();

            app.HandleRequest(header, networkStream);
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }
    }

    public delegate string CtrlAction(string input, string[] path, HtmlWriter writer);

    public class ControllerInfo
    {
        public Type Type { get; private set; }
        public Dictionary<string, MethodInfo> Methods { get; private set; } = new Dictionary<string, MethodInfo>();
        WebServer server;
        public string BasePath { get; set; }

        public ControllerInfo(Type type, WebServer server)
        {
            Type = type;
            this.server = server;
            foreach (MethodInfo mi in type.GetMethods())
            {
                if (typeof(ActionResult).IsAssignableFrom(mi.ReturnType))
                    Methods.Add(mi.Name.ToUpper(), mi);
            }
        }

        public ActionResult Invoke(string method, string text, string[] path, HtmlWriter writer, ref string sessionID)
        {

            if (Methods.ContainsKey(method.ToUpper()))
            {
                Controller ctrl;
                lock (SessionInfo.Infos)
                {
                    if (sessionID != null && SessionInfo.Infos.ContainsKey(sessionID))
                    {
                        if (SessionInfo.Infos[sessionID].SessionControllers.ContainsKey(Type))
                        {
                            ctrl = SessionInfo.Infos[sessionID].SessionControllers[Type];
                        }
                        else
                        {
                            ctrl = (Controller)Activator.CreateInstance(Type);
                            SessionInfo.Infos[sessionID].SessionControllers.Add(Type, ctrl);
                        }
                    }
                    else//if (sessionID == null)
                    {
                        ctrl = (Controller)Activator.CreateInstance(Type);
                        SessionInfo info = null;
                        if (sessionID != null)
                            info = new SessionInfo(sessionID);
                        else
                            info = new SessionInfo();
                        sessionID = info.SessionID;
                        info.SessionControllers.Add(Type, ctrl);
                        SessionInfo.Infos.Add(sessionID, info);
                    }
                }
                ctrl.BasePath = BasePath;
                ctrl.Server = server;
                ctrl.Session = SessionInfo.Infos[sessionID];

                return (ActionResult)Methods[method.ToUpper()].Invoke(ctrl, new object[] { path });
                //return (string)Methods[method.ToUpper()].Invoke(ctrl, new object[] { text, path, writer });
            }
            return null;
        }
    }

    public class SessionInfo
    {
        public Dictionary<Type, Controller> SessionControllers { get; private set; } = new Dictionary<Type, Controller>();
        private string sessionID;
        public string SessionID
        {
            get
            {
                if (sessionID == null)
                    sessionID = Guid.NewGuid().ToString();
                return sessionID;
            }
        }

        public SessionInfo()
        {

        }

        public SessionInfo(string oldSessionInfo)
        {
            this.sessionID = oldSessionInfo;
        }
        public static Dictionary<string, SessionInfo> Infos = new Dictionary<string, SessionInfo>(); //HACK: Temporary test place
    }

    public class Controller
    {
        //public Dictionary<string, CtrlAction> AllActions { get; set; }
        public WebServer Server { get; set; }
        public SessionInfo Session { get; set; }
        public virtual string DefaultFile { get; }
        public virtual string BasePath { get; set; }

        public Controller()
        {
            /*AllActions = new Dictionary<string, CtrlAction>();
            Type t = GetType();
            MethodInfo[] methods = t.GetMethods();
            foreach (MethodInfo mi in methods)
            {
                Delegate d = Delegate.CreateDelegate(typeof(CtrlAction), this, mi, false);
                if (d != null)
                    AllActions.Add(mi.Name.ToUpper(), d as CtrlAction);
            }*/
        }

        protected ActionResult View(string content)
        {
            return View(DefaultFile, content);
        }

        protected ActionResult View(string content, Dictionary<string, string> other)
        {
            return View(DefaultFile, content, other);
        }

        protected ActionResult View(string path, string content)
        {
            ActionResult result = new ActionResult();
            result.MIMEType = MimeTypes.GetMimeTypeFor(".html");
            result.Data = FileManager.ReadAllText(BasePath + path).Replace("@Content", content);
            return result;
        }

        protected ActionResult View(string path, string content, Dictionary<string, string> other)
        {
            ActionResult result = new ActionResult();
            result.MIMEType = MimeTypes.GetMimeTypeFor(".html");
            string data = FileManager.ReadAllText(BasePath + path).Replace("@Content", content);
            foreach (KeyValuePair<string, string> pair in other)
            {
                data = data.Replace("@" + pair.Key, pair.Value);
            }
            result.Data = data;
            return result;
        }

        protected ActionResult Json(object o)
        {
            return Json(JSONObject.Create(o));
        }

        protected ActionResult Json(JSONObject obj)
        {
            ActionResult result = new ActionResult();
            result.MIMEType = MimeTypes.GetMimeTypeFor(".json");
            result.Data = obj.ToJsonString();
            return result;
        }

        protected ActionResult SendFile(string path)
        {
            return new FileResult() { FilePath = path };
        }
    }

    public class ActionResult
    {
        public string Data { get; set; }

        public string MIMEType { get; set; }

    }

    public class FileResult : ActionResult
    {
        public string FilePath { get; set; }
    }

    public class WebServerApplication
    {
        public string Name { get; set; }
        public WebServer Server { get; private set; }
        public List<Binding> AllBindings { get; private set; } = new List<Binding>();
        public string Path { get; set; } = "";
        public string TempPath { get; set; } = "temp\\";
        public ILogger Logger { get; set; }

        public WebServerApplication(WebServer server)
        {
            this.Server = server;
        }

        public virtual void HandleRequest(HttpHeaderRequest request, Stream stream)
        {
            throw new NotImplementedException();
        }

    }

    public class NWWebServerApplication : WebServerApplication
    {
        public Dictionary<string, ControllerInfo> Linkers { get; private set; } = new Dictionary<string, ControllerInfo>();
        public string DefaultControllerName { get; set; } = "WEB";

        public NWWebServerApplication(WebServer server)
            : base(server)
        {
        }

        public override void HandleRequest(HttpHeaderRequest request, Stream stream)
        {
            HtmlWriter writer = new HtmlWriter(stream);
            bool forcedExit = false;
            if (request.AllFiles.Count > 0)
            {
                forcedExit = HandleFiles(request);
            }
            Logger.WriteVerbose(request.RequestType + " request: " + request.FullPath);

            ActionResult result = null;
            string[] path = request.BasePath.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

            string sid = null;
            bool storeSid = true;
            if (request.CurrentCookies.ContainsKey("sid"))
            {
                sid = request.CurrentCookies["sid"];
                storeSid = false;
            }
            else
            {
                SessionInfo info = new SessionInfo();
                sid = info.SessionID;
                SessionInfo.Infos.Add(sid, info);
            }

            HttpHeaderResponse response = HttpWriter.GenerateDefault(200);
            Dictionary<string, ControllerInfo> linkers = this.Linkers;
            if (File.Exists(Path + request.BasePath))
            {
                result = new FileResult() { FilePath = Path + request.BasePath };
            }
            else if (path.Length > 0 && linkers.ContainsKey(path[0].ToUpper()))
            {
                if (path.Length > 1 && linkers[path[0].ToUpper()].Methods.ContainsKey(path[1].ToUpper()))
                {

                    //FIXED: Problem with session if the page is loaded without a controller being called.
                    //TODO: Still problem with multiple sessions beeing created
                    result = linkers[path[0].ToUpper()].Invoke(path[1], null, path, writer, ref sid);
                }
            }
            else
            {
                //HACK: Find way to specify default controller action
                result = linkers[DefaultControllerName.ToUpper()].Invoke("index", null, path, writer, ref sid);
            }

            if (storeSid)
            {
                response.NewCookies.Add("sid", new HttpCookie() { Name = "sid", Value = sid });
            }

            try
            {
                if (result is FileResult)
                {
                    writer.WriteFile(response, ((FileResult)result).FilePath);
                }
                else if (result is ActionResult)
                {
                    writer.WriteMessage(response, result.Data, result.MIMEType);
                }
            }
            catch (Exception e)
            {
                Logger.WriteException("Problem with sending", e, 3);
            }
            finally
            {
                writer.Close();
            }
            if (forcedExit)
            {
                Logger.WriteInfo("Terminating NetworkManager");
                Environment.Exit(0);
            }
        }

        public virtual bool HandleFiles(HttpHeaderRequest request)
        {
            foreach (HttpFileInfo file in request.AllFiles)
            {
                FileInfo fi = new FileInfo(TempPath + file.Name);
                if (!fi.Directory.Exists)
                    fi.Directory.Create();
                File.WriteAllBytes(fi.FullName, file.RawData);
                
                if (fi.Extension == ".zip")
                {
                    ZipArchive zip = new ZipArchive(fi.OpenRead());
                    ZipArchiveEntry zae = zip.GetEntry("update.conf");
                    if (zae != null)
                    {
                        if (Program.CoreOnline)
                        {
                            CommandSender sender = new CommandSender();
                            sender.SendCommand("Update", new string[] { fi.FullName });
                            sender.Close();
                            Logger.WriteInfo("Started Updating");
                            return true;
                        }
                        else
                        {
                            Logger.WriteInfo("Updating is not available sins the core is not running");
                        }
                    }
                }
            }
            return false;
        }
    }

    public class Binding
    {
        public IPAddress BindAddress { get; set; }
        public int Port { get; set; }
        public string HostName { get; set; }
    }

    public class MobileController : Controller
    {
        public override string DefaultFile { get; } = "index.html";

        public MobileController()
        {
            
        }

        public ActionResult Index(string[] path)
        {
            return View("");
        }
    }

    public class PageInfo : DynamicObject
    {
        public Dictionary<string, object> Items { get; private set; } = new Dictionary<string, object>();

        public PageInfo()
        {

        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            return Items.TryGetValue(binder.Name, out result);
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            Items[binder.Name] = value;
            return true;
        }

    }

    /*public class ListnerInstance
    {
        public TcpListener Listner { get; set; }
        public Dictionary<string, WebServerApplication> Bound = new Dictionary<string, WebServerApplication>();
        public WebServerApplication Default { get; set; }
    }*/
}
