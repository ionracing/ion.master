﻿using Ion.Data.Networking.Manager;
using Ion.Data.Networking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Ion.Data.Networking.Logging;

namespace Ion.Pro.NetworkManager
{
    public class ScreenUpdater : IService
    {
        //UdpClient sender = new UdpClient();
        ILogger logger;
        DataClient sender;
        public Service Container { get; set; }

        public void Initialize(Service container)
        {
            this.logger = container.Logger;
            logger.WriteInfo("Sending info to: Loopback" + NetPorts.ScreenSend);
            //sender.Connect("127.0.0.1", 9960);
            sender = new LocalNetworkClient(NetPorts.ScreenRecive) { PackageMode = WrapperMode.CompressedMode };
            (sender as LocalNetworkClient).Connect(IPAddress.Loopback, NetPorts.ScreenSend);
        }

        public void Start()
        {
            SensorDataStore.SensorUpdate += SensorDataStore_SensorUpdate;
        }

        private void SensorDataStore_SensorUpdate(object sender, SensorUpdateEventArgs e)
        {
            byte[] bytes = e.Data.GetBytesWithTime(WrapperMode.NormalMode);
            try
            {
                this.sender.SendDataWrappers(new DataWrapper[] { e.Data }, false);//.Send(bytes, bytes.Length);
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }
        }

        public void Stop()
        {
            SensorDataStore.SensorUpdate -= SensorDataStore_SensorUpdate;
        }
    }
}
