﻿//#define LOCALDEBUG
using Ion.Data.Networking;
using Ion.Data.Networking.FileSystem;
using Ion.Data.Networking.Logging;
using Ion.Data.Networking.Manager;
using Ion.Data.Networking.NewStack;
using Ion.Data.Sensor;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using NewStack = Ion.Data.Networking.NewStack;

namespace Ion.Pro.NetworkManager
{
    public class DataReceiver : IService
    {
#if LOCALDEBUG
        static bool Override = true;
#else
        static bool Override = false;
#endif
        public DataClient ReceiveClient { get; set; }
        public CanLinkClient GPSReceiver { get; set; }
        public int PackageCount { get; private set; }
        Task task;
        ILogger logger;
        bool running = true;
        static string logDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "IonServer", "DataLog") + "/";
        public static FileWriter usartLogWriter;
        public static FileWriter exceptionWriter;

        SerialPort piPort;

        public Service Container { get; set; }

        public void Initialize(Service container)
        {
            this.logger = container.Logger;
            if (!Directory.Exists(logDir))
                Directory.CreateDirectory(logDir);
            logger.WriteVerbose("Setting logpath to: " + logDir);

            //Find the latest available filecount
            int counter = 0;
            while (File.Exists(logDir + counter.ToString("000") + "_usart_data.log") || File.Exists(logDir + counter.ToString("000") + "_usart_exception.log"))
                counter++;

            byte[] prevMessage = new byte[0];
            if (File.Exists(logDir + (counter - 1).ToString("000") + "_usart_exception.log"))
                prevMessage = File.ReadAllBytes(logDir + (counter - 1).ToString("000") + "_usart_exception.log");
            if (prevMessage.Length == 0)
                logger.WriteVerbose("No CAN errors from previous run, not loading anything");
            else
                logger.WriteVerbose("CAN errors from previous run, loading them in");
            ILogger prevCan = container.Manager.LogManager.CreateLogger("CANOLDMSG");
            if (prevMessage.Length > 0)
            {
                for (int i = 0; i < prevMessage.Length; i += 10)
                {
                    DataWrapper wrapper = DataWrapper.ReadDataWithTime(prevMessage, i, WrapperMode.NormalMode);
                    CanError error = CanError.GetError(wrapper.SensorID, wrapper.Value);
                    prevCan.WriteLine("CAN OLD: " + error.Msg, "ERROR", 3);
                    //prevCan.WriteCustom(new CanEntry() { CanMessage = DataWrapper.ReadData(prevMessage, i, true), Category = "ERROR", Level = 4, Value = "Previous CAN error" });
                    //canLogger.WriteCustom(new CanErrorEntry() { CanMessage = wrapper, Category = "ERROR", Level = 3, Value = "CAN error message"});
                }
            }

            usartLogWriter = new FileWriter(logDir + counter.ToString("000") + "_usart_data.log");
            exceptionWriter = new FileWriter(logDir + counter.ToString("000") + "_usart_exception.log");

            logger.WriteInfo("File loggers created");

        }

        public void Start()
        {
            //GpsLoop();
            string[] availablePorts = SerialPort.GetPortNames();
            try
            {
                if (Override || Contains(SerialPort.GetPortNames(), "/dev/ttyAMA0"))
                {
                    logger.WriteLine("Open car serial port", "INFO", 1);

#if LOCALDEBUG
                    TcpClient client = new TcpClient();
                    client.Connect(new IPEndPoint(IPAddress.Loopback, 9999));
                    PiUsartInterface piUsartInt = new PiUsartInterface(client.GetStream());
#else
                    //piPort = new SerialPort("/dev/ttyAMA0", 9600, Parity.None, 8, StopBits.One);
                    piPort = new SerialPort("/dev/ttyAMA0", 38400, Parity.None, 8, StopBits.One);
                    PiUsartInterface piUsartInt = new PiUsartInterface(piPort);
#endif
                    NewStack.NetworkManager.RegisterInterface(10, piUsartInt);

                    ReceiveClient = new CanLinkClient(10);
                    GPSReceiver = new CanLinkClient(11);
                    task = Task.Run(new Action(ReceiveLoop));
                    Task.Run(new Action(GpsLoop));
                }
                else
                {
                    logger.WriteLine("Receiver not available, starting sim mode", "ERROR", 3);
                    ReceiveClient = new LocalNetworkClient(NetPorts.DataRecive) { PackageMode = WrapperMode.NormalMode };

                    task = Task.Run(new Action(ReceiveLoop));
                }
            }
            catch (Exception e)
            {
                logger.WriteException("Error opening connection", e, 4);
            }
        }

        public void ReceiveLoop()
        {
            logger.WriteInfo("Entered ReceiveLoop");
            if (ReceiveClient == null)
            { 
                Container.Crash(this, "ReceiveClient is not available");
                return;
            }
            try
            {
                while (running)
                {
                    DataWrapper[] data = ReceiveClient.ReadDataWrappers(false);
                    PackageCount++;
                    SensorDataStore.AddValue(data);
                }

            }
            catch (Exception e)
            {
                logger.WriteException("Exception", e, 4);
            }
        }

        public void GpsLoop()
        {
            while (true)
            {
                string line = Encoding.Default.GetString(GPSReceiver.Receive());
                //string line = "$GPRMC,075401.600,A,5856.1557,N,00543.5761,E,0.49,143.65,180416,,,A*62";
                Console.WriteLine(line);
                string[] parts = line.Split(',');
                try
                {
                    switch (parts[0].ToUpper())
                    {
                        case "$GPRMC":
                            if (parts[2] == "A")
                            {
                                UpdateGPS(parts[3], parts[5], parts[4], parts[6]);
                            }
                            break;
                    }
                }
                catch (Exception e)
                {
                    //TODO: Create gps logger
                    logger.WriteException("GPS Logger An exception occured", e, 3);
                }
                //System.Threading.Thread.Sleep(1000);
            }
        }

        public void Stop()
        {
            running = false;
        }

        //static bool sendFullGps = false;

        static ushort prevdeg;
        static ushort prevmin;
        static ushort prevsecLat;
        static ushort prevsecLong;
#pragma warning disable
        static ushort deg;
        static ushort min;
        static ushort secLat;
        static ushort secLong;
#pragma warning restore

        public void UpdateGPS(string latitude, string longitude, string dirNS, string dirEW)
        {
            prevdeg = deg;
            prevmin = min;
            prevsecLat = secLat;
            prevsecLong = secLong;

            int degLat = byte.Parse(latitude.Substring(0, 2)) + (dirNS == "S" ? 90 : 0);
            int degLong = ushort.Parse(longitude.Substring(0, 3)) + (dirEW == "W" ? 90 : 0);

            int minLat = byte.Parse(latitude.Substring(2, 2));
            int minLong = byte.Parse(longitude.Substring(3, 2));

            //deg = (ushort)((degLat << 9) | degLong);
            //min = (ushort)(((dirNS == "N" ? 0 : 1) << 15) | ((dirEW == "E" ? 0 : 1) << 14) | (minLat << 6) | minLong);
            secLat = ushort.Parse(latitude.Substring(5, 2));
            secLong = ushort.Parse(longitude.Substring(6, 2));
            int highResLat = ushort.Parse(latitude.Substring(7, latitude.Length - 7));
            int highResLong = ushort.Parse(longitude.Substring(8, longitude.Length - 8));

            int latVal = degLat * (60 * 100) + minLat * 100 + secLat;
            int longVal = degLong * (60 * 100) + minLong * 100 + secLong;

            SensorDataStore.AddValue(new DataWrapper() { SensorID = 252, Value = highResLat << 12 | highResLong });
            SensorDataStore.AddValue(new DataWrapper() { SensorID = 250, Value = latVal});
            SensorDataStore.AddValue(new DataWrapper() { SensorID = 251, Value = longVal });

            /*if (prevmin != min || prevdeg != deg)
            {
                SensorDataStore.AddValue(new DataWrapper() { SensorID = 200, Value = deg });
                SensorDataStore.AddValue(new DataWrapper() { SensorID = 201, Value = min });
                sendFullGps = true;
            }*/
        }

        //TODO: Move to utility library/class
        public static bool Contains(string[] devices, string dev)
        {
            foreach (string s in devices)
            {
                if (s == dev)
                    return true;
            }
            return false;
        }
    }



    public class Coordinate
    {

    }
}
