﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NicroWare.Lib.NetUtility.Http;
using NicroWare.Lib.NetUtility.Html;
using System.Net;
using System.Net.Sockets;
using System.IO;
using Ion.Data.Networking.Logging;
using System.Diagnostics;
using System.Reflection;
using Ion.Data.Networking.CoreCom;
using Ion.Data.Networking;
using Ion.Data.Networking.Net;
using Ion.Data.Networking.NewStack;
using Ion.Data.Networking.Manager;
using Ion.Data.Sensor;

namespace Ion.Pro.NetworkManager
{

    /*
    +-----------+-----------+-------------------+
    |Port       |Protocol   |Usage              |
    +-----------+-----------+-------------------+
    |9966       |TCP        |HTTP               |
    |9965       |UDP        |Log Listner (Core) |
    |9965       |TCP        |Log Sender  (Core) |
    |9960       |UDP        |Sensor Data Stream |
    |9950       |TCP        |CoreCom     (Core) |
    +-----------+-----------+-------------------+
    */
    
    class Program
    {
        
        
        //public static ILogManager TestManager = new LogManager();
        public static ILogManager TestManager;// = new LogManagerClient();
        public static bool Running { get; set; } = true;
        public const string Version = "0.2.1 Beta";
        public const string Name = "NETMAN";
        public static bool CoreOnline { get; private set; } = false;

#pragma warning disable
        static TcpListener listner;
#pragma warning restore

        static void Main(string[] args)
        {
            /*PiUsartInterface interf = new PiUsartInterface();
            interf.Receive();
            Data.Networking.NewStack.NetworkManager.RegisterInterface(10, interf);
            CanLinkClient ReceiveClient = new CanLinkClient(10);
            DataWrapper[] wrappers = ReceiveClient.ReadDataWrappers(false);

            return;*/
            /*NRFDataLink linker = new NRFDataLink();
            int counter = 0;
            byte[] bytes = new byte[32];
            bytes[25] = 0x69;
            while (true)
            {
                linker.RawSend(bytes);
                if (counter % 500 == 0)
                    Console.WriteLine(counter);
                counter++;

                System.Threading.Thread.Sleep(1);
            }*/

            /*listner = new TcpListener(IPAddress.Any, 9999);
            listner.Start();
            Task.Run(new Action(GenerateRandom));*/
            NicroWare.Lib.NetUtility.Dhcp.AdvIPAddress adr = new NicroWare.Lib.NetUtility.Dhcp.AdvIPAddress(new byte[] { 10, 0, 0, 1 }, 24);

            //NicroWare.Lib.NetUtility.Dhcp.AdvIPAddress adr2 = new NicroWare.Lib.NetUtility.Dhcp.AdvIPAddress(new byte[] { 0x20, 0x01, 0xAC, 0xAD, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 }, 64);

            //Console.WriteLine(adr.FullNetmask);

            Console.Title = "Network Manager";
            Process p = Process.GetCurrentProcess();
            try
            {
                CoreInterface core = new CoreInterface(new CommandSender());
                core.Register(Name, p.Id.ToString());
                core.Close();
                CoreOnline = true;
            }
            catch
            {

            }

            if (CoreOnline)
            {
                TestManager = new LogManagerClient();
            }
            else // Creates a logger in case the IonCore is not responding.
            {
                TestManager = new LogManager();
            }
            
            ILogger system = TestManager.CreateLogger("SYSTEM");
            Data.Networking.NewStack.NetworkManager.DefaultLogger = TestManager.CreateLogger("NEWNETMANAGER");
            //Data.Networking.NewStack.NetworkManager.LoggReceived = true;
            if (!CoreOnline) system.WriteInfo("IonCore not available");
            system.WriteInfo("Starting System");

            try
            {
                NRFDataLink link = new NRFDataLink();
                Data.Networking.NewStack.NetworkManager.RegisterInterface(5, link);
                link.FastWriteMode = true;
            }
            catch (Exception e)
            {
                system.WriteException("NRF data link not available.", e, 3);
            }
            //UDPDataLink udpDataLink = new UDPDataLink(10110);
            //udpDataLink.Connect(10111);
            //Data.Networking.NewStack.NetworkManager.RegisterInterface(6, udpDataLink);

            ServiceManager manager = new ServiceManager(TestManager);
            manager.Add(new WebServer(NetPorts.WebServer));
            manager.Add(new DataReceiver());
            manager.Add(new ScreenUpdater());
            manager.Add(new TelemetryClient());
            manager.Add(new DhcpService());
            manager.Initialize();
            manager.Start();
            manager.WaitForExit();
            //TestManager.OnInfo += TestManager_OnInfo;

            /*system.WriteInfo("Creating Server");
            WebServer server = new WebServer(9966);
            system.WriteInfo("Starting Server");
            server.Start();
            while (Running)
            {
                System.Threading.Thread.Sleep(1000);
            }*/
        }
        public static void GenerateRandom()
        {
            TcpClient client = listner.AcceptTcpClient();
            byte counter = 0;
            //byte counter = 0xCB;
            ushort counter2 = 0;
            Random rnd = new Random();
            Stream s = client.GetStream();
            while (true)
            {
                byte[] data = new byte[2];
                rnd.NextBytes(data);
                s.Write(new byte[] { 0xFF, 0xFF, counter, data[0], (byte)(data[1] & 0x0F), 0, 0 }, 0, 7);
                //byte[] bytes = BitConverter.GetBytes((ushort)64 * counter2);
                //byte[] bytes = BitConverter.GetBytes((ushort)(Math.Pow(2, 16) - 64));
                //s.Write(new byte[] { 0xFF, 0xFF, counter, bytes[0], bytes[1], 0, 0 }, 0, 7);
                /*if (false && rnd.Next(100) > 90)
                {
                    s.Write(new byte[] { 0x43, 0x76 }, 0, 2);
                    Console.WriteLine("--------------------------------");
                }*/
                System.Threading.Thread.Sleep(50);
                counter++;
                counter2++;
            }
        }

        private static void TestManager_OnInfo(object sender, LogWriteEventArgs e)
        {
            //Console.WriteLine($"{e.Entry.Time} ({e.Entry.Sender})[{e.Entry.Level}] {e.Entry.Value}");
            Console.WriteLine("{0} ({1})[{2}] {3}", e.Entry.Time, e.Entry.Sender, e.Entry.Level, e.Entry.Value);
        }
    }


}
/*
 For å oppdatere, logg inn med putty på RPi
 user pi -> pass raspberry
     
     */