function generateElements(parent, list) {
    for (var i = 0; i < list.length; i++) {
        var li = document.createElement("li");
        var a = document.createElement("a");
        li.appendChild(a);
        a.href = list[i].link;
        a.innerHTML = list[i].name;
        parent.appendChild(li);

        if (list[i].sub != null) {
            let span = document.createElement("span");
            span.className = "expander";
            span.innerHTML = "&or;";
            a.appendChild(span);
            let ul = document.createElement("ul");
            ul.className = "sub-menu hidden";
            li.appendChild(ul);
            a.addEventListener("click", function() {
                //ul.style.display = (ul.style.display == "block" ? "none" : "block");
                ul.classList.toggle("hidden");
                span.classList.toggle("rotate");
                /*span.style.transform = "rotate(0deg)";*/
            })
            generateElements(ul, list[i].sub);
        }
    }
}

window.onload = function() {
    var ulnav = document.getElementById("mainmenu");
    generateElements(ulnav, menuItems);
}