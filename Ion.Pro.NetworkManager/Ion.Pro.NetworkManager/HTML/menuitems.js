var menuItems = [
    { name: "Home", link: "#" },
    { name: "Log", link: "#"},
    { name: "Info", link: "#", sub: 
    [
        { name: "Sub Element 1", link: "#" },
        { name: "Sub Element 2", link: "#" },
        { name: "Sub Element 3", link: "#" },
        { name: "Sub Element 4", link: "#" }
    ] },
    { name: "Network", link: "#"},
    { name: "Diagnostics", link: "#" },
    { name: "Another Test", link: "#" }
];