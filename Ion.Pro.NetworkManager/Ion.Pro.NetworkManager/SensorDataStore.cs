﻿using Ion.Data.Networking.Manager;
using Ion.Data.Sensor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.NetworkManager
{
    public static class SensorDataStore
    {
        public static Dictionary<SensorLookup, DataWrapper> Latest { get; private set; } = new Dictionary<SensorLookup, DataWrapper>();
        public static Dictionary<int, PackageInfo> PackageInfos { get; private set; } = new Dictionary<int, PackageInfo>();
        public static event SensorUpdateEventHandler SensorUpdate;

        public static void AddValue(DataWrapper[] wrapper)
        {
            foreach (DataWrapper dw in wrapper)
            {
                try
                {
                    AddValue(dw);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        public static void AddValue(DataWrapper wrapper)
        {
            wrapper.TimeStamp = (int)GlobalTime.ElapsedTime.TotalMilliseconds;
            DataReceiver.usartLogWriter.Write(wrapper.GetBytesWithTime(WrapperMode.NormalMode));
            SensorLookup lookup = SensorLookup.GetById(wrapper.SensorID);
            if (!PackageInfos.ContainsKey(wrapper.SensorID))
            {
                PackageInfos.Add(wrapper.SensorID, new PackageInfo(wrapper.SensorID));
            }
            PackageInfos[wrapper.SensorID].RegisterValue(wrapper.Value);
            //Console.WriteLine("Adding if existed, Lookup:  " + (lookup == null ? "null" : lookup.ToString()));
            
            if (lookup != null)
            {
                if (Latest.ContainsKey(lookup))
                    Latest[lookup] = wrapper;
                else
                    Latest.Add(lookup, wrapper);
            }
            //Console.WriteLine("----------------Invoking SensorUpdate--------------------");
            SensorUpdate?.Invoke(null, new SensorUpdateEventArgs(wrapper));
            if (wrapper.SensorID > 99 && wrapper.SensorID < 200)
            {
                //TODO: Can exception handling
                //exceptionWriter.Write(wrapper.GetBytes(true));
                //canLogger.WriteCustom(new CanEntry() { CanMessage = wrapper, Category = "ERROR", Level = 4, Value = "CAN error message" });
            }
        }

        public static DataWrapper? GetData(SensorLookup lookup)
        {
            if (lookup == null)
                return null;
            if (Latest.ContainsKey(lookup))
                return Latest[lookup];
            return null;
        }

        public static DataWrapper? GetData(ushort sensorID)
        {
            return GetData(SensorLookup.GetById(sensorID));
        }

        public static DataWrapper? GetData(string name)
        {
            return GetData(SensorLookup.GetByName(name));
        }
    }

    public class SensorUpdateEventArgs : EventArgs
    {
        public DataWrapper Data { get; private set; }

        public SensorUpdateEventArgs(DataWrapper data)
        {
            Data = data;
        }
    }

    public delegate void SensorUpdateEventHandler(object sender, SensorUpdateEventArgs e);

    public class ObjectWrap<T>
    {
        public T Value { get; set; }

        public ObjectWrap(T value)
        {
            this.Value = value;
        }
    }


}
