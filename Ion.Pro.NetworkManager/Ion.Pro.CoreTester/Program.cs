﻿using Ion.Data.Networking.CoreCom;
using Ion.Data.Networking.Logging;
using Ion.Data.Networking.NewStack;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NicroWare.Lib.NetUtility.Html.Parser;
using System.Net.Http;
using System.IO;

namespace Ion.Pro.CoreTester
{
    public class ComponentInfo
    {
        public string URL { get; set; }
        public string Title { get; set; }
        public string Brand { get; set; }
        public string SKU { get; set; }
        public string MPN { get; set; }
    }

    class Program
    {
        static List<ComponentInfo> allInfos = new List<ComponentInfo>();
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter RS URL");
            Console.Write(">");
            GetInformation(Console.ReadLine());
            string line;
            while (ReadLine(out line))
            {
                try
                {
                    GetInformation(line);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Could not parse url properly");
                    Console.WriteLine(e);
                }
            }
            TextWriter tw = new StreamWriter("test.csv");
            tw.WriteLine("Pos.;Beskrivelse;Kundens varenr.;RS varenr.;Produsentens varenr.;Prodisent;Ditt antall;Tilbuds antall;Salgsenhet;Ordreantall;Pris pr stk;Totalpris pr linje;Leveringsinformasjon;NCNR;ROHS kompatibel");
            int counter = 1;
            foreach (ComponentInfo ci in allInfos)
            {
                tw.WriteLine($"{counter};\"{ci.Title}\";;{ci.SKU};{ci.MPN};{ci.Brand};{10};");
                counter++;
            }
            tw.Close();

            Console.ReadLine();
        }

        static void OldTest()
        {
            /*LoopbackDataLink loopback = new LoopbackDataLink();
            NetworkManager.RegisterInterface(0, loopback);

            IdpClient client = new IdpClient();

            client.Send(0, 4, new byte[] { 0xA, 0xB, 12, 65 });

            byte[] bytes = client.Receive(4);*/

            /*LogManagerClient client = new LogManagerClient();
            client.AddEventListner((object sender, LogWriteEventArgs e) => { Console.WriteLine(e.Entry.Value); }, LogLevel.VerboseInfo);
            ILogger logger = client.CreateLogger("TestLogger");
            logger.WriteVerbose("Hello");*/

            //CoreInterface core = new CoreInterface(new CommandSender());
            //Process p = Process.GetCurrentProcess();
            /*Console.WriteLine(core.Version());
            Console.WriteLine(core.Register("TESTER", p.Id.ToString()));*/
        }

        static bool ReadLine(out string line)
        {
            Console.Write(">");
            line = Console.ReadLine();
            if (line.ToLower() == "exit")
                return false;
            return true;
        }

        static void GetInformation(string url)
        {
            HtmlParser parser = new HtmlParser();
            HttpClient client = new HttpClient();
            Uri uri = new Uri(url);
            Task<string> task = client.GetStringAsync(uri);
            task.Wait();
            HtmlObject obj = parser.Parse(task.Result);
            ComponentInfo info = new ComponentInfo();
            info.URL = url;
            info.Title = obj.GetChildsByParameter("itemprop", "name")[0].InnerText;
            info.SKU = obj.GetChildsByParameter("itemprop", "sku")[0].InnerText;
            info.MPN = obj.GetChildsByParameter("itemprop", "mpn")[0].InnerText;
            info.Brand = obj.GetChildsByParameter("itemprop", "brand")[0].InnerText;
            //var v = obj.GetChildsByClass("pageHeader")[0].children[0].children[0].innerHTML;
            Console.WriteLine("Title:\t" + info.Title);
            Console.WriteLine("SKU:\t" + info.SKU);
            Console.WriteLine("Brand:\t" + info.Brand);
            Console.WriteLine("MPN:\t" + info.MPN);
            allInfos.Add(info);
        }
    }
}
